# Global Game Jam 2017

This repo is for a game to be created for the Global Game Jam 2017.

## The Team
The team consists of:
  * **Jay Suong** (AI / Systems) 
  * **Steven Wong** (Procedural Mesh)
  * **Wengu (Eddy) Hu**  (Music / Audio / Systems)
  * **James Cao**  (Mocap Tech / Visuals)
  * **Chopin Chee** (3D Modeling / Visuals)

## Setup
Clone the repository, and open on Unity 5.5 or higher.

## Demos / Videos 
[A video of the game can be found here on YouTube](https://youtu.be/gXuMbsU93Dw)