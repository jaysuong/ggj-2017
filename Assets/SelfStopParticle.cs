﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfStopParticle : MonoBehaviour {
	ParticleSystem pS;
	float timer = 0;

	public float stopAfterSeconds = 3;
	// Use this for initialization
	void Start () {
		pS = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		if(timer > stopAfterSeconds)
		{
			pS.Stop();
			//r e = pS.emission;
			//enabled = false;
		}
		else
		{
			timer += Time.deltaTime;
		}
	}
}
