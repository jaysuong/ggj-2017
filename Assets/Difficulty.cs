﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Difficulty : MonoBehaviour {
	public float difficultyIncreaseSpeed = 0.03f;
	WaveManager waveManager;
	// Use this for initialization
	void Start () {
		waveManager = GetComponent<WaveManager>();
	}
	
	// Update is called once per frame
	void Update () {
		waveManager.spawnInterval -= difficultyIncreaseSpeed * Time.deltaTime;
	}
}
