﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(SphereCollider))]

public class SpikeBall : MonoBehaviour
{

    // https://blog.nobel-joergensen.com/2010/12/25/procedural-generated-mesh-in-unity/

    // A mesh consists of vertices (array of Vector3).
    // Each triangle face consists of three vertices.

    // Triangle is defined using clockwise polygon winding?

    public bool RandomizeHeights = true;
    public float InnerRadius = 1;
    public int MaxHeight = 3;
    public float updateInterval = 0.1f;

    //float InnerRadius = 1f;
    float InitialHeight = 1f;

    float[,] PyramidHeight = new float[12, 12];
    Vector3[,,] PyramidVertices = new Vector3[12, 12, 5];
    //Vector3[,,,] PyramidTriangles = new Vector3[12, 12, 4, 3];

    Vector3[] MeshVertices = new Vector3[144 * 5];
    int[] MeshTriangles = new int[144 * 4 * 3];



    // Use this for initialization
    void Start()
    {

        // Puts the sphere inside the spikes
        SphereCollider InnerSphere = GetComponent<SphereCollider>();
        InnerSphere.radius = InnerRadius;

        // Generate initial spikes
        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 12; j++)
            {
                PyramidHeight[i, j] = InitialHeight;

                Vector3 PyramidTop = Quaternion.Euler((i + 1) * 30 - 15, 0, (j + 1) * 30 - 15) * new Vector3(0, InnerRadius + PyramidHeight[i, j], 0);

                Vector3 PyramidTopLeft = Quaternion.Euler(i * 30, 0, j * 30) * new Vector3(0, InnerRadius, 0);
                Vector3 PyramidTopRight = Quaternion.Euler(i * 30, 0, (j + 1) * 30) * new Vector3(0, InnerRadius, 0);
                Vector3 PyramidBotLeft = Quaternion.Euler((i + 1) * 30, 0, j * 30) * new Vector3(0, InnerRadius, 0);
                Vector3 PyramidBotRight = Quaternion.Euler((i + 1) * 30, 0, (j + 1) * 30) * new Vector3(0, InnerRadius, 0);

                PyramidVertices[i, j, 0] = PyramidTop;
                PyramidVertices[i, j, 1] = PyramidTopLeft;
                PyramidVertices[i, j, 2] = PyramidTopRight;
                PyramidVertices[i, j, 3] = PyramidBotLeft;
                PyramidVertices[i, j, 4] = PyramidBotRight;


                //PyramidTriangles[i, j, 0, 0] = PyramidTop;
                //PyramidTriangles[i, j, 0, 1] = PyramidTopLeft;
                //PyramidTriangles[i, j, 0, 2] = PyramidTopRight;

                //PyramidTriangles[i, j, 1, 0] = PyramidTop;
                //PyramidTriangles[i, j, 1, 1] = PyramidTopLeft;
                //PyramidTriangles[i, j, 1, 2] = PyramidBotLeft;

                //PyramidTriangles[i, j, 2, 0] = PyramidTop;
                //PyramidTriangles[i, j, 2, 1] = PyramidBotRight;
                //PyramidTriangles[i, j, 2, 2] = PyramidTopRight;

                //PyramidTriangles[i, j, 3, 0] = PyramidTop;
                //PyramidTriangles[i, j, 3, 1] = PyramidBotRight;
                //PyramidTriangles[i, j, 3, 2] = PyramidBotLeft;

                //PyramidTriangles[i, j, 4, 0] = PyramidTop;
                //PyramidTriangles[i, j, 4, 1] =PyramidTopLeft;
                //PyramidTriangles[i, j, 4, 2] =PyramidTopRight;
            }
        }




        MeshFilter meshFilter = GetComponent<MeshFilter>();
        if (meshFilter == null)
        {
            Debug.LogError("MeshFilter not found!");
            return;
        }


        Mesh mesh = meshFilter.sharedMesh;
        if (mesh == null)
        {
            meshFilter.mesh = new Mesh();
            mesh = meshFilter.sharedMesh;
        }
        mesh.Clear();

        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 12; j++)
            {

                for (int k = 0; k < 5; k++)
                {
                    MeshVertices[5 * (12 * i + j) + k] = PyramidVertices[i, j, k];
                    //mesh.vertices += PyramidVertices[i, j, k];
                }

                MeshTriangles[12 * (12 * i + j) + 0] = 0 + 5 * (12 * i + j);
                MeshTriangles[12 * (12 * i + j) + 1] = 1 + 5 * (12 * i + j);
                MeshTriangles[12 * (12 * i + j) + 2] = 2 + 5 * (12 * i + j);

                MeshTriangles[12 * (12 * i + j) + 3] = 0 + 5 * (12 * i + j);
                MeshTriangles[12 * (12 * i + j) + 4] = 1 + 5 * (12 * i + j);
                MeshTriangles[12 * (12 * i + j) + 5] = 3 + 5 * (12 * i + j);

                MeshTriangles[12 * (12 * i + j) + 6] = 0 + 5 * (12 * i + j);
                MeshTriangles[12 * (12 * i + j) + 7] = 2 + 5 * (12 * i + j);
                MeshTriangles[12 * (12 * i + j) + 8] = 4 + 5 * (12 * i + j);

                MeshTriangles[12 * (12 * i + j) + 9] = 0 + 5 * (12 * i + j);
                MeshTriangles[12 * (12 * i + j) + 10] = 3 + 5 * (12 * i + j);
                MeshTriangles[12 * (12 * i + j) + 11] = 4 + 5 * (12 * i + j);
            }
        }


        mesh.vertices = MeshVertices;
        mesh.triangles = MeshTriangles;


        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }

    float timer = 0.5f;

    // Update is called once per frame
    void Update()
    {

        if (RandomizeHeights == true)
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                timer = updateInterval;
                updateMeshRandom();
            }
        }

    }

    // Sets the pyramids to random heights
    void updateMeshRandom()
    {

        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 12; j++)
            {
                PyramidHeight[i, j] = Mathf.Log(Random.Range(0.01f, MaxHeight)) * MaxHeight / 5 + MaxHeight / 2;
                //Debug.Log(PyramidHeight[i, j]);

                Vector3 PyramidTop = Quaternion.Euler((i + 1) * 30 - 15, 0, (j + 1) * 30 - 15) * new Vector3(0, InnerRadius + PyramidHeight[i, j], 0);

                PyramidVertices[i, j, 0] = PyramidTop;

                MeshVertices[5 * (12 * i + j)] = PyramidTop;
            }
        }

        MeshFilter meshFilter = GetComponent<MeshFilter>();
        if (meshFilter == null)
        {
            Debug.LogError("MeshFilter not found!");
            return;
        }


        Mesh mesh = meshFilter.sharedMesh;
        if (mesh == null)
        {
            meshFilter.mesh = new Mesh();
            mesh = meshFilter.sharedMesh;
        }
        
        //mesh.Clear();

        mesh.vertices = MeshVertices;
        //mesh.triangles = MeshTriangles;

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }
}