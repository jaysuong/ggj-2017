using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ {
    /// <summary>
    /// A manager object that automatically pools all objects ahead of time to spawn
    /// </summary>
    public class ObjectPool : ScriptableObject {
        [SerializeField]
        private PoolInfo[] poolInfo;

        [NonSerialized]
        private bool isInitialized = false;

        private Dictionary<string, List<GameObject>> pool;

        [Serializable]
        public struct PoolInfo {
            public GameObject poolObject;
            public int count;
        }

        private void OnEnable() {
            pool = new Dictionary<string, List<GameObject>>();
        }

        private void OnDisable() {
            pool.Clear();
            isInitialized = false;
        }

        private void Init() {
            foreach(var info in poolInfo) {
                var root = new GameObject(string.Format("[Pool] {0}", info.poolObject.name));
                var list = new List<GameObject>(info.count);

                for(var i = 0; i < info.count; ++i) {
                    var clone = Instantiate(info.poolObject);
                    clone.SetActive(false);
                    clone.transform.SetParent(root.transform);

                    list.Add(clone);
                }

                pool.Add(info.poolObject.name, list);
            }

            isInitialized = true;
        }

        /// <summary>
        /// Spawns an object in the pool
        /// </summary>
        /// <param name="name">The name of the object</param>
        /// <param name="position">The position of the object</param>
        /// <param name="rotation">The object's rotation</param>
        /// <returns></returns>
        public GameObject Spawn(string name, Vector3 position, Quaternion rotation) {
            if(!isInitialized) { Init(); }

            List<GameObject> pooledObjects;

            if(pool.TryGetValue(name, out pooledObjects)) {
                for(var i = 0; i < pooledObjects.Count; ++i) {
                    var obj = pooledObjects[i];

                    if(!obj.activeInHierarchy) {
                        obj.transform.position = position;
                        obj.transform.rotation = rotation;
                        obj.SetActive(true);
                        return obj;
                    }
                }
            }

            return null;
        }

    }
}
