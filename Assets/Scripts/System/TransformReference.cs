using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InitialPrefabs.Core;

namespace GGJ {
    public class TransformReference : MonoBehaviour {
        [SerializeField]
        private ReferenceBank bank;
        [SerializeField]
        private string refName;


        private void Awake() {
            bank.AddReference(refName, transform);
        }
    }
}
