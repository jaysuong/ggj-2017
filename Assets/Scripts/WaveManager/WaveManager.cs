﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GGJ;

public class WaveManager : MonoBehaviour {

    // Every interval, spawn enemies

    // Who to spawn?
    // Where to spawn?
    // How many to spawn?

    // Enemies:
    // Boat
    // Hulk
    // Blimp
    // Knight

    public ObjectPool Pool;

    public GameObject[] Monsters;

    public Transform[] SpawnPoints;

    public float spawnInterval = 0.5f;  // seconds

    public int spawnAmount = 1;


    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	//void Update () {
		
	//}

    private IEnumerator LoopSpawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnInterval);

            GameObject newMob = Monsters[Random.Range(0, Monsters.Length)];

            Debug.Log(newMob.name);

            for (int i = 0; i < spawnAmount; i++)
            {
                Transform newMobSpawnPoint = SpawnPoints[Random.Range(0, SpawnPoints.Length)];

                GameObject mobClone = Pool.Spawn(newMob.name, newMobSpawnPoint.position, newMobSpawnPoint.rotation);
            }
            //Debug.Log("here");
        }

    }

    public void StartSpawning()
	{
		StopCoroutine(LoopSpawn());
		StartCoroutine(LoopSpawn());
    }

    public void StopSpawning()
    {
        StopCoroutine(LoopSpawn());
    }

}
