using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ {
    /// <summary>
    /// An animation behaviour that causes the agent to move forward
    /// </summary>
    public class MoveForward : StateMachineBehaviour {
        [SerializeField]
        private float speed = 1f;

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
            var transform = animator.transform;

            transform.position += transform.forward * speed * Time.deltaTime * animator.speed;
        }
    }
}
