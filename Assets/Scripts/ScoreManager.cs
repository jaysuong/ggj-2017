﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {
	public static ScoreManager instance;
	int score = 0;
	public TextMesh scoreGUI;
	// Use this for initialization
	void Awake () {
		if(ScoreManager.instance == null)
		{
			ScoreManager.instance = this;
		}
	}

	// Update is called once per frame
	public void AddScore (int scoreGet) {
		score += scoreGet;
		DisplayScoreRefresh();
	}

	void DisplayScoreRefresh()
	{
		scoreGUI.text = "SCORE: " + score;
	}
}
