using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InitialPrefabs.Core;

namespace GGJ {
    /// <summary>
    /// A simple script that handles enemy deaths
    /// </summary>
    [RequireComponent(typeof(Health))]
    public class EnemyDeathHandler : MonoBehaviour {
        [SerializeField]
        private GameEvent deathEvent;

        [SerializeField]
        private GameObject hitParticle;
        [SerializeField]
        private GameObject deathBody;
		[SerializeField]
		private int killScore = 10;


		private Health health;

        private void Awake() {
            health = GetComponent<Health>();
        }

        private void OnEnable() {
            health.Heal(health.MaxHealth);
            health.DeathEvent += HandleDeath;
            health.DamageHealthEvent += HandleDamage;
        }

        private void OnDisable() {
            health.DeathEvent -= HandleDeath;
            health.DamageHealthEvent -= HandleDamage;
        }

        private void HandleDeath(Health health) {
            if(deathBody != null) {
                Instantiate(deathBody, transform.position, transform.rotation);
            }

            gameObject.SetActive(false);

            if(deathEvent != null) {
                deathEvent.InvokeEvent();
			}
			ScoreManager.instance.AddScore(killScore);
		}

        private void HandleDamage(float damage, Health health) {
            if(hitParticle != null) {
                Instantiate(hitParticle, transform.position, transform.rotation);
            }
        }
    }
}
