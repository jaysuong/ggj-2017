using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InitialPrefabs.Core;

namespace GGJ {
    /// <summary>
    /// A simple script that handles enemy deaths
    /// </summary>
    [RequireComponent(typeof(Health))]
    public class PlayerDeathHandler : MonoBehaviour {
        [SerializeField]
        private GameEvent deathEvent;
		
        [SerializeField]
        private GameObject deathBody;
		[SerializeField]
		private float deathBodyDelay;

		[SerializeField]
		private GameObject playerBody;

		[SerializeField]
		private GameObject nextPlayer;

		[SerializeField]
		private GameObject hpBarFront;

		[SerializeField]
		private GameObject hpBar;

		[SerializeField]
		private GameObject gameOverText;

		[SerializeField]
		private WaveManager waveManager;


		private Health health;

        private void Awake() {
            health = GetComponent<Health>();
        }

        private void OnEnable() {
            health.Heal(health.MaxHealth);
            health.DeathEvent += HandleDeath;
            health.DamageHealthEvent += HandleDamage;
        }

        private void OnDisable() {
            health.DeathEvent -= HandleDeath;
            health.DamageHealthEvent -= HandleDamage;
        }

        private void HandleDeath(Health health)
		{
			nextPlayer.SetActive(true);
			waveManager.StopSpawning();
			Invoke("ActualDeath", deathBodyDelay);
        }

        private void HandleDamage(float damage, Health health) {
			/*
            if(hitParticle != null) {
                Instantiate(hitParticle, transform.position, transform.rotation);
            }
			*/
			var s = health.CurrentHealth / health.MaxHealth;
			if(health.CurrentHealth <= 0)
			{
				s = 0;
			}
			hpBarFront.transform.localScale = new Vector3(s, 1, 1);

		}

		private void ActualDeath()
		{
			if (deathBody != null)
			{

			}

			playerBody.SetActive(false);
			hpBar.SetActive(false);
			gameOverText.SetActive(true);

			if (deathEvent != null)
			{
				deathEvent.InvokeEvent();
			}
		}
    }
}
