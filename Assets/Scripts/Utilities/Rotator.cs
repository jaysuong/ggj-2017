﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {
	public Vector3 rotatingDirection = new Vector3(0, 0, 0);
	// Update is called once per frame
	void Update () {
		transform.Rotate (rotatingDirection * Time.deltaTime);
	}
}
