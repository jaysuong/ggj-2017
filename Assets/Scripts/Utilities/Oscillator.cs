using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ {
    /// <summary>
    /// A simple utility that vibrates an object in a sine pattern
    /// </summary>
    public class Oscillator : MonoBehaviour {
        [SerializeField]
        private float frequency;
        [SerializeField]
        private Vector3 amplitude;


        private void Update() {
            transform.localPosition = new Vector3(
                amplitude.x * Mathf.Sin(Time.time * frequency),
                amplitude.y * Mathf.Sin(Time.time * frequency),
                amplitude.z * Mathf.Sin(Time.time * frequency)
                );
        }
    }
}
