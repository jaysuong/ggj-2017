using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InitialPrefabs.Core;

namespace GGJ {
    public class Ammo : MonoBehaviour {
        [SerializeField]
        private float damage = 10f;
        [SerializeField]
        private float speed = 30f;
        [SerializeField]
        private float timeToLive = 4f;

        [SerializeField]
        private Rigidbody rigid;
        [SerializeField]
        private TrailRenderer trailRenderer;
		[SerializeField]
		private GameObject woundParticle;
		[SerializeField]
		private GameObject defendParticle;


		protected virtual void OnEnable() {
            rigid.velocity = transform.forward * speed;

            Invoke("DisableBullet", timeToLive);
        }

        protected virtual void OnDisable() {
            rigid.velocity = Vector3.zero;
            rigid.angularVelocity = Vector3.zero;

            if(trailRenderer != null) {
                trailRenderer.Clear();
            }
        }

        protected virtual void OnTriggerEnter(Collider other) {
			if (other.gameObject.layer != gameObject.layer)
			{
				var health = other.GetComponentInParent<Health>();

				if (health != null)
				{
					health.Damage(damage);
					Instantiate(woundParticle, transform.position, transform.rotation);
				}
				else
				{
					Instantiate(defendParticle, transform.position, transform.rotation);
				}
				DisableBullet();
			}

		}

        private void DisableBullet() {
            CancelInvoke();
            gameObject.SetActive(false);
        }
    }
}
