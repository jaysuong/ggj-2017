using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InitialPrefabs.Core;

namespace GGJ {
    public class PlayerAttackCollider : MonoBehaviour {
        [SerializeField]
        private float damage = 1000f;
		
		[SerializeField]
		private GameObject woundParticle;
		[SerializeField]
		private GameObject defendParticle;

        protected virtual void OnTriggerEnter(Collider other) {
			if (other.gameObject.tag != "PlayerCore")
			{
				var health = other.GetComponentInParent<Health>();

				if (health != null)
				{
					health.Damage(damage);
					Instantiate(woundParticle, transform.position, transform.rotation);
				}
				else
				{
					Instantiate(defendParticle, transform.position, transform.rotation);
				}
			}

		}
    }
}
