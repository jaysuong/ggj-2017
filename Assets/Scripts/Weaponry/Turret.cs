using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InitialPrefabs.Core;

namespace GGJ {
    public class Turret : MonoBehaviour {
        [SerializeField]
        private float fireSpeed = 1f;
        [SerializeField]
        private Transform firePoint;

        [SerializeField]
        private GameObject bulletPrefab;
        [SerializeField]
        private ObjectPool pool;

        [SerializeField]
        private bool rotateTowardsTarget = true;
        [SerializeField]
        private ReferenceBank bank;
        [SerializeField]
        private string targetName;
        [SerializeField]
        private float rotateSpeed = 1f;
		[SerializeField]
		private float firingRandomnize = 3.01f;
		[SerializeField]
		private GameObject firingParticle;

		private Transform target;
        private float fireCounter;
		private float fireTimer = 0;
		private float fireTimerTotal = 1;
		[SerializeField]
		private Vector2 timerTotalRange = new Vector2(0, 3.01f);
		private Vector3 tempTarget = new Vector3();



		private void Start() {
            fireCounter = 1f / fireSpeed;
			fireTimerTotal = Random.Range(timerTotalRange.x, timerTotalRange.y);

			target = bank.GetReference<Transform>(targetName);

			if (firePoint == null)
			{
				firePoint = transform;
			}
        }

        public void Update() {

            // Get the angle, and rotate towards it
            if(rotateTowardsTarget) {
				var d = Vector3.Distance(transform.position, target.position);
				if (d < 20)
				{
					var angle = (tempTarget - transform.position).normalized;
					transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(angle),
					Time.deltaTime * rotateSpeed);

					//Random Shooting
					fireTimer += Time.deltaTime;

					if (fireTimer > fireTimerTotal)
					{
						tempTarget = target.position + new Vector3(Random.Range(0, firingRandomnize), Random.Range(0, firingRandomnize), Random.Range(0, firingRandomnize));
						FireBullet();
						fireTimer = 0;
						fireTimerTotal = Random.Range(timerTotalRange.x, timerTotalRange.y);
					}
				}
			}
			else {
				//Regular interval Shooting 
				fireCounter -= Time.deltaTime;

				if(fireCounter <= 0f) {
					FireBullet();
					fireCounter = 1f / fireSpeed;
				}
			}
		}

        public void FireBullet() {
            pool.Spawn(bulletPrefab.name, firePoint.position, Quaternion.Euler(firePoint.eulerAngles));
			Instantiate(firingParticle, firePoint.position, firePoint.rotation);
        }

    }
}
