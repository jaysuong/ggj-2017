﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyTransform : MonoBehaviour {
	public Transform headTransform;
	public bool IsApplyRotation = false;
	public Vector3 offset = new Vector3();
	
	// Update is called once per frame
	void Update () {
		transform.position = headTransform.position + offset;
		if(IsApplyRotation)
		{
			transform.rotation = headTransform.rotation;
		}
	}
}
