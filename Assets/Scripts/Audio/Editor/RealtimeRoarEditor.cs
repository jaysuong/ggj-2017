using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace GGJ {
    [CustomEditor(typeof(RealtimeRoar))]
    public class RealtimeRoarEditor : Editor {
        private RealtimeRoar roar;

        private void OnEnable() {
            roar = target as RealtimeRoar;
            EditorApplication.update += Repaint;
        }

        private void OnDisable() {
            EditorApplication.update -= Repaint;
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            EditorGUILayout.Space();

            var rect = EditorGUILayout.BeginVertical();
            EditorGUI.ProgressBar(rect, roar.AverageVolume, string.Format("Volume: {0}", roar.AverageVolume));
            GUILayout.Space(16f);
            EditorGUILayout.EndVertical();
        }
    }
}
