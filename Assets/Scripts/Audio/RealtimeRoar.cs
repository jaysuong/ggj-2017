using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ {
    public class RealtimeRoar : MonoBehaviour {
        [SerializeField]
        private AudioSource source;
        [SerializeField, Range(0, 1)]
        private float minRoarThreshold = 0.5f;

        public float AverageVolume { get { return average; } }

        public event RoarEventHandler OnRoarStart;
        public event RoarEventHandler OnRoarEnd;

        public delegate void RoarEventHandler();

        private AudioClip clip;
        private const int ClipLength = 1;
        private float[] samples;

        private float average;
        private bool wasRoarStarted;

        private void Start() {
            clip = Microphone.Start(string.Empty, true, ClipLength, 16000);
            source.clip = clip;
            while(!( Microphone.GetPosition(null) > 0)) { }

            samples = new float[clip.samples];
        }

        private void Update() {
            CalculateAverage();

            if(!wasRoarStarted) {
                if(average > minRoarThreshold) {
                    wasRoarStarted = true;

                    if(OnRoarStart != null) {
                        OnRoarStart();
                    }
                }
            }
            else {
                if(average < minRoarThreshold) {
                    wasRoarStarted = false;

                    if(OnRoarEnd != null) {
                        OnRoarEnd();
                    }
                }
            }
        }

        private void CalculateAverage() {            
            clip.GetData(samples, 0);

            var total = 0f;
            int count = 0;

            for(var i = 0; i < samples.Length; ++i) {
                if(samples[i] > 0) {
                    total += samples[i];
                    count++;
                }
            }

            average = total / count;
        }
    }
}
