using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InitialPrefabs.Core;

namespace GGJ {
    public class Explosion : MonoBehaviour {
        [SerializeField]
        private float damage = 100f;
        [SerializeField]
        private LayerMask explosionMask;
        [SerializeField]
        private float distance = 5;
        [SerializeField]
        private ParticleSystem explosionParticle;


        private void OnCollisionEnter(Collision c) {
            if(explosionParticle != null) {
                Instantiate(explosionParticle, transform.position, transform.rotation);
            }

            var colliders = Physics.OverlapSphere(transform.position, distance, explosionMask);
            foreach(var collider in colliders) {
                var hp = collider.GetComponent<Health>();

                if(hp != null) {
                    hp.Damage(damage);
                }
            }

            gameObject.SetActive(false);
        }
    }
}
