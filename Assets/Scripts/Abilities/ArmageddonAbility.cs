using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ {
    /// <summary>
    /// An ability that rains hell on enemies
    /// </summary>
    public class ArmageddonAbility : MonoBehaviour {
        [SerializeField]
        Transform[] hands;
        [SerializeField]
        private float minHandHeight = 1f;

        [SerializeField]
        private RealtimeRoar roar;
        [SerializeField]
        private float minRoarTime = 0.5f;

        [SerializeField]
        private ObjectPool pool;
        [SerializeField]
        private GameObject projectile;
        [SerializeField]
        private Transform spawnPoint;
        [SerializeField]
        private Vector3 spread;


        private float startTime;
        private bool shouldSpawn;


        private void OnEnable() {
            roar.OnRoarStart += HandleRoarStart;
            roar.OnRoarEnd += HandleRoarEnd;
        }

        private void OnDisable() {
            roar.OnRoarStart -= HandleRoarStart;
            roar.OnRoarEnd -= HandleRoarEnd;
        }

        private void Update() {
            if(shouldSpawn && Time.timeSinceLevelLoad > startTime + minRoarTime) {
                var areHandsRaised = true;

                for(var i = 0; i < hands.Length; ++i) {
                    if(hands[i].position.y < minHandHeight) {
                        areHandsRaised = false;
                        break;
                    }
                }

                if(areHandsRaised) {
                    // Launch a projectile
                    pool.Spawn(projectile.name, spawnPoint.position + new Vector3(spread.x * Random.value, spread.y * Random.value, spread.z * Random.value), 
                        spawnPoint.rotation);
                    startTime = Time.timeSinceLevelLoad;
                }
            }
        }


        private void HandleRoarStart() {
            shouldSpawn = true;
        }

        private void HandleRoarEnd() {
            shouldSpawn = false;
        }
    }
}
