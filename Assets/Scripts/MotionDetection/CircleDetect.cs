﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleDetect : MonoBehaviour {

    float AngleRadius = 30; 
    float AngleDelta = 15;  // degrees

    Vector3 EpicenterRotation;
    Vector3 EpicenterRotationOffset;
    Vector3[] Checkpoints = new Vector3[6];

    float[,] CheckPointX = new float[6,2];
    float[,] CheckPointY = new float[6,2];
    float[,] CheckPointZ = new float[6,2];

    bool[] CheckpointReached = new bool[6];
    int NumCheckpointsReached = 0;

    int currentPoint = 0;
    float timer = 0.5f;

    // Use this for initialization
    void Start () {
        EpicenterRotation = transform.eulerAngles;

        Vector3 aPerpendicularVector = new Vector3(-EpicenterRotation.x, EpicenterRotation.y, 0);

        EpicenterRotationOffset = Quaternion.AngleAxis(AngleRadius, aPerpendicularVector) * EpicenterRotation;

        for (int i = 0; i < 6; i++)
        {
            CheckpointReached[i] = false;
            NumCheckpointsReached = 0;

            //Checkpoints[i] = Quaternion.AngleAxis(i * 60, EpicenterRotation) * EpicenterRotationOffset;
            Checkpoints[i] = Quaternion.AngleAxis(i * 60, EpicenterRotation) * EpicenterRotationOffset;

            //Checkpoints[i] = Quaternion.Euler(Checkpoints[i]).eulerAngles;   // wrap to [0, 360]


            Debug.Log(Checkpoints[i]);

            CheckPointX[i, 0] = Checkpoints[i].x - AngleDelta;
            CheckPointX[i, 1] = Checkpoints[i].x + AngleDelta;
            //if (CheckPointX[i, 0] < 0) CheckPointX[i, 0] += 360;
            //if (CheckPointX[i, 1] > 360) CheckPointX[i, 1] -= 360;

            CheckPointY[i, 0] = Checkpoints[i].y - AngleDelta;
            CheckPointY[i, 1] = Checkpoints[i].y + AngleDelta;
            //if (CheckPointY[i, 0] < 0) CheckPointY[i, 0] += 360;
            //if (CheckPointY[i, 1] > 360) CheckPointY[i, 1] -= 360;

            CheckPointZ[i, 0] = Checkpoints[i].z - AngleDelta;
            CheckPointZ[i, 1] = Checkpoints[i].z + AngleDelta;
            //if (CheckPointZ[i, 0] < 0) CheckPointZ[i, 0] += 360;
            //if (CheckPointZ[i, 1] > 360) CheckPointZ[i, 1] -= 360;

            //Debug.Log(string.Format("{0}, {1}", CheckPointX[i, 0], CheckPointX[i, 1]));
            //Debug.Log(string.Format("{0}, {1}", CheckPointY[i, 0], CheckPointY[i, 1]));
            //Debug.Log(string.Format("{0}, {1}", CheckPointZ[i, 0], CheckPointZ[i, 1]));
        }
    }
	
	// Update is called once per frame
	void Update () {
        

        timer -= Time.deltaTime;
        if (timer < 0)
        {
            timer = 1f;
            currentPoint++;
            if (currentPoint == 6) currentPoint = 0;
            transform.rotation = Quaternion.Euler(Checkpoints[currentPoint]);
            //Debug.Log(transform.eulerAngles);
        }

        // Checking if arm crosses checkpoints:
        for (int i = 0; i < 6; i++)
        {
            if (CheckpointReached[i] == false)
            {

                //Debug.Log(string.Format("{0}, {1}, {2}", CheckPointX[i, 0], CheckPointX[i, 1], transform.eulerAngles.x));
                //Debug.Log(string.Format("{0}, {1}, {2}", CheckPointY[i, 0], CheckPointY[i, 1], transform.eulerAngles.y));
                //Debug.Log(string.Format("{0}, {1}, {2}", CheckPointY[i, 0], CheckPointY[i, 1], transform.eulerAngles.z));

                //Debug.Log(string.Format("{0}, {1}, {2}", isbetween(checkpointx[i, 0], checkpointx[i, 1], transformeulerx),
                //isbetween(checkpointy[i, 0], checkpointy[i, 1], transformeulery),
                //isbetween(checkpointz[i, 0], CheckPointZ[i, 1], transformEulerZ)));

                //if (isBetween(CheckPointX[i, 0], CheckPointX[i, 1], transform.eulerAngles.x) &
                //    isBetween(CheckPointY[i, 0], CheckPointY[i, 1], transform.eulerAngles.y) &
                //    isBetween(CheckPointZ[i, 0], CheckPointZ[i, 1], transform.eulerAngles.z))

                if ( closeEnoughAngle(transform.eulerAngles, Checkpoints[i]) )
                {
                    Debug.Log("here");
                    CheckpointReached[i] = true;
                    NumCheckpointsReached++;
                }
            }
        }

        if (NumCheckpointsReached == 6)
        {
            NumCheckpointsReached = 0;  // Or let the user circle indefinitely.

            // Do callback.
            Debug.Log("User drew hand in a circle");
        }
    }

    bool closeEnoughAngle(Vector3 a, Vector3 b)
    {
        //Debug.Log(Mathf.Acos(Vector3.Dot(a, b) / (a.magnitude * b.magnitude)));
        //Debug.Log(Mathf.Acos(AngleDelta / 180 * Mathf.PI));
        if (Mathf.Acos(Vector3.Dot(a, b) / (a.magnitude * b.magnitude)) < Mathf.Acos(AngleDelta / 180 * Mathf.PI))
            return true;
        else
            return false;
    }

    bool isBetween(float lo, float hi, float val)
    {
        if (lo < 0) lo += 180;
        if (hi < 0) hi += 180;

        if ((val >= lo) & (val <= hi))
            return true;

        else
            return false;
    }

    // Callback for epicenter
    //Vector3
    //EpicenterRotationOffset = Quaternion.AngleAxis(AngleRadius, )
    //for (int i = 1; i < 6; i++) {

    // }
    //// EpicenterRotationOffset = Quaternion.AngleAxis(angle, RotationAxis)

    //transform.rotation = Quaternion.Euler(Quaternion.AngleAxis(angle, RotationAxis) * RotationAxisO);

    // CheckPointX1 = 

    //vector = Quaternion.AngleAxis(-45, Vector3.up) * vector;
}