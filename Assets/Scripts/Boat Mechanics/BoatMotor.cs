using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InitialPrefabs.Core;

namespace GGJ {
    public class BoatMotor : MonoBehaviour {
        [SerializeField]
        private Rigidbody rigid;
		[SerializeField]
		private float speed = 10;
		[SerializeField]
		private ReferenceBank bank;
		[SerializeField]
		private string targetName;
		private Transform target;
		[SerializeField]
		private float stopDistance = 3;

		private void Start()
		{
			target = bank.GetReference<Transform>(targetName);
		}

		private void Update()
		{
			var r = target.position;
			r.y = transform.position.y;
			transform.LookAt(r);

			if (Vector3.Distance(r, transform.position) > stopDistance)
			{
				var v = target.position - transform.position;
				rigid.AddForce(v * speed * Time.deltaTime);
			}
        }


    }
}
