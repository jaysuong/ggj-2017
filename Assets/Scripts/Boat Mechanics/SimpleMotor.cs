using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InitialPrefabs.Core;

namespace GGJ {
    /// <summary>
    /// A simple motor whose purpose is to move in one and only one direction
    /// </summary>
    public class SimpleMotor : MonoBehaviour {
        [SerializeField]
        private float speed = 1f;
		[SerializeField]
		private ReferenceBank bank;
		[SerializeField]
		private string targetName;
		private Transform target;
		[SerializeField]
		private Vector2 spawnHeight = new Vector2(3, 6.01f);
		[SerializeField]
		private float stopDistance = 3;

		private void Start()
		{
			target = bank.GetReference<Transform>(targetName);
			transform.position += new Vector3(0, Random.Range(spawnHeight.x, spawnHeight.y), 0);
		}

		private void Update()
		{
			var r = target.position;
			r.y = transform.position.y;
			transform.LookAt(r);
			if (Vector3.Distance(r, transform.position) > stopDistance) {
				transform.position += transform.forward * speed * Time.deltaTime;
			}
        }

    }
}
