﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroyParticle : MonoBehaviour {
	ParticleSystem pS;
	float timer = 0;
	// Use this for initialization
	void Start () {
		pS = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		if(timer > pS.main.duration + 3)
		{
			Destroy(gameObject);
		}
		else
		{
			timer += Time.deltaTime;
		}
	}
}
