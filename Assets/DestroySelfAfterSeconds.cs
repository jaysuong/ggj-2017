﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySelfAfterSeconds : MonoBehaviour {

	public float seconds = 6;
	// Use this for initialization
	void Start () {
		Invoke("DestroySelf", seconds);
	}
	
	// Update is called once per frame
	void DestroySelf() {
		Destroy(gameObject);
	}
}
