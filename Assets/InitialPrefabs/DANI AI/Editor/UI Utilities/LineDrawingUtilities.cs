using UnityEngine;
using UnityEditor;

namespace InitialPrefabs.DANI.Editor {
    public static class LineDrawingUtilities {

        /// <summary>
        /// Draws a straight line from one rect to another
        /// </summary>
        public static void DrawLine(Rect start, Rect end) {
            var startCenter = new Vector3(start.x + start.width / 2f,
                start.y + start.height / 2f, 0);
            var endCenter = new Vector3(end.x + end.width / 2f,
                end.y + end.height / 2f, 0);
            Handles.DrawLine(startCenter, endCenter);
        }

        /// <summary>
        /// Draws a straight line from one rect to another
        /// </summary>
        public static void DrawLine(Rect start, Vector2 end) {
            var startCenter = new Vector3(start.x + start.width / 2f,
                start.y + start.height / 2f, 0);
            Handles.DrawLine(startCenter, end);
        }

        public static void DrawLine(Vector2 start, Vector2 end) {
            Handles.DrawLine(start, end);
        }

        public static void DrawLine(Rect start, Rect end, Vector2 offset) {
            var startCenter = new Vector3(start.x + start.width / 2f + offset.x,
                start.y + start.height / 2f + offset.y, 0);
            var endCenter = new Vector3(end.x + end.width / 2f + offset.x,
                end.y + end.height / 2f + offset.y, 0);
            Handles.DrawLine(startCenter, endCenter);
        }


        /// <summary>
        /// Draws a bezier curve from one rect to another
        /// </summary>
        public static void DrawBezierCurve(Rect start, Rect end) {
            var startPos = new Vector3(start.x + start.width, start.y + start.height / 2, 0);
            var endPos = new Vector3(end.x, end.y + end.height / 2, 0);
            var startTan = startPos + Vector3.right * 50;
            var endTan = endPos + Vector3.left * 50;
            var shadowCol = new Color(0, 0, 0, 0.06f);
            for(var i = 0; i < 3; i++) // Draw a shadow
                Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, ( i + 1 ) * 5);
            Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.black, null, 1);
        }

        public static void DrawBezierCurve(Rect start, Rect end, Vector2 offset) {
            var startPos = new Vector3(start.x + start.width + offset.x, 
                start.y + start.height / 2 + offset.y, 0);
            var endPos = new Vector3(end.x + offset.x, end.y + end.height / 2 + offset.y, 0);
            var startTan = startPos + Vector3.right * 50;
            var endTan = endPos + Vector3.left * 50;
            var shadowCol = new Color(0, 0, 0, 0.06f);
            for(var i = 0; i < 3; i++) // Draw a shadow
                Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, ( i + 1 ) * 5);
            Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.black, null, 1);
        }

        public static void DrawBezierCurve(Rect start, Rect end, Vector2 offset, Color color) {
            var startPos = new Vector3(start.x + start.width + offset.x,
                start.y + start.height / 2 + offset.y, 0);
            var endPos = new Vector3(end.x + offset.x, end.y + end.height / 2 + offset.y, 0);
            var startTan = startPos + Vector3.right * 50;
            var endTan = endPos + Vector3.left * 50;
            var shadowCol = new Color(color.r, color.g, color.b, 0.06f);
            for(var i = 0; i < 3; i++) // Draw a shadow
                Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, ( i + 1 ) * 5);
            Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, 1);
        }

        public static void DrawBezierCurve(Rect start, Rect end, Vector2 offset, Color color, float width) {
            var startPos = new Vector3(start.x + start.width + offset.x,
                start.y + start.height / 2 + offset.y, 0);
            var endPos = new Vector3(end.x + offset.x, end.y + end.height / 2 + offset.y, 0);
            var startTan = startPos + Vector3.right * 50;
            var endTan = endPos + Vector3.left * 50;
            var shadowCol = new Color(color.r, color.g, color.b, 0.06f);
            for(var i = 0; i < 3; i++) // Draw a shadow
                Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, ( i + 1 ) * 5);
            Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, width);
        }

        public static void DrawBoxOnBezier(Rect start, Rect end, Vector2 offset, Vector2 size, string message) {
            var startPos = new Vector3(start.x + start.width + offset.x,
                start.y + start.height / 2 + offset.y, 0);
            var endPos = new Vector3(end.x + offset.x, end.y + end.height / 2 + offset.y, 0);
            var midPos = Vector2.Lerp(startPos, endPos, 0.5f);

            var box = new Rect(midPos.x - size.x / 2f, midPos.y - size.y / 2f, size.x, size.y);

            GUI.Box(box, message);
        }

        public static void DrawBoxOnBezier(Rect start, Rect end, Vector2 offset, Vector2 size, Color color, string message) {
            var oldColor = GUI.color;
            GUI.color = color;

            var startPos = new Vector3(start.x + start.width + offset.x,
                start.y + start.height / 2 + offset.y, 0);
            var endPos = new Vector3(end.x + offset.x, end.y + end.height / 2 + offset.y, 0);
            var midPos = Vector2.Lerp(startPos, endPos, 0.5f);

            var box = new Rect(midPos.x - size.x / 2f, midPos.y - size.y / 2f, size.x, size.y);

            GUI.Box(box, message);

            GUI.color = oldColor;
        }

    }
}
