using UnityEngine;
using UnityEditor;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// A simple component used to draw the background grid
    /// </summary>
    public class GridBackground {
        public const float GridCellSize = 15f;

        private Texture2D backgroundTexture;
        private Texture2D observerTexture;
        private Texture2D decisionTexture;
        private Texture2D actionTexture;
        private DaniEditorSettings settings;

        private const float GuideSize = 300f;


        public GridBackground(DaniEditorSettings settings) {
            this.settings = settings;
            var color = EditorGUIUtility.isProSkin ?
                settings.GridDarkSkinBackgroundColor : settings.GridLightSkinBackgroundColor;
            CreateTexture(ref backgroundTexture, color);
            CreateTexture(ref observerTexture, color);
            CreateTexture(ref decisionTexture, color);
            CreateTexture(ref actionTexture, color);
        }


        public void DrawGrid(Rect rect, Vector2 scroll, bool drawGuideLines) {
            if(Event.current.type == EventType.Repaint) {
                // Draw the background
                if(backgroundTexture == null) {
                    var backgroundColor = EditorGUIUtility.isProSkin ?
                        settings.GridDarkSkinBackgroundColor : settings.GridLightSkinBackgroundColor;
                    CreateTexture(ref backgroundTexture, backgroundColor);
                }
                else {
                    var backgroundColor = EditorGUIUtility.isProSkin ? 
                        settings.GridDarkSkinBackgroundColor : settings.GridLightSkinBackgroundColor;

                    var currentColor = backgroundTexture.GetPixel(0, 0);
                    if(backgroundColor != currentColor) {
                        SetColor(backgroundTexture, backgroundColor);
                    }
                }
                GUI.DrawTextureWithTexCoords(rect, backgroundTexture,
                        new Rect(0, 0, rect.width / backgroundTexture.width, rect.height / backgroundTexture.height));

                var color = ( EditorGUIUtility.isProSkin ) ?
                    settings.GridDarkSkinLineColor : settings.GridLightSkinLineColor;

                // Push Matrix, and add instructions
                GL.PushMatrix();
                GL.Begin(GL.LINES);

                GL.Color(color);
                for(var x = rect.xMin - ( rect.xMin % GridCellSize ) + scroll.x; x < rect.xMax; x += GridCellSize) {
                    if(x < rect.xMin) {
                        continue;
                    }

                    // Draw lines from one point to another
                    DrawLineVertexes(new Vector2(x, rect.yMin), new Vector2(x, rect.yMax));
                }

                GL.Color(color);
                for(var y = rect.yMin - ((rect.yMin - scroll.y) % GridCellSize ); y < rect.yMax; y += GridCellSize) {
                    if(y < rect.yMin) {
                        continue;
                    }

                    DrawLineVertexes(new Vector2(rect.xMin, y), new Vector2(rect.xMax, y));
                }

                GL.End();
                GL.PopMatrix();

                // Draw the guide lines, if possible
                if(drawGuideLines) {
                    DrawGuides(rect, scroll);
                }
            }
        }

        private void DrawLineVertexes(Vector2 start, Vector2 end) {
            GL.Vertex(start);
            GL.Vertex(end);
        }

        private void CreateTexture(ref Texture2D texture, Color color) {
            texture = new Texture2D(32, 32);
            

            for(var x = 0; x < texture.width; ++x) {
                for(var y = 0; y < texture.height; ++y) {
                    texture.SetPixel(x, y, color);
                }
            }

            texture.Apply();
        }

        private void SetColor(Texture2D texture, Color color) {
            for(var x = 0; x < texture.width; ++x) {
                for(var y = 0; y < texture.height; ++y) {
                    texture.SetPixel(x, y, color);
                }
            }

            texture.Apply();
        }


        private void DrawGuides(Rect rect, Vector2 scroll) {
            PrepareGuideTextures();

            using(var group = new GUI.GroupScope(rect)) {
                // Draw observer guides
                GUI.DrawTextureWithTexCoords(new Rect(scroll.x, 0f, GuideSize, rect.height), 
                    observerTexture, 
                    new Rect(0f, 0f, rect.width / observerTexture.width, rect.height / observerTexture.height));
                GUI.Label(new Rect(scroll.x, 0f, GuideSize, 20f),
                    "Observers", EditorStyles.whiteLargeLabel);

                // Draw decision guides
                GUI.DrawTextureWithTexCoords(new Rect(GuideSize + scroll.x, 0f, GuideSize, rect.height),
                    decisionTexture,
                    new Rect(0f, 0f, rect.width / decisionTexture.width, rect.height / decisionTexture.height));
                GUI.Label(new Rect(GuideSize + scroll.x, 0f, GuideSize, 20f),
                    "Decisions", EditorStyles.whiteLargeLabel);

                // Draw Action guides
                GUI.DrawTextureWithTexCoords(new Rect(GuideSize * 2f + scroll.x, 0f, GuideSize, rect.height),
                    actionTexture,
                    new Rect(0f, 0f, rect.width / actionTexture.width, rect.height / actionTexture.height));
                GUI.Label(new Rect(GuideSize * 2f + scroll.x, 0f, GuideSize, 20f),
                    "Actions", EditorStyles.whiteLargeLabel);
            }
        }

        private void PrepareGuideTextures() {
            var obsColor = settings.ObserverGuideColor;
            if(observerTexture == null) {
                CreateTexture(ref observerTexture, obsColor);
            }
            else {
                if(obsColor != observerTexture.GetPixel(0, 0)) {
                    SetColor(observerTexture, obsColor);
                }
            }

            var decColor = settings.DecisionGuideColor;
            if(decisionTexture == null) {
                CreateTexture(ref decisionTexture, decColor);
            }
            else if(decColor != decisionTexture.GetPixel(0, 0)) {
                SetColor(decisionTexture, decColor);
            }

            var actColor = settings.ActionGuideColor;
            if(actionTexture == null) {
                CreateTexture(ref actionTexture, actColor);
            }
            else if(actColor != actionTexture.GetPixel(0, 0)) {
                SetColor(actionTexture, actColor);
            }
        }
    }
}
