using UnityEngine;
using System.Collections;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// Editor utility class for drawing rectangles
    /// </summary>
    public static class RectUtilities {
        
        public static Rect GetCenterLeft(Rect rect, float width, float height) {
            return new Rect(
                rect.x - width,
                rect.y + (rect.height / 2f - height / 2f),
                width,
                height);
        }

        public static Rect GetCenterLeft(Rect rect, float width, float height, Vector2 offset) {
            return new Rect(
                rect.x - width + offset.x,
                rect.y + ( rect.height / 2f - height / 2f ) + offset.y,
                width,
                height);
        }

        public static Rect GetCenterRight(Rect rect, float width, float height) {
            return new Rect(
                rect.x + rect.width,
                rect.y + (rect.height / 2f - height / 2f),
                width,
                height);
        }

        public static Rect GetCenterRight(Rect rect, float width, float height, Vector2 offset) {
            return new Rect(
                rect.x + rect.width + offset.x,
                rect.y + ( rect.height / 2f - height / 2f ) + offset.y,
                width,
                height);
        }
    }
}