using UnityEngine;
using UnityEditor;
using InitialPrefabs.DANI;
using System;
using System.Collections.Generic;
using System.Linq;


namespace InitialPrefabs.DANI.Editor {
    using Object = UnityEngine.Object;

    public class NoVirtualModuleException : Exception { }

    /// <summary>
    /// A virtual editor version of the AIDiagram.
    /// </summary>
    [Serializable]
    public class VirtualEditorDiagram {
        /// <summary>
        /// The current diagram
        /// </summary>
        public AIDiagram Diagram { get { return diagramInstance; } }

        /// <summary>
        /// The serialized object representation of AIDiagram
        /// </summary>
        public SerializedObject SerializedDiagram {
            get {
                if(serializedDiagram == null) {
                    serializedDiagram = new SerializedObject(diagramInstance);
                }

                return serializedDiagram;
            }
        }

        /// <summary>
        /// Is this virtual version a valid version?
        /// </summary>
        public bool IsValid {
            get { return Diagram != null && SerializedDiagram != null &&
                    modules != null && observers != null; }
        }

        public bool IsInternalsValid {
            get { return observers != null && decisions != null && actions != null && conditions != null; }
        }

        public VirtualModule[] Modules { get { return modules.ToArray(); } }
        public List<VirtualVariable> Variables { get { return variables; } }
        public bool IsRuntime { get; private set; }

        [SerializeField]
        private List<VirtualModule> modules;
        [SerializeField]
        private List<VirtualVariable> variables;

        [SerializeField]
        private AIDiagram diagramInstance;

        private Dictionary<string, VirtualModule> observers;
        private Dictionary<string, VirtualModule> decisions;
        private Dictionary<string, VirtualModule> actions;
        private Dictionary<string, VirtualModule> conditions;

        private SerializedObject serializedDiagram;


        public VirtualEditorDiagram(AIDiagram diagram) {
            diagramInstance = diagram;
            serializedDiagram = new SerializedObject(diagramInstance);

            IsRuntime = string.IsNullOrEmpty(AssetDatabase.GetAssetPath(diagramInstance));

            modules = new List<VirtualModule>();
            variables = new List<VirtualVariable>();
            Setup();
        }

        private void Setup() {
            observers = new Dictionary<string, VirtualModule>();
            decisions = new Dictionary<string, VirtualModule>();
            actions = new Dictionary<string, VirtualModule>();
            conditions = new Dictionary<string, VirtualModule>();

            // Setup flag to see if the diagram is dirty while building
            var isDirty = false;

            foreach(var obs in Diagram.Observers) {
                if(obs != null) {
                    var vmod = new VirtualModule(VirtualModuleType.Observer, obs);
                    observers.Add(vmod.id, vmod);
                }
                else {
                    isDirty = true;
                }
            }
            
            foreach(var dec in Diagram.Decisions) {
                if(dec != null) {
                    var vmod = new VirtualModule(VirtualModuleType.Decision, dec);
                    decisions.Add(vmod.id, vmod);
                }
                else {
                    isDirty = true;
                }
            }

            foreach(var act in Diagram.Actions) {
                if(act != null) {
                    var vmod = new VirtualModule(VirtualModuleType.Action, act);
                    actions.Add(vmod.id, vmod);
                }
                else {
                    isDirty = true;
                }
            }

            foreach(var con in Diagram.Conditions) {
                if(con != null) {
                    var vmod = new VirtualModule(VirtualModuleType.Condition, con);
                    conditions.Add(vmod.id, vmod);
                }
                else {
                    isDirty = true;
                }
            }

            // Sanitize the diagram if its dirty
            if(isDirty) {
                Diagram.Sanitize();
            }

            if(!IsRuntime) {
                foreach(var pointer in Diagram.ConditionPointers) {
                    if(pointer.isSimplePointer) {
                        try {
                            var decision = decisions[pointer.sourceId];
                            decision.connectionIds.Add(pointer.targetId);
                        }
                        catch { continue; }
                    }
                    else {
                        try {
                            var observer = observers[pointer.sourceId];
                            observer.connectionIds.Add(pointer.targetId);

                            var condition = conditions[pointer.conditionId];

                            var decision = decisions[pointer.targetId];
                            ( (DecisionModule)decision.module ).AddConditional(
                                new Conditional(observer.module as ObserverModule, condition.module as ConditionModule));
                        }
                        catch { continue; }
                    }
                }
            }

            modules.AddRange(observers.Values);
            modules.AddRange(decisions.Values);
            modules.AddRange(actions.Values);

            // Set up variables
            variables = new List<VirtualVariable>();
            var varList = Diagram.Variables;

            foreach(var variable in varList) {
                variables.Add(new VirtualVariable(variable));
            }
        }

        public void AddObserver(ObserverModule obs) {
            Diagram.AddObserver(obs);
            obs.hideFlags = HideFlags.HideInHierarchy;
            AssetDatabase.AddObjectToAsset(obs, Diagram);
            AssetDatabase.SaveAssets();
            
            var virtualObs = new VirtualModule(VirtualModuleType.Observer, obs);
            observers.Add(obs.Id, virtualObs);
            modules.Add(virtualObs);
        }

        public void AddDecision(DecisionModule dec) {
            Diagram.AddDecision(dec);
            dec.hideFlags = HideFlags.HideInHierarchy;
            AssetDatabase.AddObjectToAsset(dec, Diagram);
            AssetDatabase.SaveAssets();

            var virtualDec = new VirtualModule(VirtualModuleType.Decision, dec);
            decisions.Add(dec.Id, virtualDec);
            modules.Add(virtualDec);
        }

        public void AddAction(ActionModule act) {
            Diagram.AddAction(act);
            act.hideFlags = HideFlags.HideInHierarchy;
            AssetDatabase.AddObjectToAsset(act, Diagram);
            AssetDatabase.SaveAssets();

            var virtualAct = new VirtualModule(VirtualModuleType.Action, act);
            actions.Add(act.Id, virtualAct);
            modules.Add(virtualAct);
        }

        public void AddCondition(VirtualModule source, VirtualModule target) {
            Diagram.AddCondition(source.id, target.id);
            AssetDatabase.SaveAssets();
        }

        public void AddCondition(VirtualModule source, VirtualModule target, ConditionModule condition) {
            Diagram.AddCondition(condition, source.id, target.id);
            condition.hideFlags = HideFlags.HideInHierarchy;
            AssetDatabase.AddObjectToAsset(condition, Diagram);
            AssetDatabase.SaveAssets();

            var virtualCondition = new VirtualModule(VirtualModuleType.Condition, condition);
            conditions.Add(condition.Id, virtualCondition);
        }

        public void AddVariable(Variable variable) {
            Diagram.AddVariable(variable);
            variable.hideFlags = HideFlags.HideInHierarchy;
            AssetDatabase.AddObjectToAsset(variable, Diagram);
            AssetDatabase.SaveAssets();

            variables.Add(new VirtualVariable(variable));
        }

        public void RemoveObserver(VirtualModule module) {
            Diagram.RemoveObserver(module.module as ObserverModule);
            observers.Remove(module.id);
            modules.Remove(module);

            Object.DestroyImmediate(module.module, true);
            AssetDatabase.SaveAssets();
        }

        public void RemoveDecision(VirtualModule module) {
            Diagram.RemoveDecision(module.module as DecisionModule);
            decisions.Remove(module.id);
            modules.Remove(module);

            Object.DestroyImmediate(module.module, true);
            AssetDatabase.SaveAssets();
        }

        public void RemoveAction(VirtualModule module) {
            Diagram.RemoveAction(module.module as ActionModule);
            actions.Remove(module.id);
            modules.Remove(module);

            Object.DestroyImmediate(module.module, true);
            AssetDatabase.SaveAssets();
        }

        public void RemoveVariable(Variable variable) {
            Diagram.RemoveVariable(variable);

            Object.DestroyImmediate(variable, true);
            AssetDatabase.SaveAssets();

            variables.RemoveAll(v => v.Variable == variable);
        }

        public VirtualModule GetModule(string id) {
            if(!IsInternalsValid) {
                Setup();
            }

            if(observers.ContainsKey(id)) {
                return observers[id];
            }

            if(decisions.ContainsKey(id)) {
                return decisions[id];
            }

            if(actions.ContainsKey(id)) {
                return actions[id];
            }

            if(conditions.ContainsKey(id)) {
                return conditions[id];
            }

            throw new NoVirtualModuleException();
        }

        public SerializedObject GetSerializedVariable(Variable variable) {
            // TODO: Speed up access times for variables
            var result = variables.Where(v => v.Variable == variable);
            if(result.Any()) {
                return result.First().SerializedVariable;
            }
            else {
                return null;
            }
        }

        public float GetMaxWeightOfTarget(string id) {
            var weight = 0f;

            for(var i = 0; i < diagramInstance.ConditionPointers.Length; ++i) {
                var pointer = diagramInstance.ConditionPointers[i];

                if(pointer.targetId == id) {
                    var condition = GetModule(pointer.conditionId);
                    if(condition != null) {
                        weight += ( condition.module as ConditionModule ).Weight;
                    }
                }
            }

            return weight;
        }

        public bool AreModulesRelated(AIModule a, AIModule b) {
            return ( from p in diagramInstance.ConditionPointers
                     where p != null
                     where (p.sourceId == a.Id && p.targetId == b.Id) || (p.sourceId == b.Id && p.targetId == a.Id)
                     select p).Any();
        }

        /// <summary>
        /// Checks if the diagram already has a connection between two nodes
        /// </summary>
        /// <param name="left">The left node to check</param>
        /// <param name="right">The right node to check</param>
        /// <returns>True if there is already a connection</returns>
        public bool HasConnection(AIModule left, AIModule right) {
            return ( from c in diagramInstance.ConditionPointers
                     where c.sourceId == left.Id
                     where c.targetId == right.Id
                     select c ).Any();
        }

        public bool ContainsModuleReference(AIModule module) {
            return ( from m in modules
                     where m.module.GetInstanceID() == module.GetInstanceID()
                     select m ).Any();
        }
        
    }
}
