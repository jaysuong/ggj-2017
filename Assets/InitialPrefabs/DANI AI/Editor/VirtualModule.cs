using UnityEngine;
using System.Collections.Generic;
using InitialPrefabs.DANI;
using System;
using UnityEditor;

namespace InitialPrefabs.DANI.Editor {
    public enum VirtualModuleType { Observer, Decision, Action, Condition }
    
    /// <summary>
    /// A simple collection of modules
    /// </summary>
    [Serializable]
    public class VirtualModule {
        public VirtualModuleType type;
        public AIModule module;
        public string id;
        public List<string> connectionIds;

        public Rect Position {
            get {
                SerializedObject.Update();
                var localPosition = SerializedObject.FindProperty(PositionProperty).vector2Value;
                return new Rect(localPosition, size);
            }
            set {
                SerializedObject.Update();
                SerializedObject.FindProperty(PositionProperty).vector2Value = value.position;
                SerializedObject.ApplyModifiedProperties();
            }
        }

        public Vector2 Size {
            get { return size; }
            set { size = value; }
        }

        public SerializedObject SerializedObject {
            get {
                if(serializedObject == null) {
                    serializedObject = new SerializedObject(module);
                }
                return serializedObject;
            }
        }

        public Rect LeftStub { get { return leftStub; } }
        public Rect RightStub { get { return rightStub; } }
        public Color NodeColor { get; set; }

        private Vector2 size;
        private SerializedObject serializedObject;

        [SerializeField]
        private Rect leftStub;
        [SerializeField]
        private Rect rightStub;

        private const string PositionProperty = "modulePosition";
        private const float StubGUISize = 15f;


        public VirtualModule(VirtualModuleType type, AIModule module) {
            this.type = type;
            this.module = module;
            id = module.Id;
            connectionIds = new List<string>();
            serializedObject = new SerializedObject(module);

            switch(type) {
                case VirtualModuleType.Observer:
                    size = new Vector2(170f, 60f);
                    break;

                case VirtualModuleType.Decision:
                    size = new Vector2(200f, 80f);
                    break;

                case VirtualModuleType.Action:
                    size = new Vector2(150f, 50f);
                    break;

                default:
                    size = new Vector2();
                    break;
            }
        }

        public void CalculateStubs(Vector2 offset) {
            leftStub = RectUtilities.GetCenterLeft(Position, StubGUISize, StubGUISize, offset);
            rightStub = RectUtilities.GetCenterRight(Position, StubGUISize, StubGUISize, offset);
        }

        public bool ContainsStub(Vector2 position) {
            switch (type) {
                case VirtualModuleType.Observer:
                    return rightStub.Contains(position);

                case VirtualModuleType.Decision:
                    return leftStub.Contains(position) || rightStub.Contains(position);

                case VirtualModuleType.Action:
                    return leftStub.Contains(position);

                default:
                    return false;
            }
        }
    }
}
