using UnityEngine;
using UnityEditor;
using System;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// A simple container containing a variable and its serialized object
    /// </summary>
    [Serializable]
    public struct VirtualVariable {
        public Variable Variable { get { return variable; } }
        public SerializedObject SerializedVariable {
            get {
                if(serializedVariable == null) {
                    serializedVariable = new SerializedObject(variable);
                }

                return serializedVariable;
            }
        }

        [SerializeField]
        private Variable variable;
        private SerializedObject serializedVariable;

        public VirtualVariable(Variable variable) {
            this.variable = variable;
            serializedVariable = new SerializedObject(variable);
        }
    }
}
