using UnityEngine;
using System;
using System.Collections.Generic;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// A simple utility object that converts code-style class names into human-style
    /// class names
    /// </summary>
    public class HumanReadableConverter {

        private static NameBinding[] bindings = new NameBinding[]
        {
            new NameBinding(typeof(bool), "Boolean"),
            new NameBinding(typeof(float), "Float"), 
            new NameBinding(typeof(int), "Integer")
        };

        /// <summary>
        /// A simple relationship from type to binding
        /// </summary>
        private struct NameBinding {
            public Type type;
            public string conversion;

            public NameBinding(Type type, string conversion) {
                this.type = type;
                this.conversion = conversion;
            }
        }


        private Dictionary<Type, string> conversions;


        public HumanReadableConverter() {
            conversions = new Dictionary<Type, string>();

            foreach(var binding in bindings) {
                conversions.Add(binding.type, binding.conversion);
            }
        }

        /// <summary>
        /// The conversion from type to name
        /// </summary>
        /// <param name="type">The type to convert</param>
        /// <returns>An appropriate converted name if found.  Otherwise the type's name is returned instead</returns>
        public string ConvertToName(Type type) {
            string name;

            if(conversions.TryGetValue(type, out name)) {
                return name;
            }

            return type.Name;
        }

    }
}