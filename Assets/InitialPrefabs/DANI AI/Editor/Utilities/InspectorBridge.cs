using UnityEngine;
using System.Collections;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// A simple bridge to connect the editor to the inspector
    /// </summary>
    public class InspectorBridge {
        /// <summary>
        /// Event used to handle the instance where the inspector has edited something
        /// </summary>
        public event InspectorEditHandler OnInspectorEdit;

        /// <summary>
        /// The current inspector bridge
        /// </summary>
        public static InspectorBridge Instance {
            get {
                if(_instance == null) {
                    _instance = new InspectorBridge();
                }
                return _instance;
            }
        }

        public delegate void InspectorEditHandler();

        private static InspectorBridge _instance;

        /// <summary>
        /// Updates the inspector and invokes all methods registered to this bridge
        /// </summary>
        public void UpdateInspector() {
            if(OnInspectorEdit != null) {
                OnInspectorEdit();
            }
        }
    }
}