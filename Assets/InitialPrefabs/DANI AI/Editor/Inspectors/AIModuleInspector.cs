using UnityEngine;
using UnityEditor;
using System.Collections;
using InitialPrefabs.DANI;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// The base inspector for all AIModules
    /// </summary>
    public class AIModuleInspector<T> : UnityEditor.Editor where T : AIModule {
        /// <summary>
        /// The inspected module
        /// </summary>
        [SerializeField]
        protected T module;

        /// <summary>
        /// The diagram that the module is attached to
        /// </summary>
        protected AIDiagram diagram;

        /// <summary>
        /// The asset path of the module.  This is null if the inspected module is a
        /// runtime module
        /// </summary>
        [SerializeField]
        protected string path;

        /// <summary>
        /// Is the inspected module a runtime module?
        /// </summary>
        protected bool IsRuntime { get; private set; }


        protected virtual void OnEnable() {
            module = target as T;

            path = AssetDatabase.GetAssetPath(target.GetInstanceID());
            IsRuntime = string.IsNullOrEmpty(path);            
        }

        public override void OnInspectorGUI() {
            serializedObject.Update();

            EditorGUI.BeginChangeCheck();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Name"));

            if(EditorGUI.EndChangeCheck()) {
                serializedObject.ApplyModifiedProperties();
                InspectorBridge.Instance.UpdateInspector();
            }

            DrawDefaultInspector();
            EditorGUILayout.Space();
        }

    }
}
