using UnityEngine;
using UnityEditor;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// A custom inspector for editing settings on DaniEditor
    /// </summary>
    [CustomEditor(typeof(DaniEditorSettings))]
    public class EditorSettingsInspector : UnityEditor.Editor {
        private bool isAsset;

        private void OnEnable() {
            isAsset = !string.IsNullOrEmpty(AssetDatabase.GetAssetPath(target as DaniEditorSettings));
        }

        public override void OnInspectorGUI() {
            if(!isAsset) {
                EditorGUILayout.HelpBox("Settings is generated by the editor!  To make sure the changes are saved, click 'save' below",
                    MessageType.Warning);

                if(GUILayout.Button("Save")) {
                    var path = EditorUtility.SaveFilePanelInProject("Save settings",
                        "Dani Editor Settings", "asset", "Where to save?");

                    if(!string.IsNullOrEmpty(path)) {
                        AssetDatabase.CreateAsset(target, path);
                        AssetDatabase.SaveAssets();
                        AssetDatabase.Refresh();
                    }
                }

                EditorGUILayout.Space();
            }

            EditorGUI.BeginChangeCheck();

            base.OnInspectorGUI();

            if(EditorGUI.EndChangeCheck()) {
                // Update the bridge
                InspectorBridge.Instance.UpdateInspector();
            }
        }
    }
}