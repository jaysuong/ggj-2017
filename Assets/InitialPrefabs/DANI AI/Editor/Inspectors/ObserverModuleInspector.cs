using InitialPrefabs.DANI;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// Custom inspector for observer modules
    /// </summary>
    [CustomEditor(typeof(ObserverModule), true)]
    public class ObserverModuleInspector : AIModuleInspector<ObserverModule> {
        [SerializeField]
        private DecisionModule selectedDecision;
        [SerializeField]
        private SerializedObject conditionObject;

        [SerializeField]
        private List<ConditionPointer> decisionPointers;
        private ReorderableList decisionList;

        private GUIStyle weightStyle;


       protected override void OnEnable() {
            base.OnEnable();

            if(IsRuntime) {
                EditorApplication.update += HandleRuntimeUpdate;
            }
            else {
                diagram = AssetDatabase.LoadAssetAtPath<AIDiagram>(path);
                SetupDecisionList();
            }

            weightStyle = new GUIStyle();
            weightStyle.alignment = TextAnchor.MiddleRight;
        }

        protected virtual void OnDisable() {
            EditorApplication.update -= HandleRuntimeUpdate;
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            EditorGUILayout.Space();

            try {
                decisionList.DoLayoutList();
           }
            catch { }

            EditorGUILayout.Space();

            try {
                if(conditionObject != null && conditionObject.targetObject != null) {
                    EditorGUILayout.LabelField(string.Format("Condition for {0}", selectedDecision.name),
                        EditorStyles.boldLabel);

                    conditionObject.Update();
                    var p = conditionObject.GetIterator();
                    p.Next(true);

                    while(p.NextVisible(false)) {
                        if(p.name == "m_Script") {
                            continue;
                        }

                        EditorGUILayout.PropertyField(p, true);
                    }

                    conditionObject.ApplyModifiedProperties();
                }
            }
            catch { }
        }

        private void HandleRuntimeUpdate() {
            if(IsRuntime) {
                if(module.Diagram != null) {
                    diagram = module.Diagram;
                    SetupDecisionList();

                    EditorApplication.update -= HandleRuntimeUpdate;
                }
            }
        }

        private void SetupDecisionList() {
            var pointers = diagram.ConditionPointers;
            decisionPointers = ( from p in pointers
                                 where p.sourceId == module.Id
                                 orderby p.priority
                                 select p ).ToList();

            decisionList = new ReorderableList(decisionPointers, typeof(ConditionPointer));

            decisionList.drawHeaderCallback = (Rect rect) =>
            {
                GUI.Label(rect, "Connected Decisions", EditorStyles.boldLabel);
                GUI.Label(rect, "Weight", weightStyle);
            };

            decisionList.drawElementCallback = (Rect rect, int index, bool active, bool focused) =>
            {
                var element = decisionPointers[index];
                var decisionCandidate = ( from d in diagram.Decisions
                                          where d.Id == element.targetId
                                          select d ).FirstOrDefault();

                var conditionCandidate = ( from c in diagram.Conditions
                                           where c.Id == element.conditionId
                                           select c ).FirstOrDefault();

                if(decisionCandidate != null) {
                    EditorGUI.LabelField(rect, decisionCandidate.name);

                    if(conditionCandidate != null) {
                        EditorGUI.LabelField(rect, conditionCandidate.Weight.ToString(), weightStyle);
                    }
                }
            };

            decisionList.onSelectCallback = (ReorderableList list) =>
            {
                var element = decisionPointers[list.index];

                var conditionCandidate = ( from c in diagram.Conditions
                                           where c.Id == element.conditionId
                                           select c ).FirstOrDefault();

                if(conditionCandidate != null) {
                    conditionObject = new SerializedObject(conditionCandidate);

                    selectedDecision = ( from d in diagram.Decisions
                                         where d.Id == element.targetId
                                         select d ).FirstOrDefault();
                }
            };

            decisionList.onRemoveCallback = (ReorderableList list) => {
                var element = decisionPointers[list.index];

                var conditionCandidate = ( from c in diagram.Conditions
                                           where c.Id == element.conditionId
                                           select c ).FirstOrDefault();

                if(conditionCandidate != null) {
                    diagram.RemoveCondition(conditionCandidate);
                    decisionPointers.RemoveAt(list.index);
                    DestroyImmediate(conditionCandidate, true);

                    AssetDatabase.SaveAssets();
                    Repaint();
                }
            };
        }

    }
}
