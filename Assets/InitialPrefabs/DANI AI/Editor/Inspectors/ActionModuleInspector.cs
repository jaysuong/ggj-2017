using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using InitialPrefabs.DANI;
using System.Collections.Generic;
using System.Linq;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// A custom editor for action modules
    /// </summary>
    [CustomEditor(typeof(ActionModule), true)]
    public class ActionModuleInspector : AIModuleInspector<ActionModule> {
        [SerializeField]
        private SerializedObject decisionObject;

        [SerializeField]
        private List<ConditionPointer> decisionPointers;

        private ReorderableList decisionList;


        protected override void OnEnable() {
            base.OnEnable();

            if(IsRuntime) {
                EditorApplication.update += HandleRuntimeUpdate;
            }
            else {
                diagram = AssetDatabase.LoadAssetAtPath<AIDiagram>(path);
                SetupActionList();
            }
        }

       protected virtual void OnDisable() {
            EditorApplication.update -= HandleRuntimeUpdate;
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            EditorGUILayout.Space();

            try {
                decisionList.DoLayoutList();
            }
            catch { }

            EditorGUILayout.Space();
            
            if(decisionObject != null && decisionObject.targetObject != null) {
                try {
                    decisionObject.Update();

                    var d = decisionObject.GetIterator();
                    d.Next(true);
                    while(d.NextVisible(false)) {
                        if(d.name == "m_Script") {
                            continue;
                        }

                        EditorGUILayout.PropertyField(d, true);
                    }

                    decisionObject.ApplyModifiedProperties();
                }
                catch { }
            }
        }


        private void HandleRuntimeUpdate() {
            if(IsRuntime) {
                if(module.Diagram != null) {
                    diagram = module.Diagram;
                    SetupActionList();

                    EditorApplication.update -= HandleRuntimeUpdate;
                }
            }
        }

        private void SetupActionList() {
            var pointers = diagram.ConditionPointers;
            decisionPointers = ( from p in pointers
                                 where p.targetId == module.Id
                                 orderby p.priority
                                 select p ).ToList();

            decisionList = new ReorderableList(decisionPointers, typeof(ConditionPointer));

            decisionList.drawHeaderCallback = (Rect rect) =>
            {
                GUI.Label(rect, "Connected Decisions");
            };

            decisionList.drawElementCallback = (Rect rect, int index, bool active, bool focused) =>
            {
                var element = decisionPointers[index];
                var decisionCandidate = ( from d in diagram.Decisions
                                          where d.Id == element.sourceId
                                          select d ).FirstOrDefault();

                if(decisionCandidate != null) {
                    EditorGUI.LabelField(rect, decisionCandidate.name);
                }
            };

            decisionList.onSelectCallback = (ReorderableList list) =>
            {
                var element = decisionPointers[list.index];

                var decisionCandidate = ( from d in diagram.Decisions
                                          where d.Id == element.sourceId
                                          select d ).FirstOrDefault();

                if(decisionCandidate != null) {
                    decisionObject = new SerializedObject(decisionCandidate);
                }
            };

            decisionList.onRemoveCallback = (ReorderableList list) =>
            {
                var element = decisionPointers[list.index];
                diagram.RemoveConnection(element.sourceId, element.targetId);
                decisionPointers.RemoveAt(list.index);

                AssetDatabase.SaveAssets();
                Repaint();
            };
        }

    }
}
