using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using InitialPrefabs.DANI;
using System.Collections.Generic;
using System.Linq;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// Custom inspector for decision modules
    /// </summary>
    [CustomEditor(typeof(DecisionModule), true)]
    public class DecisionModuleInspector : AIModuleInspector<DecisionModule> {
        [SerializeField]
        private SerializedObject conditionObject;
        [SerializeField]
        private ObserverModule selectedObserver;

        [SerializeField]
        private SerializedObject actionObject;

        [SerializeField]
        private List<ConditionPointer> observerPointers;
        private ReorderableList observerList;

        [SerializeField]
        private List<ConditionPointer> actionPointers;
        private ReorderableList actionList;

        private GUIStyle weightStyle;


        protected override void OnEnable() {
            base.OnEnable();

            if(IsRuntime) {
                EditorApplication.update += HandleRuntimeUpdate;
            }
            else {
                diagram = AssetDatabase.LoadAssetAtPath<AIDiagram>(path);
                SetupObserverList();
                SetupActionList();
            }

            weightStyle = new GUIStyle();
            weightStyle.alignment = TextAnchor.MiddleRight;
        }

        protected virtual void OnDisable() {
            EditorApplication.update -= HandleRuntimeUpdate;
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            try {
                observerList.DoLayoutList();
            }
            catch { }

            EditorGUILayout.Space();

            try {
                if(conditionObject != null && conditionObject.targetObject != null) {
                    EditorGUILayout.LabelField(string.Format("Condition from {0}", selectedObserver.name),
                        EditorStyles.boldLabel);

                    EditorGUI.BeginChangeCheck();

                    conditionObject.Update();
                    var p = conditionObject.GetIterator();
                    p.Next(true);

                    while(p.NextVisible(false)) {
                        if(p.name == "m_Script") {
                            continue;
                        }

                        EditorGUILayout.PropertyField(p, true);
                    }

                    if(EditorGUI.EndChangeCheck()) {
                        conditionObject.ApplyModifiedProperties();
                        InspectorBridge.Instance.UpdateInspector();
                    }
                }
            }
            catch { }

            EditorGUILayout.Space();

            try {
                actionList.DoLayoutList();
            }
            catch { }

            EditorGUILayout.Space();
            
            if(actionObject != null && actionObject.targetObject != null) {
                try {
                    actionObject.Update();
                    var p = actionObject.GetIterator();
                    p.Next(true);

                    while(p.NextVisible(false)) {
                        if(p.name == "m_Script") {
                            continue;
                        }

                        EditorGUILayout.PropertyField(p, true);
                    }
                    actionObject.ApplyModifiedProperties();
                }
                catch { }
            }
        }

        private void HandleRuntimeUpdate() {
            if(IsRuntime) {
                if(module.Diagram != null) {
                    diagram = module.Diagram;
                    SetupActionList();
                    SetupObserverList();

                    EditorApplication.update -= HandleRuntimeUpdate;
                }
            }
        }


        #region Observer Lists

        private void SetupObserverList() {
            var pointers = diagram.ConditionPointers;
            observerPointers = ( from p in pointers
                                 where p.targetId == module.Id
                                 orderby p.priority
                                 select p ).ToList();

            observerList = new ReorderableList(observerPointers, typeof(ConditionPointer));

            observerList.drawHeaderCallback = (Rect rect) => {
                GUI.Label(rect, "Connected Observers", EditorStyles.boldLabel);
                GUI.Label(rect, "Weight", weightStyle);
            };

            observerList.drawElementCallback = (Rect rect, int index, bool active, bool focused) =>
            {
                var element = observerPointers[index];
                var observerCandidate = ( from o in diagram.Observers
                                          where o.Id == element.sourceId
                                          select o ).FirstOrDefault();

                var conditionCandidate = ( from c in diagram.Conditions
                                           where c.Id == element.conditionId
                                           select c ).FirstOrDefault();

                element.priority = index;

                if(observerCandidate != null) {
                    EditorGUI.LabelField(rect, observerCandidate.name);
                    EditorGUI.LabelField(rect, conditionCandidate.Weight.ToString(), weightStyle);
                }
            };

            observerList.onSelectCallback = (ReorderableList list) =>
            {
                var element = (ConditionPointer)observerList.list[list.index];
                var conditionCandidate = ( from c in diagram.Conditions
                                           where c.Id == element.conditionId
                                           select c );

                if(conditionCandidate.Any()) {
                    conditionObject = new SerializedObject(conditionCandidate.First());

                    selectedObserver = ( from o in diagram.Observers
                                         where o.Id == element.sourceId
                                         select o ).FirstOrDefault();
                }
            };

            observerList.onRemoveCallback = (ReorderableList list) =>
            {
                var element = observerPointers[list.index];

                var conditionCandidate = ( from c in diagram.Conditions
                                           where c.Id == element.conditionId
                                           select c ).FirstOrDefault();

                if(conditionCandidate != null) {
                    diagram.RemoveCondition(conditionCandidate);
                    observerPointers.RemoveAt(list.index);
                    DestroyImmediate(conditionCandidate, true);

                    AssetDatabase.SaveAssets();

                    Repaint();
                }
            };
        }

        private void SetupActionList() {
            var pointers = diagram.ConditionPointers;
            actionPointers = ( from p in pointers
                               where p.sourceId == module.Id
                               orderby p.priority
                               select p ).ToList();

            actionList = new ReorderableList(actionPointers, typeof(ConditionPointer));

            actionList.drawHeaderCallback = (Rect rect) =>
            {
                GUI.Label(rect, "Connected Actions", EditorStyles.boldLabel);
            };

            actionList.drawElementCallback = (Rect rect, int index, bool active, bool focused) =>
            {
                var element = actionPointers[index];
                var actionCandidate = ( from a in diagram.Actions
                                        where a.Id == element.targetId
                                        select a ).FirstOrDefault();

                element.priority = index + 1;

                if(actionCandidate != null) {
                    EditorGUI.LabelField(rect, actionCandidate.name);
                }
            };

            actionList.onSelectCallback = (ReorderableList list) =>
            {
                var element = actionPointers[list.index];

                var actionCandidate = ( from a in diagram.Actions
                                        where a.Id == element.targetId
                                        select a ).FirstOrDefault();

                if(actionCandidate != null) {
                    actionObject = new SerializedObject(actionCandidate);
                }
            };

            actionList.onRemoveCallback = (ReorderableList list) =>
            {
                var element = actionPointers[list.index];
                diagram.RemoveConnection(element.sourceId, element.targetId);
                actionPointers.RemoveAt(list.index);

                AssetDatabase.SaveAssets();
                Repaint();
            };

            actionList.onReorderCallback = (ReorderableList list) =>
            {
                InspectorBridge.Instance.UpdateInspector();
            };
        }

        #endregion


    }
}
