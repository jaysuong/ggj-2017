using UnityEngine;
using UnityEditor;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// The main inspector for AIBrain.  Allows hooking into Dani Editor
    /// </summary>
    [CustomEditor(typeof(AIBrain))]
    public class AIBrainInspector : UnityEditor.Editor {
        private bool isEventInvoked;

        private const string DiagramProperty = "diagram";

        private void OnEnable() {
            isEventInvoked = false;
            var diagramProp = serializedObject.FindProperty(DiagramProperty);

            if(diagramProp.objectReferenceValue != null) {
                if(!EditorApplication.isPlaying) {
                    var diagram = diagramProp.objectReferenceValue as AIDiagram;

                    DaniRuntimeBridge.Instance.SelectDiagram(diagram);
                    isEventInvoked = true;
                }
            }

            EditorApplication.update += Update;
        }

        private void OnDisable() {
            EditorApplication.update -= Update;
        }

        private void Update() {
            if(!isEventInvoked && EditorApplication.isPlaying) {
                var state = ( target as AIBrain ).RunningStatus;

                if(state == AIBrain.RunningState.Running || state == AIBrain.RunningState.Paused) {
                    var diagram = ( target as AIBrain ).Diagram;

                    DaniRuntimeBridge.Instance.SelectDiagram(diagram);
                    isEventInvoked = true;
                }                               
            }
        }
    }
}
