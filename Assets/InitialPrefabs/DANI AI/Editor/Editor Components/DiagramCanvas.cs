using UnityEngine;
using UnityEditor;
using InitialPrefabs.DANI;
using System.Linq;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// Handles drawing nodes and lines
    /// </summary>
    public class DiagramCanvas {
        public event AddObserverHandler OnAddObserver;
        public event AddDecisionHandler OnAddDecision;
        public event AddActionHandler OnAddAction;
        public event AddConditionHandler OnAddCondition;
        public event RemoveObserverHandler OnRemoveObserver;
        public event RemoveDecisionHandler OnRemoveDecision;
        public event RemoveActionHandler OnRemoveAction;


        private DaniDiagramEditor window;
        private DragEventHelper eventHelper;
        private Vector2 scroll;
        private HumanReadableConverter nameConverter;
        private VirtualEditorDiagram diagram;

        private VirtualModule selectedModule;

        public delegate void AddObserverHandler(MonoScript script, Vector2 position);
        public delegate void AddDecisionHandler(MonoScript script, Vector2 position);
        public delegate void AddActionHandler(MonoScript script, Vector2 position);
        public delegate void AddConditionHandler(VirtualModule source, VirtualModule target);
        public delegate void RemoveObserverHandler(VirtualModule observer);
        public delegate void RemoveDecisionHandler(VirtualModule decision);
        public delegate void RemoveActionHandler(VirtualModule action);

        private const float MinConnectionWidth = 1f;
        private const float MaxConnectionWidth = 10f;
        private readonly Vector2 ActionMessageSize = new Vector2(25, 20);
        private readonly Vector2 WeightMessageSize = new Vector2(50, 20);

        private DaniEditorSettings settings;


        [System.Serializable]
        public class DragEventHelper {
            public VirtualModule startModule;
            public EventType eventType;

            public Vector2 mouseOrigin;

            public enum EventType { None, Connecting }

            public void Reset() {
                startModule = null;
                eventType = EventType.None;
            }
        }



        public DiagramCanvas(DaniDiagramEditor window) {
            this.window = window;
            eventHelper = new DragEventHelper();
            nameConverter = new HumanReadableConverter();
            settings = window.Settings;
        }

        public Vector2 Draw(VirtualEditorDiagram diagram, Rect rect, Vector2 scroll) {
            this.scroll = scroll;
            this.diagram = diagram;

            using(var scope = new EditorGUILayout.ScrollViewScope(scroll)) {
                window.BeginWindows();

                // Draw the nodes
                var modules = diagram.Modules;
                foreach(var module in modules) {
                    if(module.module != null) {
                        DrawModule(module);

                        var oldColor = GUI.color;
                        GUI.color = module.NodeColor;

                        // Draw module node stubs
                        module.CalculateStubs(scroll);

                        if(module.type == VirtualModuleType.Observer || module.type == VirtualModuleType.Decision) {
                            GUI.Box(module.RightStub, GUIContent.none);
                        }

                        if(module.type == VirtualModuleType.Decision || module.type == VirtualModuleType.Action) {
                            GUI.Box(module.LeftStub, GUIContent.none);
                        }

                        GUI.color = oldColor;
                    }
                }

                // Draw the condition lines
                foreach(var pointer in diagram.Diagram.ConditionPointers) {
                    VirtualModule start;
                    VirtualModule end;

                    try {
                        start = diagram.GetModule(pointer.sourceId);
                        end = diagram.GetModule(pointer.targetId);
                    }
                    catch(NoVirtualModuleException) {
                        continue;
                    }

                    var color = settings.DefaultConnectionColor;
                    var width = 1f;

                    // Determine the width, if possible via a selected node on the canvas
                    if(selectedModule != null) {
                        if(start.id == selectedModule.id || end.id == selectedModule.id) {
                            color = settings.RelatedConnectionColor;
                        }

                        if(IsObserverToDecisionRelationship(start, end, pointer)) {

                            VirtualModule condition;

                            try {
                                condition = diagram.GetModule(pointer.conditionId);
                            }
                            catch(NoVirtualModuleException) {
                                condition = null;
                            }

                            if(condition != null) {
                                var maxWeight = diagram.GetMaxWeightOfTarget(selectedModule.id);
                                var currentWeight = ( condition.module as ConditionModule ).Weight;

                                if(maxWeight <= 0f) {
                                    maxWeight = 1f;
                                }

                                width = Mathf.Clamp(MaxConnectionWidth * currentWeight / maxWeight,
                                    MinConnectionWidth, MaxConnectionWidth);

                                LineDrawingUtilities.DrawBezierCurve(start.Position, end.Position, scroll, color, width);

                                // Draw a box midway, showing the percentage of the weights in relation to others
                                LineDrawingUtilities.DrawBoxOnBezier(start.Position, end.Position, scroll, WeightMessageSize, color, 
                                    string.Format("{0}%", ( currentWeight / maxWeight * 100f ).ToString("F2")));
                            }
                        }
                        else {
                            LineDrawingUtilities.DrawBezierCurve(start.Position, end.Position, scroll, color, width);
                        }
                    }
                    else {
                        LineDrawingUtilities.DrawBezierCurve(start.Position, end.Position, scroll, color, width);
                    }

                    // Draw priority boxes, if possible
                    if(ShouldDrawPriorityBoxes(start)) {
                        var priority = pointer.priority.ToString();

                        LineDrawingUtilities.DrawBezierCurve(start.Position, end.Position, scroll, color, width);
                        LineDrawingUtilities.DrawBoxOnBezier(start.Position, end.Position, 
                            scroll, ActionMessageSize, color, priority);
                    }
                }

                // Draw connective curve to mouse position, if there is any events
                if(eventHelper.eventType == DragEventHelper.EventType.Connecting) {
                    LineDrawingUtilities.DrawLine(eventHelper.mouseOrigin,
                        Event.current.mousePosition);
                }

                window.EndWindows();

                // Detect events
                var current = Event.current;
                switch(current.type) {
                    case EventType.ContextClick:
                        ShowAddMenu(new Vector2(-scroll.x + current.mousePosition.x, -scroll.y + current.mousePosition.y));
                        break;

                    case EventType.MouseUp:
                        if(current.button == 0) {
                            Selection.activeObject = diagram.Diagram;
                            selectedModule = null;
                            window.Repaint();
                        }

                        if(eventHelper.eventType == DragEventHelper.EventType.Connecting) {
                            var stubResult = ( from m in modules
                                               where m != null
                                               where m.ContainsStub(current.mousePosition)
                                               select m );

                            if(stubResult.Any()) {
                                if(OnAddCondition != null) {
                                    OnAddCondition(eventHelper.startModule, stubResult.First());
                                }
                            }

                            eventHelper.Reset();
                        }

                        break;

                    case EventType.MouseDown:
                        if(eventHelper.eventType == DragEventHelper.EventType.None) {
                            var stubResult = ( from m in modules
                                               where m != null
                                               where m.ContainsStub(current.mousePosition)
                                               select m );
                            if(stubResult.Any()) {
                                if(eventHelper.eventType == DragEventHelper.EventType.None) {
                                    eventHelper.eventType = DragEventHelper.EventType.Connecting;
                                    eventHelper.startModule = stubResult.First();

                                    if(eventHelper.startModule.LeftStub.Contains(current.mousePosition)) {
                                        eventHelper.mouseOrigin = eventHelper.startModule.LeftStub.center;
                                    }
                                    else {
                                        eventHelper.mouseOrigin = eventHelper.startModule.RightStub.center;
                                    }
                                }
                            }
                        }

                        break;

                    case EventType.MouseDrag:
                        if(current.button == 2) {
                            scroll.x = Mathf.Clamp(scroll.x + current.delta.x, -(900f - rect.width), 0f);
                            scroll.y += current.delta.y;
                        }

                        window.Repaint();
                        break;

                    case EventType.ScrollWheel:
                        scroll += current.delta;
                        window.Repaint();
                        break;
                }
            }

            return scroll;
        }

        private void DrawModule(VirtualModule module) {
            if(Event.current.type == EventType.Repaint) {
                return;
            }

            var oldColor = GUI.color;
            UpdateNodeColor(module);
            GUI.color = module.NodeColor;

            var position = module.Position;
            position.position += scroll;

            position = GUI.Window(module.module.GetInstanceID(), position,
                (int id) => {
                    switch(module.type) {
                        case VirtualModuleType.Observer:
                            DrawObserverWindow(module.module as ObserverModule);
                            break;

                        case VirtualModuleType.Decision:
                            DrawDecisionWindow(module.module as DecisionModule);
                            break;

                        case VirtualModuleType.Action:
                            break;

                        default:
                            break;
                    }

                    var current = Event.current;

                    switch(current.type) {
                        case EventType.MouseUp:
                            // Handle left-click
                            if(current.button == 0) {
                                Selection.activeObject = module.module;
                                selectedModule = module;
                            }
                            else if(current.button == 1) {
                                ShowNodeContextMenu(module);
                                current.Use();
                            }

                            break;
                    }


                    GUI.DragWindow();
                },
                new GUIContent(
                    !diagram.IsRuntime ? module.module.name : string.Format("{0} (Clone)", module.module.name)
                    ));

            position.position -= scroll;
            module.Position = position;

            GUI.color = oldColor;
        }

        #region Helpers

        private void DrawObserverWindow(ObserverModule obs) {
            EditorGUILayout.LabelField(string.Format("Type: {0}", nameConverter.ConvertToName(obs.OutputType)));
            EditorGUILayout.LabelField(string.Format("Value: {0}", obs.Output));
        }

        private void DrawDecisionWindow(DecisionModule dec) {
            EditorGUILayout.LabelField(string.Format("Total Score: {0}", dec.TotalScore));
            if(EditorApplication.isPlaying) {
                EditorGUILayout.LabelField(string.Format("Current Score: {0}", dec.CurrentScore));
            }
        }

        /// <summary>
        /// Updates the node color of the module
        /// </summary>
        /// <param name="module">The module to update</param>
        private void UpdateNodeColor(VirtualModule module) {
            if(!module.module.IsEnabled) {
                module.NodeColor = settings.DisabledNodeColor;
            }
            else {
                if(IsModuleAlikeOrRelated(module)) {
                    if(module.type != VirtualModuleType.Action || 
                        (module.type == VirtualModuleType.Action && 
                            window.SelectedDecision != null && 
                            window.SelectedDecision.ActiveAction == module.module)) {

                        module.NodeColor = settings.ActiveNodeColor;
                    }
                    else {
                        module.NodeColor = GetDefaultNodeColor(module);
                    }
                }
                else {
                    module.NodeColor = GetDefaultNodeColor(module);
                }
            }
        }

        private Color GetDefaultNodeColor(VirtualModule module) {
            switch(module.type) {
                case VirtualModuleType.Observer:
                    return settings.ObserverNodeColor;

                case VirtualModuleType.Decision:
                    return settings.DecisionNodeColor;

                case VirtualModuleType.Action:
                    return settings.ActionNodeColor;

                default:
                    return settings.DefaultNodeColor;
            }
        }

        #endregion

        #region Commands

        private void ShowAddMenu(Vector2 mousePosition) {
            var menu = new GenericMenu();

            // Add observers
            var observers = AssetSearcher.GetMonoScripts<ObserverModule>(true);
            if(observers.Length > 0) {
                foreach(var obs in observers) {
                    var type = obs.GetClass();
                    var localPath = AssetSearcher.GetLocalMenuPath(obs);

                    menu.AddItem(
                        new GUIContent(string.Format("Add Observer/{0}", 
                            (!string.IsNullOrEmpty(localPath)) ? localPath : type.Name)),
                        false,
                        (object o) => {
                            if(OnAddObserver != null) {
                                OnAddObserver((MonoScript)o, mousePosition);
                            }
                        },
                        obs);
                }
            }
            else {
                menu.AddDisabledItem(new GUIContent("Add Observer"));
            }

            // Add decisions
            var decisions = AssetSearcher.GetMonoScripts<DecisionModule>();
            if(decisions.Length > 0) {
                foreach(var dec in decisions) {
                    var type = dec.GetClass();
                    var localPath = AssetSearcher.GetLocalMenuPath(dec);

                    menu.AddItem(
                        new GUIContent(string.Format("Add Decision/{0}",
                            ( !string.IsNullOrEmpty(localPath) ) ? localPath : type.Name)),
                        false,
                        (object o) =>
                        {
                            if(OnAddDecision != null) {
                                OnAddDecision((MonoScript)o, mousePosition);
                            }
                        },
                        dec);
                }
            }
            else {
                menu.AddDisabledItem(new GUIContent("Add Decision"));
            }

            // Add actions
            var actions = AssetSearcher.GetMonoScripts<ActionModule>();
            if(actions.Length > 0) {
                foreach(var act in actions) {
                    var type = act.GetClass();
                    var localPath = AssetSearcher.GetLocalMenuPath(act);

                    menu.AddItem(
                        new GUIContent(string.Format("Add Action/{0}",
                           (!string.IsNullOrEmpty(localPath)) ? localPath : type.Name)), 
                        false, 
                        (object o) =>
                        {
                            if(OnAddAction != null) {
                                OnAddAction((MonoScript)o, mousePosition);
                            }
                        },
                        act
                        );
                }
            }
            else {
                menu.AddDisabledItem(new GUIContent("Add Action"));
            }


            menu.ShowAsContext();
        }

        /// <summary>
        /// Shows the context menu for nodes
        /// </summary>
        private void ShowNodeContextMenu(VirtualModule module) {
            var menu = new GenericMenu();

            menu.AddItem(new GUIContent("Delete Module"),
                false,
                () => {
                    var prompt = EditorUtility.DisplayDialog("Deleting an AI Module",
                        string.Format("Are you sure you want to delete {0}?", module.module.name), 
                        "Yes", "Cancel!");

                    if(prompt) {
                        switch(module.type) {
                            case VirtualModuleType.Observer:
                                if(OnRemoveObserver != null) {
                                    OnRemoveObserver(module);
                                }
                                break;

                            case VirtualModuleType.Decision:
                                if(OnRemoveDecision != null) {
                                    OnRemoveDecision(module);
                                }
                                break;

                            case VirtualModuleType.Action:
                                if(OnRemoveAction != null) {
                                    OnRemoveAction(module);
                                }
                                break;
                        }
                    }
                });

            menu.ShowAsContext();
        }

        #endregion

        #region Check Utilities

        /// <summary>
        /// Should the canvas draw a priority box form this VirtualModule?
        /// </summary>
        /// <param name="module">The module to check against</param>
        /// <returns></returns>
        private bool ShouldDrawPriorityBoxes(VirtualModule module) {
            return selectedModule != null &&
                module.type == VirtualModuleType.Decision &&
                selectedModule.id == module.id;
        }

        private bool IsObserverToDecisionRelationship(VirtualModule start, VirtualModule end, ConditionPointer pointer) {
            return selectedModule.type == VirtualModuleType.Decision &&
                                start.type == VirtualModuleType.Observer &&
                                end.id == selectedModule.id &&
                                !pointer.isSimplePointer;
        }

        private bool IsModuleAlikeOrRelated(VirtualModule module) {
            return window.SelectedDecision != null && 
                (module.module == window.SelectedDecision || diagram.AreModulesRelated(module.module, window.SelectedDecision)) &&
                (diagram.ContainsModuleReference(module.module));
        }

        #endregion

    }
}
