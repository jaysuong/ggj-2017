using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// A canvas that draws the variable list
    /// </summary>
    [Serializable]
    public class VariableListCanvas {
        public event AddVariableHandler OnAddVariable;
        public event RemoveVariableHandler OnRemoveVariable;

        public delegate void AddVariableHandler(MonoScript script);
        public delegate void RemoveVariableHandler(Variable variable);

        [SerializeField]
        private Vector2 scroll;
        [SerializeField]
        private int selectedIndex;
        private ReorderableList variableList;
        private AIDiagram selectedDiagram;

        private SerializedProperty listProperty;

        private const string NameProperty = "m_Name";
        private const string ValueProperty = "value";
        private const float NameWidth = 0.6f;


        public VariableListCanvas() {
            scroll = new Vector2();
            selectedIndex = -1;
        }

        /// <summary>
        /// Draws the canvas
        /// </summary>
        /// <param name="show">Will the editor draw the list?</param>
        /// <returns>The size of the window</returns>
        public void Draw(Rect rect, VirtualEditorDiagram diagram) {
            using (var scope = new EditorGUILayout.ScrollViewScope(scroll, GUILayout.Width(rect.width))) {
                if(diagram != null && variableList != null) {
                    try {
                        variableList.DoLayoutList();
                    }
                    catch (System.Exception e) {
                        Debug.LogWarning(e);
                    }
                }
                else {
                    GUILayout.Label("Select a diagram above to see its variables");
                }
            }
        }

        public void RefreshList(VirtualEditorDiagram diagram) {
            if(diagram == null || diagram.SerializedDiagram == null) {
                return;
            }

            var serializedObject = diagram.SerializedDiagram;
            serializedObject.Update();
            listProperty = serializedObject.FindProperty("variables");

            variableList = new ReorderableList(serializedObject, listProperty, true, false, true, true);

            variableList.headerHeight = 0f;

            // Draw the elements
            variableList.drawElementCallback = (Rect r, int index, bool active, bool focused) =>
            {
                serializedObject.Update();
                var element = listProperty.GetArrayElementAtIndex(index);
                var serializedElement = diagram.GetSerializedVariable(element.objectReferenceValue as Variable);
                serializedElement.Update();

                // Draw the element headers
                EditorGUI.LabelField(new Rect(r.x, r.y, r.width * NameWidth, r.height), 
                    serializedElement.FindProperty(NameProperty).stringValue, EditorStyles.boldLabel);
                EditorGUI.LabelField(new Rect(r.x + r.width * NameWidth, r.y, r.width * (1f - NameWidth), r.height),
                    ( serializedElement.targetObject as Variable).GetValue().ToString(), EditorStyles.boldLabel);

                // Draw the content, if selected
                if(index == selectedIndex) {
                    EditorGUI.BeginChangeCheck();

                    EditorGUI.LabelField(new Rect(r.x, r.y + variableList.elementHeight, r.width / 4f, r.height),
                        "Name");
                    EditorGUI.PropertyField(new Rect(r.x + r.width / 4f, r.y + variableList.elementHeight, r.width * 0.75f, r.height),
                        serializedElement.FindProperty(NameProperty), GUIContent.none);

                    EditorGUI.LabelField(new Rect(r.x, r.y + variableList.elementHeight * 2f, r.width / 4f, r.height),
                        "Value");
                    EditorGUI.PropertyField(new Rect(r.x + r.width / 4f, r.y + variableList.elementHeight * 2f, r.width * 0.75f, r.height),
                        serializedElement.FindProperty(ValueProperty), GUIContent.none);

                    if(EditorGUI.EndChangeCheck()) {
                        serializedElement.ApplyModifiedProperties();
                        serializedObject.ApplyModifiedProperties();
                    }
                }                    
            };

            // Add the "Add Button"
            variableList.onAddDropdownCallback = (Rect r, ReorderableList list) =>
            {
                var menu = new GenericMenu();
                var scripts = AssetSearcher.GetMonoScripts<Variable>(true);

                foreach(var script in scripts) {
                    menu.AddItem(new GUIContent(script.name), false,
                        (object o) =>
                        {
                            if(OnAddVariable != null) {
                                OnAddVariable((MonoScript)o);
                            }
                        },
                        script);
                }

                menu.ShowAsContext();
            };

            // Add the "Remove Button"
            variableList.onRemoveCallback = (ReorderableList list) =>
            {
                if(OnRemoveVariable != null) {
                    var variable = listProperty.GetArrayElementAtIndex(list.index).objectReferenceValue as Variable;
                    OnRemoveVariable(variable);
                }
            };

            // Add selection support
            variableList.onSelectCallback = (ReorderableList list) =>
            {
                if(selectedIndex == list.index) {
                    selectedIndex = -1;
                }
                else {
                    selectedIndex = list.index;
                }
            };

            // Add the height callback
            variableList.elementHeightCallback = (int index) =>
            {
                return selectedIndex == index ? variableList.elementHeight * 3f : variableList.elementHeight;
            };
        }
    }
}
