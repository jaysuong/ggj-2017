using UnityEngine;
using System.Collections;

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// Basic editor settings for the dani editor
    /// </summary>
    public class DaniEditorSettings : ScriptableObject {
        [SerializeField, Header("Default Node Colors")]
        private Color defaultNodeColor = Color.white;
        [SerializeField]
        private Color disabledNodeColor = Color.gray;
        [SerializeField]
        private Color activeNodeColor = Color.green;
        [SerializeField]
        private Color defaultConnectionColor = new Color(0.6f, 0.6f, 0.6f, 1f);
        [SerializeField]
        private Color relatedConnectionColor = Color.white;

        [SerializeField, Header("AI Module Colors")]
        private Color observerNodeColor = new Color(1f, 0.9f, 0.9f, 1f);
        [SerializeField]
        private Color decisionNodeColor = new Color(0.67f, 0.9f, 1f, 1f);
        [SerializeField]
        private Color actionNodeColor = new Color(0.9f, 1f, 0.9f, 1f);

        [SerializeField, Header("Light Skin Grid Background")]
        private Color gridLightSkinBackgroundColor = new Color(0f, 0f, 0f, 0.6f);
        [SerializeField]
        private Color gridLightSkinLineColor = new Color(0f, 0f, 0f, 0.10f);
        [SerializeField, Header("Dark Skin Grid Background")]
        private Color gridDarkSkinBackgroundColor = new Color(0f, 0f, 0f, 0.6f);
        [SerializeField]
        private Color gridDarkSkinLineColor = new Color(0f, 0f, 0f, 0.18f);

        [SerializeField]
        private Color observerGuideColor = new Color(1f, 0f, 0f, 0.1f);
        [SerializeField]
        private Color decisionGuideColor = new Color(0f, 0.39f, 0.52f, 0.1f);
        [SerializeField]
        private Color actionGuideColor = new Color(0f, 1f, 0f, 0.1f);

        public Color DefaultNodeColor { get { return defaultNodeColor; } }
        public Color DisabledNodeColor { get { return disabledNodeColor; } }
        public Color ActiveNodeColor { get { return activeNodeColor; } }
        public Color DefaultConnectionColor { get { return defaultConnectionColor; } }
        public Color RelatedConnectionColor { get { return relatedConnectionColor; } }

        public Color ObserverNodeColor { get { return observerNodeColor; } }
        public Color DecisionNodeColor { get { return decisionNodeColor; } }
        public Color ActionNodeColor { get { return actionNodeColor; } }

        public Color GridLightSkinBackgroundColor { get { return gridLightSkinBackgroundColor; } }
        public Color GridLightSkinLineColor { get { return gridLightSkinLineColor; } }
        public Color GridDarkSkinBackgroundColor { get { return gridDarkSkinBackgroundColor; } }
        public Color GridDarkSkinLineColor { get { return gridDarkSkinLineColor; } }

        public Color ObserverGuideColor { get { return observerGuideColor; } }
        public Color DecisionGuideColor { get { return decisionGuideColor; } }
        public Color ActionGuideColor { get { return actionGuideColor; } }

    }
}
