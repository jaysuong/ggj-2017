using UnityEngine;
using UnityEditor;
using System.Linq;
#if UNITY_5_5_OR_NEWER
using UnityEngine.Profiling;
#endif

namespace InitialPrefabs.DANI.Editor {
    /// <summary>
    /// The primary editor window for editing diagrams
    /// </summary>
    public class DaniDiagramEditor : EditorWindow {
        [SerializeField]
        private DaniEditorSettings settings;

        [SerializeField]
        private AIDiagram selectedDiagram;
        private VirtualEditorDiagram diagramRepresentation;

        public DecisionModule SelectedDecision { get; private set; }
        public DaniEditorSettings Settings {
            get {
                if(settings == null) {
                    var settingsGuids = AssetDatabase.FindAssets("t:DaniEditorSettings");
                    if(settingsGuids.Length > 0) {
                        var path = AssetDatabase.GUIDToAssetPath(settingsGuids.First());
                        settings = AssetDatabase.LoadAssetAtPath<DaniEditorSettings>(path);
                    }
                    else {
                        settings = CreateInstance<DaniEditorSettings>();
                    }
                }
                return settings;
            }
        }
       
        [SerializeField]
        private Vector2 scroll;
        [SerializeField]
        private bool showList;
        [SerializeField]
        private bool showGuides;

        private GridBackground background;
        private DiagramCanvas canvas;
        [SerializeField]
        private VariableListCanvas variableList = new VariableListCanvas();

        private int updateCounter;    // For EditorApplication.update delegate
        private const int MaxUpdateCount = 1;


        [MenuItem("Tools/Dani AI/Open Editor %#d")]
        public static void Open() {
            GetWindowWithRect<DaniDiagramEditor>(new Rect(0f, 0f, 900f, 600f), false, "Dani Editor");
        }

        private void OnEnable() {
            background = new GridBackground(Settings);
            canvas = new DiagramCanvas(this);
            canvas.OnAddObserver += HandleAddObserverEvent;
            canvas.OnAddDecision += HandleAddDecisionEvent;
            canvas.OnAddAction += HandleAddActionEvent;
            canvas.OnAddCondition += HandleAddConditionEvent;
            canvas.OnRemoveObserver += HandleRemoveObserverEvent;
            canvas.OnRemoveDecision += HandleRemoveDecisionEvent;
            canvas.OnRemoveAction += HandleRemoveActionEvent;
            variableList.OnAddVariable += HandleAddVariableEvent;
            variableList.OnRemoveVariable += HandleRemoveVariableEvent;

            SelectedDecision = DaniRuntimeBridge.Instance.SelectedDecisionModule;
            variableList.RefreshList(diagramRepresentation);
        }

        private DaniDiagramEditor() {
            showList = false;
            DaniRuntimeBridge.Instance.diagramSelectEvent += HandleDiagramSelectEvent;
            DaniRuntimeBridge.Instance.decisionSelectEvent += HandleDecisionSelectEvent;
            InspectorBridge.Instance.OnInspectorEdit += HandleInspectorEdit;
            EditorApplication.playmodeStateChanged += HandlePlayModeChangeState;
            EditorApplication.update += HandleEditorUpdateLoop;
            Undo.undoRedoPerformed += HandleUndoEvent;
        }

        private void OnGUI() {
            Profiler.BeginSample("Dani Editor Window");
            
            DrawToolbar();

            using(var horizontal = new EditorGUILayout.HorizontalScope()) {

                // Draw the list, if possible
                var listRect = showList ? new Rect(0, 17, 265, position.height - 17) :
                    new Rect();
                if(showList) {
                    variableList.Draw(listRect, diagramRepresentation);
                }

                // Draw the graph background
                var canvasRect = new Rect(listRect.xMax, 17, position.width - listRect.width,
                    position.height - 17);
                background.DrawGrid(canvasRect, scroll, showGuides);

                // Draw the canvas, if possible
                if(selectedDiagram != null) {
                    // Reconstruct virtuals if necessary
                    if(diagramRepresentation == null || diagramRepresentation.Diagram != selectedDiagram) {
                        diagramRepresentation = new VirtualEditorDiagram(selectedDiagram);
                    }

                    scroll = canvas.Draw(diagramRepresentation, canvasRect, scroll);
                }

            }
            
            Profiler.EndSample();
        }

    #region Delegates

        /// <summary>
        /// Delegate used to handle events where a new diagram is selected
        /// </summary>
        /// <param name="diagram">The selected diagram</param>
        private void HandleDiagramSelectEvent(AIDiagram diagram) {
            selectedDiagram = diagram;
            diagramRepresentation = new VirtualEditorDiagram(diagram);
            scroll = Vector2.zero;
            variableList.RefreshList(diagramRepresentation);
            Repaint();
        }

        /// <summary>
        /// Delegate used to handle events where the playmode has changed
        /// </summary>
        private void HandlePlayModeChangeState() {
            background = new GridBackground(Settings);
            Repaint();
        }

        /// <summary>
        /// Delegate used to handle event where Update() on the editor application
        /// is invoked
        /// </summary>
        private void HandleEditorUpdateLoop() {
            if(EditorApplication.isPlaying) {
                updateCounter++;

                if(updateCounter >= MaxUpdateCount) {
                    updateCounter = 0;
                    Repaint();
                }
            }
        }

        /// <summary>
        /// Delegate used to handle the event where the user edits something in the inspector
        /// </summary>
        private void HandleInspectorEdit() {
            Repaint();
        }

        /// <summary>
        /// Delegate used to handle event where an observer is created
        /// </summary>
        /// <param name="script">The script to create the observer with</param>
        /// <param name="position">The position of the observer</param>
        private void HandleAddObserverEvent(MonoScript script, Vector2 position) {
            if(diagramRepresentation == null || !diagramRepresentation.IsValid) {
                diagramRepresentation = new VirtualEditorDiagram(selectedDiagram);
            }
            var obs = CreateInstance(script.GetClass()) as ObserverModule;
            obs.name = RegexTools.GetReadableText(script.GetClass().Name);

            var obsObj = new SerializedObject(obs);
            obsObj.Update();
            obsObj.FindProperty("modulePosition").vector2Value = position;
            obsObj.ApplyModifiedPropertiesWithoutUndo();

            diagramRepresentation.AddObserver(obs);
        }

        /// <summary>
        /// Delegate used to handle the event where the user attempts adds a decision
        /// </summary>
        /// <param name="script">The decision's script</param>
        /// <param name="position">The position of the mouse</param>
        private void HandleAddDecisionEvent(MonoScript script, Vector2 position) {
            if(diagramRepresentation == null || !diagramRepresentation.IsValid) {
                diagramRepresentation = new VirtualEditorDiagram(selectedDiagram);
            }
            var dec = CreateInstance(script.GetClass()) as DecisionModule;
            dec.name = RegexTools.GetReadableText(script.GetClass().Name);

            var decObj = new SerializedObject(dec);
            decObj.Update();
            decObj.FindProperty("modulePosition").vector2Value = position;
            decObj.ApplyModifiedPropertiesWithoutUndo();

            diagramRepresentation.AddDecision(dec);
        }

        /// <summary>
        /// Delegate used to handle the event where the user attempts to add an action
        /// </summary>
        /// <param name="script">The action's script</param>
        /// <param name="position">The mouse position</param>
        private void HandleAddActionEvent(MonoScript script, Vector2 position) {
            if(diagramRepresentation == null || !diagramRepresentation.IsValid) {
                diagramRepresentation = new VirtualEditorDiagram(selectedDiagram);
            }
            var act = CreateInstance(script.GetClass()) as ActionModule;
            act.name = RegexTools.GetReadableText(script.GetClass().Name);

            var actObj = new SerializedObject(act);
            actObj.Update();
            actObj.FindProperty("modulePosition").vector2Value = position;
            actObj.ApplyModifiedPropertiesWithoutUndo();

            diagramRepresentation.AddAction(act);
        }

        /// <summary>
        /// Delegate used to handle the event where the user tries to connect two modules together
        /// </summary>
        /// <param name="source">The source module</param>
        /// <param name="target">The target module</param>
        private void HandleAddConditionEvent(VirtualModule source, VirtualModule target) {
            if(source == target || source.type == target.type) {
                return;
            }

            if(diagramRepresentation == null || !diagramRepresentation.IsValid) {
                diagramRepresentation = new VirtualEditorDiagram(selectedDiagram);
            }

            if(diagramRepresentation.HasConnection(source.module, target.module) ||
                    diagramRepresentation.HasConnection(target.module, source.module)) {
                return;
            }

            var isConnectionAllowed = ( source.type == VirtualModuleType.Observer && target.type == VirtualModuleType.Decision ) ||
                (source.type == VirtualModuleType.Decision && target.type == VirtualModuleType.Action);

            if(!isConnectionAllowed) {
                // check if the connection is reversed, if so then swap
                if((source.type == VirtualModuleType.Action && target.type == VirtualModuleType.Decision) ||
                    source.type == VirtualModuleType.Decision && target.type == VirtualModuleType.Observer) {

                    var swap = source;
                    source = target;
                    target = swap;
                }
                else {
                    return;
                }
            }

            if(source.type == VirtualModuleType.Observer) {
                var type = ( source.module as ObserverModule ).OutputType;
                var conditionList = AssetSearcher.GetMonoScripts<ConditionModule>();
                var result = ( from c in conditionList
                               where c.GetClass() != null
                               where c.GetClass().GetCustomAttributes(true).
                                Where(o => o is GenericTypeAttribute && ((GenericTypeAttribute)(o)).type == type).Any()
                              select c);

                if(result.Any()) {
                    var condition = CreateInstance(result.First().GetClass()) as ConditionModule;
                    diagramRepresentation.AddCondition(source, target, condition);
                }
                else {
                    diagramRepresentation.AddCondition(source, target);
                }
            }
            else {
                diagramRepresentation.AddCondition(source, target);
            }

            Repaint();
        }

        private void HandleAddVariableEvent(MonoScript script) {
            if(diagramRepresentation == null || !diagramRepresentation.IsValid) {
                diagramRepresentation = new VirtualEditorDiagram(selectedDiagram);
            }

            var variable = CreateInstance(script.GetClass()) as Variable;
            variable.name = script.GetClass().Name;
            diagramRepresentation.AddVariable(variable);
            variableList.RefreshList(diagramRepresentation);
            Repaint();
        }

        private void HandleRemoveObserverEvent(VirtualModule module) {
            if(diagramRepresentation == null || !diagramRepresentation.IsValid) {
                diagramRepresentation = new VirtualEditorDiagram(selectedDiagram);
            }
            diagramRepresentation.RemoveObserver(module);
        }

        private void HandleRemoveDecisionEvent(VirtualModule module) {
            if(diagramRepresentation == null || !diagramRepresentation.IsValid) {
                diagramRepresentation = new VirtualEditorDiagram(selectedDiagram);
            }
            diagramRepresentation.RemoveDecision(module);
        }

        private void HandleRemoveVariableEvent(Variable variable) {
            if(diagramRepresentation == null || !diagramRepresentation.IsValid) {
                diagramRepresentation = new VirtualEditorDiagram(selectedDiagram);
            }

            diagramRepresentation.RemoveVariable(variable);
            variableList.RefreshList(diagramRepresentation);
            Repaint();
        }

        private void HandleRemoveActionEvent(VirtualModule module) {
            if(diagramRepresentation == null || !diagramRepresentation.IsValid) {
                diagramRepresentation = new VirtualEditorDiagram(selectedDiagram);
            }
            diagramRepresentation.RemoveAction(module);
        }

        private void HandleDecisionSelectEvent(DecisionModule module) {
            SelectedDecision = module;
        }

        private void HandleUndoEvent() {
            Repaint();
        }

    #endregion

    #region UI

        /// <summary>
        /// Draws the toolbar for the editor
        /// </summary>
        private void DrawToolbar() {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar, GUILayout.Width(position.width));
            
            // Draw the selected diagram button
            if(selectedDiagram != null) {
                if(GUILayout.Button(new GUIContent(selectedDiagram.name), EditorStyles.toolbarDropDown, GUILayout.MinWidth(300f))) {
                    // Show a list of diagrams to select from
                    ShowDiagramSelectMenu();
                }

                showList = GUILayout.Toggle(showList, "Variables", EditorStyles.toolbarButton);
            }
            else {
                if(GUILayout.Button("Select a diagram", EditorStyles.toolbarDropDown, GUILayout.MinWidth(300f))) {
                    // Show a list of diagrams to select from
                    ShowDiagramSelectMenu();
                }
            }

            // Draw a button to create a new diagram
            if(GUILayout.Button("Create New...", EditorStyles.toolbarButton)) {
                CreateNewDiagram();
            }

            // Draw guides, if possible
            showGuides = GUILayout.Toggle(showGuides, "Graph Guides", EditorStyles.toolbarButton);

            // Draw an inspector menu for settings
            if(GUILayout.Button("Settings", EditorStyles.toolbarButton, GUILayout.Width(75f))) {
                Selection.activeObject = Settings;
            }            

            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }

    #endregion

    #region UI Helpers

        /// <summary>
        /// Shows a dropdown menu for the diagram
        /// </summary>
        private void ShowDiagramSelectMenu() {
            var guids = AssetDatabase.FindAssets("t:AIDiagram");
            var diagrams = new AIDiagram[guids.Length];

            for(var i = 0; i < guids.Length; ++i) {
                var path = AssetDatabase.GUIDToAssetPath(guids[i]);
                diagrams[i] = AssetDatabase.LoadAssetAtPath<AIDiagram>(path);
            }

            // Sort the diagrams by name
            diagrams = diagrams.OrderBy(d => d.name).ToArray();

            var menu = new GenericMenu();

            foreach(var diagram in diagrams) {
                if(diagram == null) {
                    continue;
                }

                menu.AddItem(
                    new GUIContent(diagram.name), 
                    false,
                    (object data) =>
                    {
                        HandleDiagramSelectEvent((AIDiagram)data);
                    },
                    diagram);
            }

            // Insert an option to create a new diagram if there are no existing diagrams
            if(menu.GetItemCount() < 1) {
                menu.AddItem(new GUIContent("Create a new diagram..."), false, () => { CreateNewDiagram(); });
            }

            menu.ShowAsContext();
        }

        /// <summary>
        /// Creates a new diagram with a prompt for location and name
        /// </summary>
        private void CreateNewDiagram() {
            var path = EditorUtility.SaveFilePanelInProject(
                "Create a new AIDiagram", 
                "AI Diagram",
                "asset",
                "Where to create?");

            if(!string.IsNullOrEmpty(path)) {
                var diagram = CreateInstance<AIDiagram>();

                AssetDatabase.CreateAsset(diagram, path);
                AssetDatabase.SaveAssets();

                HandleDiagramSelectEvent(diagram);
            }
        }

    #endregion
    }
}
