using UnityEngine;
using System;

namespace InitialPrefabs.DANI {
    [Serializable]
    public class ConditionPointer {
        public bool isSimplePointer;

        public string conditionId;
        public string sourceId;
        public string targetId;

        public int priority;

        public ConditionPointer(bool isSimplePointer, string conditionId, string source, string target) {
            this.conditionId = conditionId;
            this.isSimplePointer = isSimplePointer;
            sourceId = source;
            targetId = target;
            priority = 0;
        }

        public ConditionPointer(ConditionPointer other) {
            conditionId = other.conditionId;
            isSimplePointer = other.isSimplePointer;
            sourceId = other.sourceId;
            targetId = other.targetId;
            priority = other.priority;
        }
    }
}