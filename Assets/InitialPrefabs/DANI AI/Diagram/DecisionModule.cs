using UnityEngine;
using System.Collections.Generic;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// The "thought process" of Dani AI.  Selects and determines what actions
    /// will run and its current scores
    /// </summary>
    public abstract class DecisionModule : AIModule {
        [SerializeField, Tooltip("Can Dani switch out of this decision while it's still running?")]
        private bool isInterruptable = true;
        [SerializeField, Tooltip("The maximum possible score this decision can have when Dani picks a new decision")]
        private float totalScore = 1;

        /// <summary>
        /// The decision's total score, used to determine how likely Dani will select this decision
        /// </summary>
        public float TotalScore { get { return totalScore; } set { totalScore = value; } }

        /// <summary>
        /// The decision's current score, evalulated from its connected observers
        /// </summary>
        public float CurrentScore { get; protected set; }

        /// <summary>
        /// Can Dani switch out from this decision while it is still running?
        /// </summary>
        public bool IsInterruptable { get { return isInterruptable; } set { isInterruptable = value; } }

        /// <summary>
        /// The current running action on this decision
        /// </summary>
        public ActionModule ActiveAction { get; protected set; }

        protected List<ActionModule> actions { get; private set; }
        protected List<Conditional> conditions { get; private set; }

        internal override void Setup(AIDiagram diagram, GameObject gameObject, Transform transform) {
            base.Setup(diagram, gameObject, transform);
            actions = new List<ActionModule>();
            conditions = new List<Conditional>();
        }

        /// <summary>
        /// Updates the decision's current score
        /// </summary>
        public virtual void UpdateScore() {
            var localWeight = 0f;
            var totalWeight = 0f;
            
            for(var i = 0; i < conditions.Count; ++i) {
                var conditional = conditions[i];

                totalWeight += conditional.condition.Weight;
                localWeight += ( conditional.Evaluate() ) ? conditional.condition.Weight : 0f;
            }

            CurrentScore = ( localWeight / totalWeight ) * totalScore;
        }

        /// <summary>
        /// Overridable method that is called when the decision is selected
        /// </summary>
        public virtual void OnDecisionSelect() { }

        /// <summary>
        /// Overridable method that is called when the decision is running.  This is 
        /// called every frame
        /// </summary>
        /// <returns>True if the decision is considered as "finished will all tasks"</returns>
        public virtual bool OnDecisionRun() { return true; }

        /// <summary>
        /// Overridable method that is called when a different decision is selected
        /// </summary>
        /// <param name="wasInterrupted">Was the decision running when switched out of context?</param>
        public virtual void OnDecisionExit(bool wasInterrupted) { }

        /// <summary>
        /// Adds an action to the current action list
        /// </summary>
        /// <param name="action">The action to add</param>
        public void AddAction(ActionModule action) {
            actions.Add(action);
        }

        /// <summary>
        /// Removes an action from the current action list
        /// </summary>
        /// <param name="action">The action to remove</param>
        public void RemoveAction(ActionModule action) {
            actions.Remove(action);
        }

        /// <summary>
        /// Adds a conditional for the decision to process
        /// </summary>
        /// <param name="conditional">The conditional to add</param>
        public void AddConditional(Conditional conditional) {
            conditions.Add(conditional);
        }

        /// <summary>
        /// Removes a conditional from the decision
        /// </summary>
        /// <param name="conditional">The conditional to remove</param>
        public void RemoveConditional(Conditional conditional) {
            conditions.Remove(conditional);
        }
    }
}
