using UnityEngine;
using System.Collections;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A combination of a ConditionModule and an Observer.  Used to describe
    /// whether or not an observer is valid in the current situation
    /// </summary>
    public struct Conditional {
        public readonly ObserverModule observer;
        public readonly ConditionModule condition;

        public Conditional(ObserverModule observer, ConditionModule condition) {
            this.observer = observer;
            this.condition = condition;
            condition.CacheObserver(observer);
        }

        public bool Evaluate() {
            return condition.Evaluate();
        }

        public override bool Equals(object obj) {
            if(!(obj is Conditional))
                return false;

            var other = (Conditional)obj;

            return observer == other.observer && condition == other.condition;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }
}
