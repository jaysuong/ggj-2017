using UnityEngine;
using System.Collections;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// Defines a condition for an observer to be considered as "valid" in respect
    /// to the current decision
    /// </summary>
    public abstract class ConditionModule : AIModule {
        [SerializeField]
        protected float weight = 1f;

        [SerializeField, HideInInspector]
        private string sourceId;

        [SerializeField, HideInInspector]
        private string targetId;


        public float Weight {
            get { return weight; }
            set { weight = value; }
        }

        public abstract object CompareValue { get; set; }

        public abstract void CacheObserver(ObserverModule module);

        public abstract bool Evaluate();

        /// <summary>
        /// Evaluates whether or not the value is considered "valid" aka. passes
        /// all parameters
        /// </summary>
        /// <param name="valueToCompare">The value to compare</param>
        public abstract bool Compare(object valueToCompare);
    }
}
