using System;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// Attribute that describes what type the script is associated with
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class GenericTypeAttribute : Attribute {
        /// <summary>
        /// The declared type as defined by the user.
        /// </summary>
        public Type type;

        /// <summary>
        /// Is the type a fallback type?
        /// </summary>
        public bool isFallback;

        public GenericTypeAttribute(Type type, bool isFallback = false) {
            this.type = type;
            this.isFallback = isFallback;
        }
    }
}