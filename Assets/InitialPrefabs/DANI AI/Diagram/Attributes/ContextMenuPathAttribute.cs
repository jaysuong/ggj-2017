using UnityEngine;
using System;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// Attribute used for positioning commands in the context menus
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class ContextMenuPathAttribute : Attribute {
        public string path;

        public ContextMenuPathAttribute(string path) {
            this.path = path;
        }
    }
}