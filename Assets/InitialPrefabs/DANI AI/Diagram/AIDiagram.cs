using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A container for all AIDiagram types
    /// </summary>
    [CreateAssetMenu(fileName = "New AI Diagram", menuName = "Dani AI/AI Diagram")]
    public class AIDiagram : ScriptableObject {
        [SerializeField]
        private string guid;

        [SerializeField, TextArea]
        private string comments;

        [SerializeField]
        private List<ObserverModule> observers;

        [SerializeField]
        private List<DecisionModule> decisions;

        [SerializeField]
        private List<ActionModule> actions;

        [SerializeField]
        private List<ConditionModule> conditions;

        [SerializeField]
        private List<ConditionPointer> conditionReferences;

        [SerializeField]
        private List<Variable> variables;

        public string Id { get { return guid; } }

        public ObserverModule[] Observers { get { return observers.ToArray(); } }
        public DecisionModule[] Decisions { get { return decisions.ToArray(); } }
        public ActionModule[] Actions { get { return actions.ToArray(); } }
        public ConditionModule[] Conditions { get { return conditions.ToArray(); } }
        public ConditionPointer[] ConditionPointers { get { return conditionReferences.ToArray(); } }
        public Variable[] Variables { get { return variables.ToArray(); } }


        private AIDiagram() {
            guid = Guid.NewGuid().ToString();
            observers = new List<ObserverModule>();
            decisions = new List<DecisionModule>();
            actions = new List<ActionModule>();
            conditions = new List<ConditionModule>();
            conditionReferences = new List<ConditionPointer>();
            variables = new List<Variable>();
        }

        private void OnDestroy() {
            foreach(var obs in observers) {
                if(obs != null) {
                    Destroy(obs);
                }
            }

            foreach(var dec in decisions) {
                if(dec != null) {
                    Destroy(dec);
                }
            }

            foreach(var act in actions) {
                if(act != null) {
                    Destroy(act);
                }
            }

            foreach(var con in conditions) {
                if(con != null) {
                    Destroy(con);
                }
            }

            foreach(var variable in variables) {
                if(variable != null) {
                    Destroy(variable);
                }
            }

            observers.Clear();
            decisions.Clear();
            actions.Clear();
            conditions.Clear();
            conditionReferences.Clear();
            variables.Clear();
        }

        /// <summary>
        /// Creates a copy of the current diagram
        /// </summary>
        /// <returns>A deep copy of the diagram</returns>
        public AIDiagram CreateCopy() {
            var clone = Instantiate(this);

            clone.observers = new List<ObserverModule>(observers.Count);
            for(var i = 0; i < observers.Count; ++i) {
                var obs = Instantiate(observers[i]);
                obs.name = observers[i].name;
                clone.observers.Add(obs);
            }

            clone.decisions = new List<DecisionModule>(decisions.Count);
            for(var i = 0; i < decisions.Count; ++i) {
                var dec = Instantiate(decisions[i]);
                dec.name = decisions[i].name;
                clone.decisions.Add(dec);
            }

            clone.actions = new List<ActionModule>(actions.Count);
            for(var i = 0; i < actions.Count; ++i) {
                var act = Instantiate(actions[i]);
                act.name = actions[i].name;
                clone.actions.Add(act);
            }

            clone.conditions = new List<ConditionModule>(conditions.Count);
            for(var i = 0; i < conditions.Count; ++i) {
                clone.conditions.Add(Instantiate(conditions[i]));
            }

            clone.conditionReferences = new List<ConditionPointer>(conditionReferences.Count);
            for(var i = 0; i < conditionReferences.Count; ++i) {
                var pointer = conditionReferences[i];

                clone.conditionReferences.Add(new ConditionPointer(pointer));
            }

            clone.variables = new List<Variable>(variables.Count);
            for(var i = 0; i < variables.Count; ++i) {
                var variable = Instantiate(variables[i]);
                variable.name = variables[i].name;
                clone.variables.Add(variable);
            }

            return clone;
        }

        /// <summary>
        /// Adds an observer to the diagram
        /// </summary>
        /// <param name="observer">The observer to add</param>
        public void AddObserver(ObserverModule observer) {
            observers.Add(observer);
        }

        /// <summary>
        /// Adds a decision to the diagram
        /// </summary>
        /// <param name="decision">The decision to add</param>
        public void AddDecision(DecisionModule decision) {
            decisions.Add(decision);
        }

        /// <summary>
        /// Adds an action to the diagram
        /// </summary>
        /// <param name="action">The action to add</param>
        public void AddAction(ActionModule action) {
            actions.Add(action);
        }

        /// <summary>
        /// Adds an edge to the diagram
        /// </summary>
        /// <param name="source">The source object's id</param>
        /// <param name="target">The target object's id</param>
        public void AddCondition(string source, string target) {
            conditionReferences.Add(new ConditionPointer(true, 
                new Guid().ToString(), source, target));
        }

        /// <summary>
        /// Adds a new condition to the diagram
        /// </summary>
        /// <param name="condition">The condition to add</param>
        /// <param name="source">The source id</param>
        /// <param name="target">The target id</param>
        public void AddCondition(ConditionModule condition, string source, string target) {
            conditions.Add(condition);

            conditionReferences.Add(new ConditionPointer(false, condition.Id, source, target));
        }
        
        /// <summary>
        /// Adds a variable to the diagram
        /// </summary>
        /// <param name="variable">The variable to add</param>
        public void AddVariable(Variable variable) {
            variables.Add(variable);
        }
        
        /// <summary>
        /// Removes an observer from the diagram
        /// </summary>
        /// <param name="obs">The observer to remove</param>
        public void RemoveObserver(ObserverModule obs) {
            observers.Remove(obs);
            RemovePointersContainingId(obs.Id);
        }

        /// <summary>
        /// Removes a decision from the diagram
        /// </summary>
        /// <param name="dec">The decision to remove</param>
        public void RemoveDecision(DecisionModule dec) {
            decisions.Remove(dec);
            RemovePointersContainingId(dec.Id);
        }

        /// <summary>
        /// Removes an action from the diagram
        /// </summary>
        /// <param name="act">The action to remove</param>
        public void RemoveAction(ActionModule act) {
            actions.Remove(act);
            RemovePointersContainingId(act.Id);
        }

        /// <summary>
        /// Removes all pointers containing the id
        /// </summary>
        /// <param name="id">The id to remove</param>
        public void RemovePointersContainingId(string id) {
            for(var i = 0; i < conditionReferences.Count; ++i) {
                var pointer = conditionReferences[i];

                if(pointer.sourceId == id) {
                    if(!pointer.isSimplePointer) {
                        // Get the condition, and delete it
                        var condition = ( from c in conditions
                                          where c.Id == pointer.conditionId
                                          select c ).FirstOrDefault();

                        if(condition != null) {
#if UNITY_EDITOR
                            DestroyImmediate(condition, true);
#else
                            Destroy(condition);
#endif
                            conditions.RemoveAll(c => c == null);
                        }
                    }
                }
            }

            conditionReferences.RemoveAll(c => c.sourceId == id || c.targetId == id);
        }

        /// <summary>
        /// Removes the condition from the diagram
        /// </summary>
        /// <param name="con">The condition to remove</param>
        public void RemoveCondition(ConditionModule con) {
            conditions.Remove(con);
            RemovePointersContainingId(con.Id);
        }

        /// <summary>
        /// Removes a connection from the diagram
        /// </summary>
        /// <param name="sourceId">The starting id of the connection</param>
        /// <param name="targetId">The target id of the connection</param>
        public void RemoveConnection(string sourceId, string targetId) {
            var result = ( from c in conditionReferences
                           where (c.sourceId != sourceId && c.targetId != targetId) ||
                            (c.sourceId != sourceId && c.targetId == targetId) ||
                            (c.sourceId == sourceId && c.targetId != targetId)
                           select c ).ToList();

            conditionReferences = result;
        }

        /// <summary>
        /// Removes a variable from a diagram
        /// </summary>
        /// <param name="variable">The variable to remove</param>
        public void RemoveVariable(Variable variable) {
            variables.Remove(variable);
        }

        #region Utilities

        /// <summary>
        /// Cleans up the diagram by removing all null references.  Use this sparingly, as this
        /// process is slow
        /// </summary>
        public void Sanitize() {
            observers.RemoveAll(o => o == null);
            decisions.RemoveAll(d => d == null);
            actions.RemoveAll(a => a == null);
            conditions.RemoveAll(c => c == null);
            variables.RemoveAll(v => v == null);
        }

        /// <summary>
        /// A simple method to check if a module is not null, delegate style
        /// </summary>
        /// <param name="module">The module to check</param>
        /// <returns>True if the module is not null, false otherwise</returns>
        private static bool IsNotNull(AIModule module) {
            return module != null;
        }

        #endregion
    }
}
