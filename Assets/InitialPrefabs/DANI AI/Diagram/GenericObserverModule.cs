using UnityEngine;
using System.Collections;
using System;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A generic version of an observer.  Handles any serializable type
    /// </summary>
    /// <typeparam name="T">The type of observer</typeparam>
    public class GenericObserverModule<T> : ObserverModule {
        [SerializeField]
        protected T output;

        public override object Output { get { return output; } }
        public sealed override Type OutputType { get { return typeof(T); } }

        public T GetOutput() { return output; }
    }
}
