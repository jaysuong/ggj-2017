using UnityEngine;
using System;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// The "sensor" of the AI system.  Outputs the information for the AI
    /// to reference with.
    /// </summary>
    public abstract class ObserverModule : AIModule {
        /// <summary>
        /// The current output of the observer module
        /// </summary>
        public abstract object Output { get; }

        /// <summary>
        /// The current type of the output
        /// </summary>
        public abstract Type OutputType { get; }

        /// <summary>
        /// Overridable method that updates the output values.  Called every step.
        /// </summary>
        public virtual void OnObserverUpdate() { }
    }
}
