using UnityEngine;
using System;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// The base module class to reference all components in the AI
    /// </summary>
    public abstract class AIModule : ScriptableObject {

        /// <summary>
        /// Notes and comments on the current module
        /// </summary>
        [SerializeField, TextArea]
        private string comments;

        [SerializeField]
        private bool isEnabled = true;

        [SerializeField, HideInInspector]
        private string guid = Guid.NewGuid().ToString();

        [SerializeField, HideInInspector]
        private Vector2 modulePosition;

        public string Id { get { return guid; } }
        public bool IsEnabled {
            get { return isEnabled; }
            set { isEnabled = value; }
        }
        public string Comments {
            get { return comments; }
            set { comments = value; }
        }

        public GameObject gameObject { get; private set; }
        public Transform transform { get; private set; }
        public AIDiagram Diagram { get; private set; }

        /// <summary>
        /// Simple generic setup that mimics MonoBehaviours
        /// </summary>
        internal virtual void Setup(AIDiagram diagram, GameObject gameObject, Transform transform) {
            Diagram = diagram;
            this.gameObject = gameObject;
            this.transform = transform;
        }

        /// <summary>
        /// Overridable method to initialize tasks
        /// </summary>
        public virtual void OnAwake() { }

        /// <summary>
        /// Overridable method that is called when the brain is paused
        /// </summary>
        public virtual void OnPause() { }

        /// <summary>
        /// Overridable method that is called when the brain resumes
        /// </summary>
        public virtual void OnResume() { }

        #region Components

        /// <summary>
        /// Gets the first instance of a component on the game object.  
        /// Shortcut for gameObject.GetComponent<T>()
        /// </summary>
        /// <typeparam name="T">The type of the component to search</typeparam>
        /// <returns>The found component.  Null otherwise</returns>
        public T GetComponent<T>() {
            return ( gameObject != null ) ? gameObject.GetComponent<T>() : default(T);
        }

        /// <summary>
        /// Gets the first instance of a component on the game object and all of its 
        /// children.  Shortcut for gameObject.GetComponentInChildren<T>()
        /// </summary>
        /// <typeparam name="T">The type of the component to search</typeparam>
        /// <returns>The found component.  Null otherwise</returns>
        public T GetComponentInChildren<T>() {
            return ( gameObject != null ) ? gameObject.GetComponentInChildren<T>() : default(T);
        }

        /// <summary>
        /// Gets the first instance of a component on the game object and all of its parents.
        /// Shortcut for gameObject.GetComponentInParent<T>()
        /// </summary>
        /// <typeparam name="T">The type of the component to search</typeparam>
        /// <returns>The found component.  Null otherwise</returns>
        public T GetComponentInParent<T>() {
            return ( gameObject != null ) ? gameObject.GetComponentInParent<T>() : default(T);
        }

        /// <summary>
        /// Gets all components of the given type on the game object.  Shortcut for 
        /// gameObject.GetComponents<T>()
        /// </summary>
        /// <typeparam name="T">The type of the components to search</typeparam>
        /// <returns>An array of found components</returns>
        public T[] GetComponents<T>() {
            return ( gameObject != null ) ? gameObject.GetComponents<T>() : new T[0];
        }

        /// <summary>
        /// Gets all components of the given type on the game object and all of its children.
        /// Shortcut for gameObject.GetComponentsInChildren<T>()
        /// </summary>
        /// <typeparam name="T">The type of the components to search</typeparam>
        /// <returns>An array of found components</returns>
        public T[] GetComponentsInChildren<T>() {
            return ( gameObject != null ) ? gameObject.GetComponentsInChildren<T>() : new T[0];
        }

        /// <summary>
        /// Gets all the components of the given type on the game object and all of its parents.
        /// Shortcut for gameObject.GetComponentsInParent<T>()
        /// </summary>
        /// <typeparam name="T">The type of the components to search</typeparam>
        /// <returns>An array of found components</returns>
        public T[] GetComponentsInParent<T>() {
            return ( gameObject != null ) ? gameObject.GetComponentsInParent<T>() : new T[0];
        }

        #endregion

    }
}
