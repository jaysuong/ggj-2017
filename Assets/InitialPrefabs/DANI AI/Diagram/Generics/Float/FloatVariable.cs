using UnityEngine;
using System.Collections;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A variable representing a float number
    /// </summary>
    public class FloatVariable : GenericVariable<float> { }
}
