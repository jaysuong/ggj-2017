using UnityEngine;
using System.Collections;
using System;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A generic condition for floats
    /// </summary>
    [GenericType(typeof(float), true)]
    public class FloatCondition : GenericConditionModule<float, FloatObserver> {
        public FloatCompareType comparison;

        public enum FloatCompareType { GreaterThan, LessThan, Equals, NotEquals }

        public override bool Evaluate() {
            var valueToCompare = observer.GetOutput();

            switch(comparison) {
                case FloatCompareType.GreaterThan:
                    return valueToCompare > compareValue;

                case FloatCompareType.LessThan:
                    return valueToCompare < compareValue;

                case FloatCompareType.Equals:
                    return valueToCompare == compareValue;

                case FloatCompareType.NotEquals:
                    return valueToCompare != compareValue;

                default:
                    return false;
            }
        }

        public override bool Compare(object valueToCompare) {
            if(!(valueToCompare is float)) {
                return false;
            }

            switch(comparison) {
                case FloatCompareType.GreaterThan:
                    return (float)valueToCompare > compareValue;

                case FloatCompareType.LessThan:
                    return (float)valueToCompare < compareValue;

                case FloatCompareType.Equals:
                    return (float)valueToCompare == compareValue;

                case FloatCompareType.NotEquals:
                    return (float)valueToCompare != compareValue;

                default:
                    return false;
            }
        }

    }
}
