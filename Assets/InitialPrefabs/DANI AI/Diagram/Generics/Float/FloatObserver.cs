using UnityEngine;
using System.Collections;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A generic observer for floats
    /// </summary>
    [GenericType(typeof(float), true)]
    public class FloatObserver : GenericObserverModule<float> { }
}