using UnityEngine;
using System.Collections;
using System;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// An enum describing the a boolean
    /// </summary>
    public enum BoolType { True, False }

    /// <summary>
    /// A generic comparison for booleans
    /// </summary>
    [GenericType(typeof(bool), true)]
    public class BoolCondition : GenericConditionModule<BoolType, BoolObserver> {

        public override bool Evaluate() {
            var value = observer.GetOutput();

            return ( compareValue == BoolType.True && value ) ||
                ( compareValue == BoolType.False && !value );
        }

        public override bool Compare(object valueToCompare) {
            if(!( valueToCompare is bool )) {
                return false;
            }

            var value = (bool)valueToCompare;

            return ( compareValue == BoolType.True && value ) ||
                ( compareValue == BoolType.False && !value );
        }
    }
}
