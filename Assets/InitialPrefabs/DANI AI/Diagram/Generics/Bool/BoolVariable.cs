using UnityEngine;
using System.Collections;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A boolean variable
    /// </summary>
    public class BoolVariable : GenericVariable<bool> {
        
    }
}
