using UnityEngine;
using System.Collections;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A generic observer for booleans
    /// </summary>
    [GenericType(typeof(bool), true)]
    public class BoolObserver : GenericObserverModule<bool> { }
}