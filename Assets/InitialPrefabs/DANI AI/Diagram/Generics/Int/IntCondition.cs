using UnityEngine;
using System.Collections;
using System;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A generic condition for ints
    /// </summary>
    [GenericType(typeof(int), true)]
    public class IntCondition : GenericConditionModule<int, IntObserver> {
        public IntCompareType comparison;

        public enum IntCompareType { GreaterThan, LessThan, Equals, NotEquals }

        public override bool Evaluate() {
            var valueToCompare = observer.GetOutput();

            switch(comparison) {
                case IntCompareType.GreaterThan:
                    return valueToCompare > compareValue;

                case IntCompareType.LessThan:
                    return valueToCompare < compareValue;

                case IntCompareType.Equals:
                    return valueToCompare == compareValue;

                case IntCompareType.NotEquals:
                    return valueToCompare != compareValue;

                default:
                    return false;
            }
        }

        public override bool Compare(object valueToCompare) {
            if(!(valueToCompare is int)) {
                return false;
            }

            switch(comparison) {
                case IntCompareType.GreaterThan:
                    return (int)valueToCompare > compareValue;

                case IntCompareType.LessThan:
                    return (int)valueToCompare < compareValue;

                case IntCompareType.Equals:
                    return (int)valueToCompare == compareValue;

                case IntCompareType.NotEquals:
                    return (int)valueToCompare != compareValue;

                default:
                    return false;
            }
        }
    }
}
