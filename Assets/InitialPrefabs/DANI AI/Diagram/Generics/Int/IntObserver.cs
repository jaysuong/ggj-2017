using UnityEngine;
using System.Collections;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A generic observer for ints
    /// </summary>
    [GenericType(typeof(int), true)]
    public class IntObserver : GenericObserverModule<int> { }
}