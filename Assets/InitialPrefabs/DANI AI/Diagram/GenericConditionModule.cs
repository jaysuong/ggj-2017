using UnityEngine;
using System.Collections;
using System;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A generic representation of a condition.  Used to create conditions for specific
    /// data types
    /// </summary>
    /// <typeparam name="TCompare">The type of the condition</typeparam>
    public abstract class GenericConditionModule<TCompare, TObserver> : ConditionModule where TObserver : ObserverModule {
        [SerializeField]
        protected TCompare compareValue;

        [NonSerialized]
        protected TObserver observer;

        public override void CacheObserver(ObserverModule module) {
            observer = module as TObserver;
        }

        public override object CompareValue {
            get { return compareValue; }
            set { compareValue = (TCompare)value; }
        }
    }
}
