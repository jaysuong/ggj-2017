using UnityEngine;
using System.Collections.Generic;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A simple thought process where actions are run either sequentially or randomly
    /// </summary>
    public class SimpleDecision : DecisionModule {
        public ActionSelectionType selectionType;

        public enum ActionSelectionType { Sequential, Random }

        protected Queue<ActionModule> actionQueue;
        protected bool wasActionStarted;
        protected bool wasActionComplete;


        public override void OnAwake() {
            base.OnAwake();

            actionQueue = new Queue<ActionModule>();
        }

        public override void OnDecisionSelect() {
            actionQueue.Clear();

            switch (selectionType) {
                case ActionSelectionType.Sequential:
                    for(var i = 0; i < actions.Count; ++i) {
                        actionQueue.Enqueue(actions[i]);
                    }
                    break;

                case ActionSelectionType.Random:
                    actionQueue.Enqueue(actions[Random.Range(0, actions.Count)]);
                    break;
            }

            wasActionStarted = false;
            wasActionComplete = false;
        }

        public override bool OnDecisionRun() {
            if(actionQueue.Count > 0) {
                var current = actionQueue.Peek();

                // If the current is disabled, then deque and continue
                if(!current.IsEnabled) {
                    actionQueue.Dequeue();
                }
                else {
                    ActiveAction = current;

                    if(!wasActionStarted) {
                        current.OnActionStart();
                        wasActionStarted = true;
                    }
                    else {
                        if(!wasActionComplete) {
                            var state = current.OnActionUpdate();

                            if(state != ActionState.Running) {
                                wasActionComplete = true;
                                current.OnActionEnd(state);
                            }
                        }
                    }

                    if(wasActionComplete) {
                        actionQueue.Dequeue();

                        wasActionStarted = false;
                        wasActionComplete = false;
                    }
                }
            }

            return actionQueue.Count < 1;
        }

        public override void OnDecisionExit(bool wasInterrupted) {
            if(actionQueue.Count > 0) {
                var current = actionQueue.Peek();

                if(wasActionStarted && !wasActionComplete) {
                    current.OnActionEnd(ActionState.Fail);
                }
            }

            actionQueue.Clear();
        }

        public override void OnPause() {
            if(actionQueue.Count > 0) {
                actionQueue.Peek().OnPause();
            }
        }

        public override void OnResume() {
            if(actionQueue.Count > 0) {
                actionQueue.Peek().OnResume();
            }
        }

    }
}
