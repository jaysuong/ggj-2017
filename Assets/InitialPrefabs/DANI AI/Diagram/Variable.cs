using UnityEngine;
using System;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A value-storing object that is local to the diagram.  They are like Parameters
    /// in the Animator
    /// </summary>
    public abstract class Variable : ScriptableObject {
        /// <summary>
        /// The variable's type
        /// </summary>
        public abstract Type Type { get; }

        /// <summary>
        /// Gets the variable's current value.
        /// </summary>
        public abstract object GetValue();
    }
}
