using UnityEngine;
using System.Collections;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// Describes the current running state of an action.
    /// Running = The action is still running
    /// Success = The action is done, and was successful
    /// Fail = The action is done, but failed
    /// </summary>
    public enum ActionState { Running, Success, Fail }

    /// <summary>
    /// An "action" of the AI agent
    /// </summary>
    public abstract class ActionModule : AIModule {

        /// <summary>
        /// Overridable method that is called when the action begins
        /// </summary>
        public virtual void OnActionStart() { }

        /// <summary>
        /// Overridable method that is called when the action is currently running.
        /// Actions will run continously until ActionState.Success or ActionState.Fail
        /// is returned
        /// </summary>
        /// <returns></returns>
        public virtual ActionState OnActionUpdate() {
            return ActionState.Running;
        }

        /// <summary>
        /// Overridable method that is called when the action has completed.
        /// </summary>
        /// <param name="state">The curent state of the action.</param>
        public virtual void OnActionEnd(ActionState state) { }

    }
}