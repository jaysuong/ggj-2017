using UnityEngine;
using InitialPrefabs.DANI;
using System.Collections.Generic;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// Decribes when the brain should run its AI steps
    /// </summary>
    public enum ExecutionType { OnUpdate, OnLateUpdate, OnFixedUpdate, ByScript }

    /// <summary>
    /// The main runtime component of Dani.  
    /// </summary>
    public class AIBrain : MonoBehaviour {
        [SerializeField]
        private AIDiagram diagram;

        [SerializeField, Tooltip("When should Dani run its AI step?")]
        private ExecutionType executionOrder = ExecutionType.OnUpdate;

        /// <summary>
        /// The diagram that this brain is using to perform actions
        /// </summary>
        public AIDiagram Diagram {
            get { return ( virtualDiagram != null ) ? virtualDiagram.Diagram : null; }
        }

        /// <summary>
        /// When the brain should run its AI Step (during Update, OnUpdate, etc.)
        /// </summary>
        public ExecutionType ExecutionOrder { get { return executionOrder; } set { executionOrder = value; } }

        /// <summary>
        /// The current running state of the brain
        /// </summary>
        public RunningState RunningStatus { get; private set; }

        /// <summary>
        /// Decribes the brain's ability to run actions
        /// </summary>
        public enum RunningState { NotInitialized, Initializing, Running, Paused }

        private VirtualDiagram virtualDiagram;
        private DecisionPool decisionPool;


        private void Awake() {
            RunningStatus = RunningState.NotInitialized;
        }

        private void Start() {
            RunningStatus = RunningState.Initializing;

            try {
                virtualDiagram = new VirtualDiagram(diagram, gameObject, transform);
                decisionPool = new DecisionPool(virtualDiagram.Diagram.Decisions.Length);
                virtualDiagram.WakeModule();

                RunObserverStep();
                virtualDiagram.ActiveDecision = SelectNextDecision();
                virtualDiagram.ActiveDecision.OnDecisionSelect();
                RunningStatus = RunningState.Running;
            }
            catch (NoDiagramException) {
                Debug.LogWarningFormat("No diagram is attached to the brain in {0}!", name);
                virtualDiagram = null;
                RunningStatus = RunningState.NotInitialized;
            }
            catch(MissingDecisionException e) {
                Debug.LogError(e);
                virtualDiagram = null;
                RunningStatus = RunningState.NotInitialized;
            }

            if(RunningStatus == RunningState.Running || RunningStatus == RunningState.Paused) {
                // Register itself onto AINexus
                AINexus.Instance.RegisterBrain(this);
            }
        }

        private void OnDestroy() {
            if(virtualDiagram != null) {
                virtualDiagram.Purge();
            }
        }

        private void OnEnable() {
            if(RunningStatus == RunningState.Paused) {
                if(virtualDiagram != null) {
                    virtualDiagram.Resume();
                }

                RunningStatus = RunningState.Running;
            }
        }

        private void OnDisable() {
            if(RunningStatus == RunningState.Running) {
                if(virtualDiagram != null) {
                    virtualDiagram.Pause();
                }

                RunningStatus = RunningState.Paused;
            }
        }

        /// <summary>
        /// Runs all observers and updates their values
        /// </summary>
        public void RunObserverStep() {
            if(virtualDiagram == null) { return; }

            var observers = virtualDiagram.Observers;

            for(int i = 0; i < observers.Length; ++i) {
                if(observers[i].IsEnabled) {
                    observers[i].OnObserverUpdate();
                }
            }
        }

        /// <summary>
        /// Plays the next AI step
        /// </summary>
        public void RunAIStep() {
            if(virtualDiagram == null) { return; }

            RunObserverStep();

            var isDecisionDone = virtualDiagram.ActiveDecision.OnDecisionRun();

            if(isDecisionDone) {
                var bestDecision = SelectNextDecision();

                if(bestDecision != virtualDiagram.ActiveDecision) {
                    if(virtualDiagram.ActiveDecision != null) {
                        virtualDiagram.ActiveDecision.OnDecisionExit(false);
                    }

                    bestDecision.OnDecisionSelect();
                    virtualDiagram.ActiveDecision = bestDecision;
                }
                else {
                    virtualDiagram.ActiveDecision.OnDecisionExit(false);
                    virtualDiagram.ActiveDecision.OnDecisionSelect();
                }

                DaniRuntimeBridge.Instance.SelectDecision(Diagram, bestDecision);
            }
            else if(virtualDiagram.ActiveDecision.IsInterruptable) {
                var bestDecision = SelectNextDecision();

                if(bestDecision != virtualDiagram.ActiveDecision) {
                    if(virtualDiagram.ActiveDecision != null) {
                        virtualDiagram.ActiveDecision.OnDecisionExit(true);
                    }

                    bestDecision.OnDecisionSelect();
                    virtualDiagram.ActiveDecision = bestDecision;
                    DaniRuntimeBridge.Instance.SelectDecision(Diagram, bestDecision);
                }
            }
        }

        /// <summary>
        /// Pauses the brain
        /// </summary>
        public void Pause() {
            if(RunningStatus == RunningState.Running) {
                if(virtualDiagram != null) {
                    virtualDiagram.Pause();
                }

                RunningStatus = RunningState.Paused;
            }
        }

        /// <summary>
        /// Resumes the brain
        /// </summary>
        public void Resume() {
            if(RunningStatus == RunningState.Paused) {
                if(virtualDiagram != null) {
                    virtualDiagram.Resume();
                }

                RunningStatus = RunningState.Running;
            }
        }

        #region Module Fetching By ID

        /// <summary>
        /// Fetches an AIModule with a matching id
        /// </summary>
        /// <typeparam name="T">The type of AIModule to look for</typeparam>
        /// <param name="id">The id of the module</param>
        /// <returns>The found module.  Null otherwise</returns>
        public T GetAIModuleById<T>(string id) where T : AIModule {
            if(virtualDiagram == null) { return default(T); }

            AIModule module = null;
            module = virtualDiagram.GetObserver(id);

            if(module == null) {
                // Attempt to look for decisions
                module = virtualDiagram.GetDecision(id);

                if(module == null) {
                    // Attempt to look for actions
                    module = virtualDiagram.GetAction(id);

                    if(module == null) {
                        module = virtualDiagram.GetCondition(id);
                    }
                }
            }
            else {
                module = null;
            }

            return (T)module;
        }

        /// <summary>
        /// Gets an observer with a matching id
        /// </summary>
        /// <param name="id">The id of the observer</param>
        /// <returns>The found observer.  Null otherwise</returns>
        public ObserverModule GetObserverById(string id) {
            return virtualDiagram != null ? virtualDiagram.GetObserver(id) : null;
        }

        /// <summary>
        /// Gets a decision with a matching id
        /// </summary>
        /// <param name="id">The id of the decision</param>
        /// <returns>The found decision.  Null otherwise</returns>
        public DecisionModule GetDecisionById(string id) {
            return virtualDiagram != null ? virtualDiagram.GetDecision(id) : null;
        }

        /// <summary>
        /// Gets an action with a matching id
        /// </summary>
        /// <param name="id">The id of the action</param>
        /// <returns>The found action.  Null otherwise</returns>
        public ActionModule GetActionById(string id) {
            return virtualDiagram != null ? virtualDiagram.GetAction(id) : null;
        }

        /// <summary>
        /// Gets a condition with a matching id
        /// </summary>
        /// <param name="id">The id of the condition</param>
        /// <returns>The found condition.  Null otherwise</returns>
        public ConditionModule GetConditionById(string id) {
            return virtualDiagram != null ? virtualDiagram.GetCondition(id) : null;
        }

        #endregion

        #region Module Fetching By Name

        /// <summary>
        /// Gets an observer by name
        /// </summary>
        /// <param name="name">The name to look for</param>
        /// <returns>The found observer.  Null otherwise</returns>
        /// <remarks>This method may be slower on larger diagrams, as the runtime is O(n)</remarks>
        public ObserverModule GetObserverByName(string name) {
            if(virtualDiagram == null) { return null; }

            var observers = virtualDiagram.Diagram.Observers;
            for(var i = 0; i < observers.Length; ++i) {
                var observer = observers[i];

                if(observer.name == name) {
                    return observer;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets a decision by name
        /// </summary>
        /// <param name="name">The name to look for</param>
        /// <returns>The found decision.  Null otherwise</returns>
        /// /// <remarks>This method may be slower on larger diagrams, as the runtime is O(n)</remarks>
        public DecisionModule GetDecisionByName(string name) {
            if(virtualDiagram == null) { return null; }

            var decisions = virtualDiagram.Diagram.Decisions;
            for(var i = 0; i < decisions.Length; ++i) {
                var decision = decisions[i];

                if(decision.name == name) {
                    return decision;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets an action by name
        /// </summary>
        /// <param name="name">The name to look for</param>
        /// <returns>The found action.  Null otherwise</returns>
        /// /// <remarks>This method may be slower on larger diagrams, as the runtime is O(n)</remarks>
        public ActionModule GetActionByName(string name) {
            if(virtualDiagram == null) { return null; }

            var actions = virtualDiagram.Diagram.Actions;
            for(var i = 0; i < actions.Length; ++i) {
                var action = actions[i];

                if(action.name == name) {
                    return action;
                }
            }

            return null;
        }

        #endregion

        /// <summary>
        /// Gets a variable by name
        /// </summary>
        /// <param name="name">The name of the variable</param>
        /// <returns>Null if there are no variables with matching names</returns>
        public Variable GetVariable(string name) {
            return virtualDiagram != null ? virtualDiagram.GetVariable(name) : null;
        }

        /// <summary>
        /// Gets a variable by name and type
        /// </summary>
        /// <typeparam name="T">The type of the variable</typeparam>
        /// <param name="name">The name of the variable</param>
        /// <returns>Null if there are no variables with the same name and type</returns>
        public T GetVariable<T>(string name) where T : Variable {
            return virtualDiagram != null ? virtualDiagram.GetVariable<T>(name) : default(T);
        }

        /// <summary>
        /// Selects the next best decision
        /// </summary>
        private DecisionModule SelectNextDecision() {
            var bestScore = Mathf.NegativeInfinity;

            var decisions = virtualDiagram.Decisions;
            for(var i = 0; i < decisions.Length; ++i) {
                var dec = decisions[i];

                if(!dec.IsEnabled) {
                    continue;
                }

                dec.UpdateScore();

                if(dec.CurrentScore > bestScore) {
                    bestScore = dec.CurrentScore;
                    decisionPool.ClearPool();
                    decisionPool.AddDecision(dec);
                }
                else if(dec.CurrentScore == bestScore) {
                    decisionPool.AddDecision(dec);
                }
            }

            return decisionPool.GetRandomDecision();
        }

    }
}
