using UnityEngine;
using System.Collections.Generic;
using InitialPrefabs.DANI;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A virtual subcomponent of AIBrain that handles cloning and cleaning up
    /// the diagram
    /// </summary>
    internal class VirtualDiagram {
        /// <summary>
        /// The runtime copy of the diagram
        /// </summary>
        public AIDiagram Diagram { get; private set; }

        /// <summary>
        /// The active decision
        /// </summary>
        public DecisionModule ActiveDecision { get; set; }

        /// <summary>
        /// The available observers
        /// </summary>
        public ObserverModule[] Observers { get; private set; }

        /// <summary>
        /// The available decisions
        /// </summary>
        public DecisionModule[] Decisions { get; private set; }

        private Dictionary<string, ObserverModule> observers;
        private Dictionary<string, DecisionModule> decisions;
        private Dictionary<string, ActionModule> actions;
        private Dictionary<string, ConditionModule> conditions;
        private Variable[] variables;


        public VirtualDiagram(AIDiagram diagram, GameObject gameObject, Transform transform) {
            observers = new Dictionary<string, ObserverModule>();
            decisions = new Dictionary<string, DecisionModule>();
            actions = new Dictionary<string, ActionModule>();
            conditions = new Dictionary<string, ConditionModule>();

            if(diagram == null) {
                throw new NoDiagramException();
            }

            if(diagram.Decisions.Length < 1) {
                throw new MissingDecisionException(diagram);
            }

            Diagram = diagram.CreateCopy();
            Diagram.name = string.Format("{0} (Runtime in {1})", diagram.name, gameObject.name);

            // Sanitize before creating refs
            if(IsDirty(Diagram)) {
                Diagram.Sanitize();
            }

            for(var i = 0; i < Diagram.Observers.Length; ++i) {
                var obs = Diagram.Observers[i];
                obs.Setup(Diagram, gameObject, transform);
                observers.Add(obs.Id, obs);
            }

            for(var i = 0; i < Diagram.Decisions.Length; ++i) {
                var dec = Diagram.Decisions[i];
                dec.Setup(Diagram, gameObject, transform);
                decisions.Add(dec.Id, dec);
            }

            for(var i = 0; i < Diagram.Actions.Length; ++i) {
                var act = Diagram.Actions[i];
                act.Setup(Diagram, gameObject, transform);
                actions.Add(act.Id, act);
            }

            for(var i = 0; i < Diagram.Conditions.Length; ++i) {
                var con = Diagram.Conditions[i];
                con.Setup(Diagram, gameObject, transform);
                conditions.Add(con.Id, con);
            }

            foreach(var decision in decisions.Values) {
                var actionPointers = new List<ConditionPointer>();
                var observerPointers = new List<ConditionPointer>();

                for(var i = 0; i < Diagram.ConditionPointers.Length; ++i) {
                    var pointer = Diagram.ConditionPointers[i];

                    // Assumed to be connected to an action
                    if(pointer.sourceId == decision.Id) {
                        actionPointers.Add(pointer);
                    }
                    else if(pointer.targetId == decision.Id) {
                        observerPointers.Add(pointer);
                    }
                }

                // Sort lists and add to decisions
                actionPointers.Sort(CompareConditionPriority);
                for(var x = 0; x < actionPointers.Count; ++x) {
                    var action = GetAction(actionPointers[x].targetId);

                    if(action != null) {
                        decision.AddAction(action);
                    }
                }

                observerPointers.Sort(delegate (ConditionPointer a, ConditionPointer b)
                {
                    return a.priority - b.priority;
                });
                for(var x = 0; x < observerPointers.Count; ++x) {
                    var observer = GetObserver(observerPointers[x].sourceId);
                    var condition = GetCondition(observerPointers[x].conditionId);

                    if(observer != null && condition != null) {
                        decision.AddConditional(new Conditional(observer, condition));
                    }
                }
            }

            // Cache observers
            Observers = Diagram.Observers;

            // Cache decisions
            Decisions = Diagram.Decisions;

            // Cache variables
            variables = Diagram.Variables;
        }

        /// <summary>
        /// Calls OnAwake() on all modules
        /// </summary>
        public void WakeModule() {
            for(int i = 0; i < Diagram.Observers.Length; ++i) {
                Diagram.Observers[i].OnAwake();
            }

            for(int i = 0; i < Diagram.Actions.Length; ++i) {
                Diagram.Actions[i].OnAwake();
            }

            for(int i = 0; i < Diagram.Decisions.Length; ++i) {
                Diagram.Decisions[i].OnAwake();
            }
        }

        /// <summary>
        /// Destroys and purges all cloned instances
        /// </summary>
        public void Purge() {
            Object.Destroy(Diagram);
            observers.Clear();
            decisions.Clear();
            actions.Clear();
            conditions.Clear();
            System.Array.Clear(variables, 0, variables.Length);
        }

        /// <summary>
        /// Pauses the diagram
        /// </summary>
        public void Pause() {
            foreach(var element in Diagram.Observers) {
                element.OnPause();
            }
            
            if(ActiveDecision != null) {
                ActiveDecision.OnPause();
            }
        }

        /// <summary>
        /// Resumes the diagram
        /// </summary>
        public void Resume() {
            foreach(var element in Diagram.Observers) {
                element.OnResume();
            }

            if(ActiveDecision != null) {
                ActiveDecision.OnResume();
            }
        }

        /// <summary>
        /// Looks for an observer with the given id
        /// </summary>
        /// <param name="id">The id to query</param>
        /// <returns>Null if no observer is found</returns>
        public ObserverModule GetObserver(string id) {
            ObserverModule obs = null;
            observers.TryGetValue(id, out obs);
            return obs;
        }

        /// <summary>
        /// Looks for a decision with the given id
        /// </summary>
        /// <param name="id">The id to query</param>
        /// <returns>Null if no decision is found</returns>
        public DecisionModule GetDecision(string id) {
            DecisionModule dec = null;
            decisions.TryGetValue(id, out dec);
            return dec;
        }

        /// <summary>
        /// Looks for an action with the given id
        /// </summary>
        /// <param name="id">The id to query</param>
        /// <returns>Null if no action is found</returns>
        public ActionModule GetAction(string id) {
            ActionModule act = null;
            actions.TryGetValue(id, out act);
            return act;
        }

        /// <summary>
        /// Looks for a condition with the given id
        /// </summary>
        /// <param name="id">The id to query</param>
        /// <returns>Null if no condition is found</returns>
        public ConditionModule GetCondition(string id) {
            ConditionModule con = null;
            conditions.TryGetValue(id, out con);
            return con;
        }

        /// <summary>
        /// Gets a variable by name
        /// </summary>
        /// <param name="name">The name of the variable</param>
        /// <returns>A variable with the same name</returns>
        public Variable GetVariable(string name) {
            for(var i = 0; i < variables.Length; ++i) {
                if(variables[i].name == name) {
                    return variables[i];
                }
            }
            
            return null;
        }

        /// <summary>
        /// Gets a variable by name and type
        /// </summary>
        /// <typeparam name="T">The type of the variable</typeparam>
        /// <param name="name">The name of the variable</param>
        /// <returns>Null if there are no variables with the same name and type</returns>
        public T GetVariable<T>(string name) where T : Variable {
            for(var i = 0; i < variables.Length; ++i) {
                if(variables[i].name == name && variables[i] is T) {
                    return variables[i] as T;
                }
            }

            return default(T);
        }

        #region Utilities

        /// <summary>
        /// Checks to see if the diagram contains any null references
        /// </summary>
        /// <param name="diagram">The diagram to check</param>
        /// <returns>True if there are any null references</returns>
        private bool IsDirty(AIDiagram diagram) {
            for(var i = 0; i < diagram.Observers.Length; ++i) {
                if(diagram.Observers[i] == null) {
                    return true;
                }
            }

            for(var i = 0; i < diagram.Decisions.Length; ++i) {
                if(diagram.Decisions[i] == null) {
                    return true;
                }
            }

            for(var i = 0; i < diagram.Actions.Length; ++i) {
                if(diagram.Actions[i] == null) {
                    return true;
                }
            }

            for(var i = 0; i < diagram.Conditions.Length; ++i) {
                if(diagram.Conditions[i] == null) {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// A simple comparision used to sort conditions based on priorities in ascending order
        /// </summary>
        private static int CompareConditionPriority(ConditionPointer a, ConditionPointer b) {
            return a.priority.CompareTo(b.priority);
        }

        #endregion
    }
}
