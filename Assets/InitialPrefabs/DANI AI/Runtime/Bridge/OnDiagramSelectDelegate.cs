using InitialPrefabs.DANI;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// Delegate used when a diagram is selected
    /// </summary>
    /// <param name="diagram">The selected diagram</param>
    public delegate void OnDiagramSelectDelegate(AIDiagram diagram);
}