using InitialPrefabs.DANI;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// The rubtime bridge between the editor and runtime.  Allows the ability to display and edit
    /// runtime diagram in realtime.
    /// </summary>
    public class DaniRuntimeBridge {
        /// <summary>
        /// Event called when a diagram is selected
        /// </summary>
        public OnDiagramSelectDelegate diagramSelectEvent;

        /// <summary>
        /// Event called when a decision is selected
        /// </summary>
        public OnDecisionSelectDelegate decisionSelectEvent;

        /// <summary>
        /// The current bridge instance
        /// </summary>
        public static DaniRuntimeBridge Instance {
            get {
                if(_instance == null) {
                    _instance = new DaniRuntimeBridge();
                }
                return _instance;
            }
        }

        private static DaniRuntimeBridge _instance;

        public AIDiagram SelectedDiagram { get { return diagram; } }
        public DecisionModule SelectedDecisionModule { get { return decision; } }

        private AIDiagram diagram;
        private DecisionModule decision;
        
        /// <summary>
        /// Selects a diagram to use
        /// </summary>
        /// <param name="diagram"></param>
        public void SelectDiagram(AIDiagram diagram) {
            this.diagram = diagram;

            if(diagramSelectEvent != null) {
                diagramSelectEvent(diagram);
            }
        }

        public void SelectDecision(AIDiagram diagram, DecisionModule module) {
            if(this.diagram == diagram) {
                decision = module;

                if(decisionSelectEvent != null) {
                    decisionSelectEvent(module);
                }
            }
        }
    }
}
