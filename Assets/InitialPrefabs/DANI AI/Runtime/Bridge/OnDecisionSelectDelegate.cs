using InitialPrefabs.DANI;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// Delegate used when a decision is selected
    /// </summary>
    /// <param name="decision">The selected decision</param>
    public delegate void OnDecisionSelectDelegate(DecisionModule decision);
}
