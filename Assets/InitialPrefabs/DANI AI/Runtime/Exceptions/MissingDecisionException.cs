using UnityEngine;
using System;
using InitialPrefabs.DANI;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// An exception that is thrown when a diagram does not have decisions to run.
    /// </summary>
    public class MissingDecisionException : Exception {
        public MissingDecisionException(AIDiagram diagram) :
            base(string.Format("{0} does not have any decisions! A brain cannot run if it can't decide on what to run!", diagram.name)) { }
    }
}