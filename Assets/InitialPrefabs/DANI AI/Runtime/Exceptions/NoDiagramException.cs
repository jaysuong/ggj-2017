using System;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// Exception thrown when there is no valid diagram
    /// </summary>
    public class NoDiagramException : Exception {
        public NoDiagramException() : base("No Diagram exists to base the AI on!") { }
    }
}