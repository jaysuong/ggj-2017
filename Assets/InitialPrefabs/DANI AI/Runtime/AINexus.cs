using UnityEngine;
using System.Collections.Generic;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A hidden class that handles running multiple AI agents
    /// </summary>
    internal class AINexus : MonoBehaviour {
        public static AINexus Instance {
            get {
                if(_instance == null) {
                    var go = new GameObject("Dani AI Nexus");
                    go.hideFlags = HideFlags.HideInHierarchy;

                    _instance = go.AddComponent<AINexus>();
                }

                return _instance;
            }
        }

        private static AINexus _instance;


        private List<AIBrain> brains;
        private bool isDirty;


        private void Awake() {
            brains = new List<AIBrain>();
            isDirty = false;
        }

        private void OnDestroy() {
            brains.Clear();
            _instance = null;
        }

        private void Update() {
            RunAIStep(ExecutionType.OnUpdate);
        }

        private void LateUpdate() {
            RunAIStep(ExecutionType.OnLateUpdate);

            if(isDirty) {
                brains.RemoveAll(b => b == null);
                isDirty = false;
            }
        }

        private void FixedUpdate() {
            RunAIStep(ExecutionType.OnFixedUpdate);
        }


        internal void RegisterBrain(AIBrain brain) {
            brains.Add(brain);
        }

        internal void DeregisterBrain(AIBrain brain) {
            brains.Remove(brain);
        }

        /// <summary>
        /// Runs all AIBrain instances that match a given execution order
        /// </summary>
        /// <param name="executionOrder">The order to satify the run condition</param>
        private void RunAIStep(ExecutionType executionOrder) {
            for(var i = 0; i < brains.Count; ++i) {
                var brain = brains[i];

                if(brain == null) {
                    isDirty = true;
                    continue;
                }

                if(brain.isActiveAndEnabled && 
                        brain.RunningStatus == AIBrain.RunningState.Running && 
                        brain.ExecutionOrder == executionOrder) {
                    brain.RunAIStep();
                }
            }
        }
    }
}
