using UnityEngine;
using System.Collections;
using InitialPrefabs.DANI;

namespace InitialPrefabs.DANI {
    /// <summary>
    /// A simple pool containing a set of decisions of the same score
    /// </summary>
    internal class DecisionPool {
        private DecisionModule[] pool;
        private int endIndex;

        public DecisionPool(int poolSize) {
            pool = new DecisionModule[poolSize];
            endIndex = 0;
        }

        public void AddDecision(DecisionModule decision) {
            if(endIndex < pool.Length) {
                pool[endIndex++] = decision;
            } 
        }

        public DecisionModule GetRandomDecision() {
            return pool[Random.Range(0, endIndex)];
        }

        public void ClearPool() {
            endIndex = 0;
        }        
    }
}
