using UnityEditor.SceneManagement;

namespace InitialPrefabs.Utilities {
    /// <summary>
    /// Recreated class that represents a scene
    /// </summary>
    [System.Serializable]
    public class SceneInfo {

        /// <summary>
        /// Determines whether or not the scene is active
        /// </summary>
        public bool isActive;

        /// <summary>
        /// Determines whether or not the scene is loaded
        /// </summary>
        public bool isLoaded;

        /// <summary>
        /// The current path of the scene
        /// </summary>
        public string path;



        public SceneSetup setup {
            get {
                SceneSetup info = new SceneSetup();
                info.isActive = isActive;
                info.isLoaded = isLoaded;
                info.path = path;
                return info;
            }
        }

        public SceneInfo(SceneSetup setup) {
            isActive = setup.isActive;
            isLoaded = setup.isLoaded;
            path = setup.path;
        }
    }
}
