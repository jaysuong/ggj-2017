using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace InitialPrefabs.Utilities {

    /// <summary>
    /// Utilitiy class that handles loading and closing multiple scenes in the
    /// Hierarchy.  Note that this is a static class, so there is no actual
    /// instance of this class.
    /// </summary>
    public static class MultiSceneManager {

        /// <summary>
        /// Saves the current multi-scene setup
        /// </summary>
        [MenuItem("Initial Prefabs/Scene Tools/Save Current Setup &#s")]
        public static void SaveSceneSetup() {
            SceneSetup[] setup = EditorSceneManager.GetSceneManagerSetup();
            string currentSceneName = SceneManager.GetActiveScene().name;

            Debug.LogWarningFormat("Current scene is {0}, and found {1} scenes to save.",
                currentSceneName, setup.Length);

            SceneData info = GetSceneInfo(currentSceneName);

            info.info = new SceneInfo[setup.Length];
            for(int i = 0; i < setup.Length; ++i) {
                info.info[i] = new SceneInfo(setup[i]);
            }

            EditorUtility.SetDirty(info);
            AssetDatabase.SaveAssets();
        }

        /// <summary>
        /// Loads a previously saved setup into the hiearchy
        /// </summary>
        [MenuItem("Initial Prefabs/Scene Tools/Restore Setup &#r")]
        public static void LoadSceneSetup() {
            string currentSceneName = SceneManager.GetActiveScene().name;

            Debug.LogWarningFormat("Current Scene is {0}", currentSceneName);

            SceneData info = GetSceneInfo(currentSceneName);

            if(info.info != null && info.info.Length > 0) {
                SceneSetup[] setup = new SceneSetup[info.info.Length];
                for(int i = 0; i < info.info.Length; ++i) {
                    setup[i] = info.info[i].setup;
                }
                EditorSceneManager.RestoreSceneManagerSetup(setup);
            }
            else
                Debug.LogErrorFormat("There are no setups involved with this subscene [{0}].",
                    currentSceneName);
        }

        /// <summary>
        /// Closes all but the active scene
        /// </summary>
        [MenuItem("Initial Prefabs/Scene Tools/Close all but the active scene")]
        public static void CloseAllButActive() {
            Scene activeScene = SceneManager.GetActiveScene();
            Scene[] scenes = new Scene[SceneManager.sceneCount];
            for(int i = 0; i < scenes.Length; ++i)
                scenes[i] = SceneManager.GetSceneAt(i);

            for(int i = 0; i < scenes.Length; ++i) {
                if(scenes[i] != activeScene) {
                    EditorSceneManager.CloseScene(scenes[i], true);
                }
            }
        }

        /// <summary>
        /// Fetches a new scene info.
        /// </summary>
        /// <param name="name">The name of the scene info to search</param>
        /// <returns>An instance of SceneSetupInfo</returns>
        static SceneData GetSceneInfo(string name) {
            
            var guids = AssetDatabase.FindAssets("t:SceneData");
            foreach(var guid in guids) {
                var data = AssetDatabase.LoadAssetAtPath<SceneData>(
                    AssetDatabase.GUIDToAssetPath(guid));

                if(data.name == name) {
                    return data;
                }
            }

            // Handle saving new data
            var path = EditorUtility.SaveFolderPanel("Pick a folder to store the scene data", 
                "Assets", string.Empty);
            var split = path.Split(new string[] { "/Assets/" }, 
                System.StringSplitOptions.RemoveEmptyEntries);

            var info = ScriptableObject.CreateInstance<SceneData>();
            info.info = new SceneInfo[0];
            AssetDatabase.CreateAsset(info, string.Format("Assets/{0}/{1}.asset", split[1], name));
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            return info;
        }

    }
}
