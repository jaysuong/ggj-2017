using UnityEngine;
using System.Collections;

namespace InitialPrefabs.Utilities {
    /// <summary>
    /// Contains a collection in the current scene setup
    /// </summary>
    public class SceneData : ScriptableObject {

        /// <summary>
        /// Collection of scenes involved in the current setup
        /// </summary>
        public SceneInfo[] info;
    }
}
