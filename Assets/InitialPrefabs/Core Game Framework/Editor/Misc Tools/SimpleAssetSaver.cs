﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace InitialPrefabs.MiscTools {

    /// <summary>
    /// A simple tool to force Unity to save assets.  Useful for version control
    /// software, since Unity can remain open while committing changes.
    /// </summary>
    public static class SimpleAssetSaver {

        [MenuItem("Initial Prefabs/Misc Tools/Save All Assets")]
        public static void SaveAssets() {
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}
