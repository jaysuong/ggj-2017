using UnityEngine;
using UnityEditor;
using System.Linq;
using System.IO;

namespace InitialPrefabs.MiscTools {

    /// <summary>
    /// The main window used for replacing text in the asset folder.
    /// </summary>
    public class ProjectTextReplacer : EditorWindow {

        string oldText;
        string newText;

        TextAsset[] assets;
        TextAsset[] result;

        readonly string DEFAULT_SEARCH_FILTER = "t:TextAsset";

        #region Unity Message Handlers

        void OnGUI() {
            EditorGUILayout.Space();
            DrawSearchTextHandles();
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();

            if(GUILayout.Button("Find All Matches")) {
                FindAllWithText(oldText);
            }

            if(GUILayout.Button("Replace All")) {
                FindAndReplace(oldText, newText);
            }

            EditorGUILayout.EndHorizontal();

            if ( result == null || result.Length < 1 ) {
                EditorGUILayout.LabelField(new GUIContent("No text assets found."));
            }
            else {
                EditorGUILayout.LabelField(new GUIContent(string.Format(
                    "Found {0} instance(s)", result.Length)));
            }
        }

        #endregion

        #region Init

        [MenuItem("Initial Prefabs/Misc Tools/Project Text Replacer")]
        public static void Open() {
            var window = GetWindow<ProjectTextReplacer>("Text Replacer");
            window.Init();
        }

        void Init() {
            FindAllTextAssets(DEFAULT_SEARCH_FILTER);
        }

        #endregion

        #region Queries

        /// <summary>
        /// Locates all Text Assets in the project folder
        /// </summary>
        void FindAllTextAssets(string filter) {
            var guids = AssetDatabase.FindAssets(filter);

            assets = new TextAsset[guids.Length];
            for(int i = 0; i < assets.Length; ++i) {
                var path = AssetDatabase.GUIDToAssetPath(guids[i]);
                var asset = AssetDatabase.LoadAssetAtPath<TextAsset>(path);

                assets[i] = asset;
            }

            result = assets;
        }

        void FindAllWithText(string text) {
            result = assets.Where(element => element.text.Contains(text)).ToArray();
        }

        void FindAndReplace(string oldText, string newText) {
            FindAllWithText(oldText);

            foreach(var asset in result) {
                var textString = asset.text;
                textString = textString.Replace(oldText, newText);

                File.WriteAllText(AssetDatabase.GetAssetPath(asset), textString);
                EditorUtility.SetDirty(asset);
            }
        }

        #endregion

        #region GUI

        void DrawSearchTextHandles() {
            oldText = EditorGUILayout.TextField(new GUIContent("Text to find"), oldText);
            newText = EditorGUILayout.TextField(new GUIContent("Replace with"), newText);
        }

        #endregion

    }
}
