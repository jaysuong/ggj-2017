using UnityEngine;
using UnityEditor;

namespace InitialPrefabs.Utilities {
    /// <summary>
    /// Series of tools to manipulate 3d objects in the hierarchy.  Supports undo.
    /// </summary>
    public static class TransformTools {
        
        [MenuItem("Initial Prefabs/Transform Tools/Rotate Clockwise #F1")]
        public static void RotateClockwise() {
            var transform = Selection.activeTransform;

            if(transform != null) {
                RotateObject(transform, Vector3.up * 90f, 
                    string.Format("Rotate {0} clockwise", transform.name));
            }
        }

        [MenuItem("Initial Prefabs/Transform Tools/Rotate Counterclockwise #F3")]
        public static void RotateCounterclockwise() {
            var transform = Selection.activeTransform;

            if(transform != null) {
                RotateObject(transform, Vector3.up * -90f,
                    string.Format("Rotate {0} counterclockwise", transform.name));
            }
        }


        #region Utilities

        private static void RotateObject(Transform transform, Vector3 rotation, string undoMessage) {
            Undo.RecordObject(transform, undoMessage);

            var rotatedAngle = transform.localEulerAngles + rotation;
            rotatedAngle.x %= 360f;
            rotatedAngle.y %= 360f;
            rotatedAngle.z %= 360f;

            transform.localEulerAngles = rotatedAngle;
            Undo.FlushUndoRecordObjects();
        }

        #endregion

    }
}
