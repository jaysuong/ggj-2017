using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace InitialPrefabs.Utilities {
    /// <summary>
    /// Simple tool to generate scriptable objects in the project folder
    /// </summary>
    public class ScriptableObjectMaker : EditorWindow {
        [SerializeField]
        private List<ScriptMeta> scripts;
        [SerializeField]
        private ScriptMeta[] results;

        [SerializeField]
        private string searchString;
        [SerializeField]
        private Vector2 scroll;

        
        [MenuItem("Initial Prefabs/Scriptable Object Maker")]
        public static void Open() {
            GetWindow<ScriptableObjectMaker>("Scriptable Object Maker");
        }

        private ScriptableObjectMaker() {
            scripts = new List<ScriptMeta>();
            searchString = string.Empty;
            results = new ScriptMeta[0];
            scroll = Vector2.zero;
        }

        private void OnEnable() {
            Refresh();
        }

        public void OnGUI() {
            GUILayout.Label(new GUIContent("Select a type below to generate a scriptable object"), 
                EditorStyles.boldLabel);
            EditorGUILayout.Space();

            // Draw queries section
            DrawQueriesSection();

            // Draw the scroll view
            scroll = EditorGUILayout.BeginScrollView(scroll);
            foreach(var element in results) {
                if(GUILayout.Button(element.DisplayableName, EditorStyles.toolbarButton)) {
                    CreateScriptableObject(element);
                }
            }
            EditorGUILayout.EndScrollView();
        }

        
        /// <summary>
        /// Refreshes the script list and resets the results
        /// </summary>
        private void Refresh() {
            scripts.Clear();

            // Fetch from Assets folder
            var guids = AssetDatabase.FindAssets("t:MonoScript");
            foreach (var guid in guids) {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var asset = AssetDatabase.LoadAssetAtPath<MonoScript>(path);
                var meta = new ScriptMeta(asset);

                if (meta.ScriptType != null && !meta.IsAbstract && !meta.IsEditor) {
                    scripts.Add(meta);
                }
            }

            // Sort by name
            scripts = scripts.OrderBy(s => s.SearchableName).ToList();

            SearchByName(searchString);
        }

        #region Search Queries

        /// <summary>
        /// Searches all scripts that are equal or contains a substring of a given name
        /// </summary>
        private void SearchByName(string name) {
            results = scripts.Where(
                s => !s.IsEditor && s.IsScriptable && !s.IsAbstract && s.SearchableName.Contains(name)).
                OrderBy(s => s.SearchableName).
                ToArray();
        }

        #endregion

        #region GUI Helper Methods

        /// <summary>
        /// Draws the search bar and reset button on the window screen
        /// </summary>
        private void DrawQueriesSection() {
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();
            searchString = EditorGUILayout.TextField(new GUIContent("Search"), searchString);
            if (EditorGUI.EndChangeCheck()) {
                SearchByName(searchString.ToUpperInvariant());
            }

            if (GUILayout.Button("Reset")) {
                searchString = string.Empty;
                Refresh();
            }

            EditorGUILayout.EndHorizontal();
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Attempts to create a new scriptable object in the assets folder
        /// </summary>
        private void CreateScriptableObject(ScriptMeta script) {
            var path = EditorUtility.SaveFilePanelInProject(
                        string.Format("Create {0}", script.Name),
                        script.Name,
                        "asset",
                        string.Format("Where to store {0}?", script.Name)
                        );

            if (string.IsNullOrEmpty(path)) {
                return;
            }

            // Generate the object
            ScriptableObject generatedObject = null;

            try {
                generatedObject = script.Create(path);
            }
            catch (ScriptMeta.BadScriptException) {
                Debug.LogWarningFormat("Error: Could not generate this particular object: {0}",
                    script.Name);
            }
            catch (ScriptMeta.BadPathException) {
                Debug.LogWarningFormat("Error: Could not generate a scriptable object (Bad Path: {0})",
                    path);
            }

            if (generatedObject != null) {
                EditorGUIUtility.PingObject(generatedObject);
            }
        }

        #endregion
    }
}
