using System;
using UnityEditor;
using UnityEngine;

namespace InitialPrefabs.Utilities {
    /// <summary>
    /// Contains metadata on scripts in the Assets folder
    /// </summary>
    [Serializable]
    public class ScriptMeta {
        public string Name { get { return name; } }
        public Type ScriptType { get { return script.GetClass(); } }
        public string Namespace { get { return scriptNamespace; } }

        public bool IsAbstract { get { return isAbstract; } }
        public bool IsEditor { get { return isEditor; } }
        public bool IsScriptable { get { return isScriptable; } }

        public string DisplayableName { get { return displayableName; } }
        public string SearchableName { get { return searchableName; } }

        [SerializeField]
        private string name;
        [SerializeField]
        private MonoScript script;
        [SerializeField]
        private string scriptNamespace;
        [SerializeField]
        private bool isAbstract;
        [SerializeField]
        private bool isEditor;
        [SerializeField]
        private bool isScriptable;
        [SerializeField]
        private string displayableName;
        [SerializeField]
        private string searchableName;
        
        /// <summary>
        /// Exception thrown when attempting to create a ScriptableObject with a null
        /// class.
        /// </summary>
        public class BadScriptException : Exception { }

        /// <summary>
        /// Exception thrown when attempting to create a ScriptableObject with an invalid path
        /// </summary>
        public class BadPathException : Exception { }


        public ScriptMeta(MonoScript script) {
            this.script = script;

            if(ScriptType != null) {
                name = ScriptType.Name;
                scriptNamespace = ScriptType.Namespace;
                isAbstract = ScriptType.IsAbstract;
                isEditor = ScriptType.IsSubclassOf(typeof(Editor)) || 
                    ScriptType.IsSubclassOf(typeof(EditorWindow));
                isScriptable = ScriptType.IsSubclassOf(typeof(ScriptableObject));

                displayableName = string.Format("{0} ({1})", Name, Namespace);
                searchableName = Name.ToUpperInvariant();
            }
        }

        /// <summary>
        /// Generates a ScriptableObject instance at a given path.  The ScriptType is
        /// the same type as the current ScriptMeta instance.
        /// 
        /// If the Type is an inline type (class of a different name from the script's name)
        /// </summary>
        public ScriptableObject Create(string path) {
            // Abort when the script is null.  Unity can't serialize inline class defs.
            if(ScriptType == null) {
                throw new BadScriptException();
            }

            // Abort when the path assigned does not make any sense.
            if(string.IsNullOrEmpty(path)) {
                throw new BadPathException();
            }

            // Get the name of the object to create
            var split = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            var name = split[split.Length - 1].
                Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries)[0];

            // Generate the scriptable object
            var generatedObj = ScriptableObject.CreateInstance(ScriptType);
            generatedObj.name = name;

            // Create in Assets Folder
            AssetDatabase.CreateAsset(generatedObj, path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            return generatedObj;
        }
    }
}
