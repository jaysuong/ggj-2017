using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace InitialPrefabs.CoreEditor {

    /// <summary>
    /// Editor tool that merges separate meshes into a single mesh to be stored the Project folder
    /// </summary>
    [CustomEditor(typeof(MeshFilter), true)]
    public class MeshMerger : Editor {

        MeshFilter filter;
        bool deleteChildenOnMerge;

        void OnEnable() {
            filter = (MeshFilter)target;
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            EditorGUILayout.Space();

            deleteChildenOnMerge = EditorGUILayout.Toggle(
                new GUIContent("Delete Children On Merge"), 
                deleteChildenOnMerge);

            if(GUILayout.Button("Merge Meshes in Children")) {
                MergeMesh();
            }

        }

        void MergeMesh() {
            var filters = filter.GetComponentsInChildren<MeshFilter>();
            var combineInstances = new List<CombineInstance>();

            var materials = new HashSet<Material>();

            for(int i = 0; i < filters.Length; ++i) {
                if(filters[i] != filter) {
                    var combine = new CombineInstance();
                    combine.mesh = filters[i].sharedMesh;
                    combine.transform = filters[i].transform.localToWorldMatrix;
                    filters[i].gameObject.SetActive(false);

                    // Add Materials
                    foreach(var material in filters[i].GetComponent<MeshRenderer>().sharedMaterials) {
                        materials.Add(material);
                    }

                    combineInstances.Add(combine);
                }
            }

            // Build Mesh
            var mesh = new Mesh();
            mesh.CombineMeshes(combineInstances.ToArray(), materials.Count < 2);
            filter.mesh = mesh;

            var path = EditorUtility.SaveFilePanelInProject(
                "Save merged mesh", filter.name, "asset", "Save merged mesh");
            AssetDatabase.CreateAsset(mesh, path);

            var renderer = filter.GetComponent<MeshRenderer>();
            if(renderer == null) {
                renderer = filter.gameObject.AddComponent<MeshRenderer>();
            }

            var materialArr = new Material[materials.Count];
            materials.CopyTo(materialArr);
            renderer.sharedMaterials = materialArr;

            if(deleteChildenOnMerge) {
                foreach(var meshFilter in filters) {
                    if(meshFilter != filter)
                        DestroyImmediate(meshFilter.gameObject);
                }
            }
        }
    }
}
