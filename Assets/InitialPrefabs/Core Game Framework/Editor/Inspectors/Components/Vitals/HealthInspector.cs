using UnityEngine;
using UnityEditor;
using InitialPrefabs.Core;

namespace InitialPrefabs.CoreEditor {

    /// <summary>
    /// The main inspecotr class for viewing health
    /// </summary>
    [CustomEditor(typeof(Health), true)]
    public class HealthInspector : Editor {

        Health healthComponent;

        void OnEnable() {
            healthComponent = (Health)target;
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            EditorGUILayout.Space();

            var barRect = EditorGUILayout.BeginVertical();
            EditorGUI.ProgressBar(barRect, healthComponent.CurrentFraction, 
                string.Format("Health: {0} / {1}", 
                healthComponent.CurrentHealth, healthComponent.MaxHealth));

            GUILayout.Space(16f);
            EditorGUILayout.EndVertical();
        }

    }
}
