using UnityEngine;
using UnityEditor;
using InitialPrefabs.Core;

namespace InitialPrefabs.CoreEditor {

    /// <summary>
    /// The main inspector class for manipulating and debugging stamina
    /// </summary>
    [CustomEditor(typeof(Stamina), true)]
    public class StaminaInspector : Editor {

        Stamina staminaComponent;


        void OnEnable() {
            staminaComponent = (Stamina)target;
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            EditorGUILayout.Space();

            var barRect = EditorGUILayout.BeginVertical();
            EditorGUI.ProgressBar(barRect, staminaComponent.CurrentFraction,
                string.Format("Stamina: {0} / {1}", 
                staminaComponent.CurrentStamina, staminaComponent.MaxStamina));

            GUILayout.Space(16f);
            EditorGUILayout.EndVertical();

        }
    }
}
