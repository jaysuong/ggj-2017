using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace InitialPrefabs.Core {

    /// <summary>
    /// Handles the flow of time ingame.
    /// </summary>
    public class GameTimeManager : ScriptableObject {

        /// <summary>
        /// The current time scale of the game
        /// </summary>
        public float CurrentTimescale { get; private set; }

        /// <summary>
        /// Is time running?
        /// </summary>
        public bool IsRunning { get { return CurrentTimescale != 0f; } }

        /// <summary>
        /// Is time paused?
        /// </summary>
        public bool IsPaused { get { return CurrentTimescale == 0f; } }

        private enum TimeFlowState { Running, Paused }

        private TimeFlowState flowState;
        private float previousFlowScale;
        private GameTimeManagerHelper helper;

        [System.NonSerialized]
        private ScriptableObjectState initState = ScriptableObjectState.Uninitialized;

        private const float PausedScale = 0f;


        protected virtual void OnStart() {
            CurrentTimescale = Time.timeScale;

            if(Time.timeScale != PausedScale)
                flowState = TimeFlowState.Running;
            else
                flowState = TimeFlowState.Paused;
        }

        #region Timescale Manipulation

        /// <summary>
        /// Pauses all time flow
        /// </summary>
        public void Pause() {
            CheckInit();
            if(flowState == TimeFlowState.Running)
                previousFlowScale = Time.timeScale;

            Time.timeScale = PausedScale;
            flowState = TimeFlowState.Paused;
            helper.enabled = false;
        }

        /// <summary>
        /// Resumes the flow of time
        /// </summary>
        public void Resume() {
            CheckInit();
            Time.timeScale = previousFlowScale;
            flowState = TimeFlowState.Running;
            helper.enabled = true;
        }

        /// <summary>
        /// Toggles between paused and running scales
        /// </summary>
        public void Toggle() {
            CheckInit();
            if(flowState == TimeFlowState.Running) {
                Pause();
            }
            else {
                Resume();
            }
        }

        #endregion

        #region Timers

        /// <summary>
        /// Adds a timer to invoke a method at a later point in time.  If the delegate already
        /// exists, then a new one will be created.
        /// </summary>
        public void AddTimer(string name, float duration, DelayedExecutionHandler del) {
            CheckInit();
            helper.AddTimer(name, duration, del);
        }

        /// <summary>
        /// Removes a timer with a specified name, if any.
        /// </summary>
        public void RemoveTimer(string name) {
            CheckInit();
            helper.RemoveTimer(name);
        }
        
        #endregion

        #region Init Helpers

        /// <summary>
        /// Check if the scriptable object is initialized
        /// </summary>
        private void CheckInit() {
            if(initState == ScriptableObjectState.Uninitialized) {
                var go = new GameObject("Time Manager Helper");
                go.hideFlags = HideFlags.HideInHierarchy;
                helper = go.AddComponent<GameTimeManagerHelper>();

                OnStart();
                initState = ScriptableObjectState.Initialized;
            }
        }

        #endregion

    }
}
