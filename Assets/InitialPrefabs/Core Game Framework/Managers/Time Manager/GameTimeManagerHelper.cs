using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace InitialPrefabs.Core {
    /// <summary>
    /// Helper class for running timers
    /// </summary>
    internal class GameTimeManagerHelper : MonoBehaviour {

        private Dictionary<string, DelayedExecution> timers;


        private void Awake() {
            timers = new Dictionary<string, DelayedExecution>();
        }

        private void Start() {
            StartCoroutine(CleanupTimers());
        }

        private void Update() {
            foreach(var item in timers.Values) {
                item.CheckForExecution();
            }
        }

        #region Timers

        /// <summary>
        /// Adds a timer to invoke a method at a later point in time.  If the delegate already
        /// exists, then a new one will be created.
        /// </summary>
        public void AddTimer(string name, float duration, DelayedExecutionHandler del) {
            DelayedExecution timer = null;

            if(timers.TryGetValue(name, out timer)) {
                timer.Dispose();
                timers[name] = new DelayedExecution(name, del, duration);
            }
            else {
                timer = new DelayedExecution(name, del, duration);
                timers.Add(name, timer);
            }
        }

        /// <summary>
        /// Removes a timer with a specified name, if any.
        /// </summary>
        public void RemoveTimer(string name) {
            DelayedExecution timer = null;

            if(timers.TryGetValue(name, out timer)) {
                timer.Dispose();
                timers.Remove(timer.Name);
            }
        }

        private IEnumerator CleanupTimers() {
            WaitForSeconds delay = new WaitForSeconds(10f);
            while(true) {
                foreach(var item in timers.Values) {
                    if(item.Expired) {
                        timers.Remove(item.Name);
                    }
                }
                yield return delay;
            }
        }

        #endregion

    }
}
