using UnityEngine;
using System.Collections;

namespace InitialPrefabs.Core {

    public delegate void DelayedExecutionHandler();

    /// <summary>
    /// Helper class that invokes methods at a later point in time
    /// </summary>
    public class DelayedExecution {

        /// <summary>
        /// The name of the execution
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// When a method will be invoked
        /// </summary>
        public float Timestamp { get; private set; }

        /// <summary>
        /// Is this execution expired?
        /// </summary>
        public bool Expired { get; private set; }


        DelayedExecutionHandler handler;
        event DelayedExecutionHandler OnDelayedExecution;


        public DelayedExecution(string name, DelayedExecutionHandler handler, float duration) {
            Name = name;
            this.handler = handler;
            OnDelayedExecution += this.handler;
            Timestamp = Time.timeSinceLevelLoad + duration;
        }

        /// <summary>
        /// Checks if the current time is appropriate to execute.  If it is, then the method is 
        /// invoked and this instance "expires"
        /// </summary>
        public void CheckForExecution() {
            if(Expired)
                return;

            if(Time.timeSinceLevelLoad >= Timestamp) {
                InvokeMethod();
            }
        }

        /// <summary>
        /// Immediately invokes the method
        /// </summary>
        public void InvokeMethod() {
            if(OnDelayedExecution != null) {
                OnDelayedExecution();
                Expired = true;
            }
        }

        /// <summary>
        /// Cleans up memory
        /// </summary>
        public void Dispose() {
            Expired = true;
            OnDelayedExecution -= handler;
        }

    }
}
