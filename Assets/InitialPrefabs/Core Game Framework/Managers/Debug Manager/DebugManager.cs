using UnityEngine;
using System.Collections.Generic;

namespace InitialPrefabs.Core {

    /// <summary>
    /// A console-like debugging tool that appears within the game.
    /// </summary>
    public class DebugManager : ScriptableObject {
        /// <summary>
        /// If true, the debug screen will allow tags to be displayed
        /// </summary>
        public bool AllowDisplayTags {
            get { return displayTags; }
            set { displayTags = value; }
        }

        /// <summary>
        /// If true, then all messages will be displayed on the screen
        /// </summary>
        public bool AllowDisplayMessages {
            get { return displayMessages; }
            set {
                displayMessages = value;
                if(helper != null) {
                    helper.enabled = value;
                }
            }
        }

        [SerializeField, Tooltip("Will debug messages be displayed?")]
        bool displayMessages = true;
        [SerializeField] bool displayTags = false;

        internal Dictionary<string, DebugMessage> debugMessages;
        Dictionary<Color, GUIStyle> styleDict;

        [System.NonSerialized]
        private DebugManagerHelper helper;

        [System.NonSerialized]
        private ScriptableObjectState initState = ScriptableObjectState.Uninitialized;


        private void CheckInit() {
            if(initState == ScriptableObjectState.Uninitialized) {
                debugMessages = new Dictionary<string, DebugMessage>();
                styleDict = new Dictionary<Color, GUIStyle>();

                var go = new GameObject("[Debug Manager Helper]");
                helper = go.AddComponent<DebugManagerHelper>();
                helper.Manager = this;

                initState = ScriptableObjectState.Initialized;
            }
        }

        #region Logging

        /// <summary>
        /// Logs a message to be displayed ingame
        /// </summary>
        /// <param name="tag">The id of the message</param>
        /// <param name="text">The message to display</param>
        public void Log(string tag, string text) {
            CheckInit();

            DebugMessage message = null;

            if(debugMessages.TryGetValue(tag, out message)) {
                message.Message = text;
            }
            else {
                message = new DebugMessage(tag, text);
                debugMessages.Add(tag, message);
            }
        }

        /// <summary>
        /// Logs a message to be displayed ingame
        /// </summary>
        /// <param name="tag">The id of the message</param>
        /// <param name="text">The message to display</param>
        public void Log(string tag, string text, Color color) {
            CheckInit();

            DebugMessage message = null;

            if(debugMessages.TryGetValue(tag, out message)) {
                message.Message = text;
                if(message.Style.normal.textColor != color) {
                    message.Style = GetStyle(color);
                }
            }
            else {
                GUIStyle style = GetStyle(color);
                message = new DebugMessage(tag, text, style);
                debugMessages.Add(tag, message);
            }
        }


#endregion

#region Maintenence

        /// <summary>
        /// Clears any message with the same tag
        /// </summary>
        public void ClearLog(string tag) {
            CheckInit();

            debugMessages.Remove(tag);
        }

        /// <summary>
        /// Deletes all logs
        /// </summary>
        public void ClearLogs() {
            CheckInit();

            debugMessages.Clear();
        }

#endregion

#region Styles

        /// <summary>
        /// Finds a style with a matching text color.  If none are found, then a new one will
        /// be created.
        /// </summary>
        GUIStyle GetStyle(Color color) {
            GUIStyle style = null;

            if(!styleDict.TryGetValue(color, out style)) {
                style = new GUIStyle();
                style.normal.textColor = color;
                style.fontStyle = FontStyle.Bold;
                styleDict.Add(color, style);
            }

            return style;
        }

#endregion

    }
}
