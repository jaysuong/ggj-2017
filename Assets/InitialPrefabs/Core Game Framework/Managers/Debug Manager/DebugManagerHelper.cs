using UnityEngine;
using System.Collections;

namespace InitialPrefabs.Core {
    internal class DebugManagerHelper : MonoBehaviour {
        
        public DebugManager Manager { get; set; }


        private void OnGUI() {
            if(Manager == null || !Manager.AllowDisplayMessages) {
                Debug.Log(Manager);
                return;
            }

            var allowTags = Manager.AllowDisplayTags;
            foreach(var message in Manager.debugMessages.Values) {
                message.Display(allowTags);
            }
        }

    }
}
