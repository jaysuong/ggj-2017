using UnityEngine;
using System.Collections;

namespace InitialPrefabs.Core {

    public class DebugMessage {

        public string Tag { get; private set; }

        public string Message {
            get {
                return content.text;
            }
            set {
                content.text = value;
                taggedContent.text = string.Format("{0}: {1}", Tag, value);
            }
        }

        public GUIStyle Style { get; set; }

        GUIContent content;
        GUIContent taggedContent;


        #region Constructors

        public DebugMessage() {
            content = new GUIContent();
            taggedContent = new GUIContent();
            Tag = string.Empty;
            Message = string.Empty;
        }

        public DebugMessage(string tag, string message) {
            content = new GUIContent();
            taggedContent = new GUIContent();
            Tag = tag;
            Message = message;;
        }

        public DebugMessage(string tag, string message, GUIStyle style) {
            content = new GUIContent();
            taggedContent = new GUIContent();
            Tag = tag;
            Message = message;
            Style = style;
        }

        #endregion

        public void Display() {
            GUILayout.Label(content);
        }

        public void Display(bool prefixTag) {
            if(prefixTag)
                GUILayout.Label(taggedContent);
            else
                GUILayout.Label(content);
        }

    }
}
