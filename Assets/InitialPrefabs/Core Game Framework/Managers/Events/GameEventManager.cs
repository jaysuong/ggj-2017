using UnityEngine;
using System.Collections.Generic;

namespace InitialPrefabs.Core {
    public class GameEventManager : ScriptableObject {

        [SerializeField]
        private GameEvent[] events;

        private Dictionary<string, GameEvent> eventsTable;
        

        /// <summary>
        /// Finds a game event with a matching name.  Returns null if no such event is found
        /// </summary>
        public GameEvent GetEvent(string name) {
            CheckDictionary();

            GameEvent gameEvent = null;
            if(!eventsTable.TryGetValue(name, out gameEvent)) {
                foreach(var element in events) {
                    if(element.name == name) {
                        gameEvent = element;
                        eventsTable.Add(element.name, gameEvent);
                    }
                }
            }

            return gameEvent;
        }

        /// <summary>
        /// Finds a game event with a matching name.  Returns null if no such event is found.
        /// </summary>
        public T GetEvent<T>(string name) where T : GameEvent {
            CheckDictionary();

            GameEvent gameEvent = null;
            if(!eventsTable.TryGetValue(name, out gameEvent)) {
                foreach(var element in events) {
                    if(element.name == name) {
                        gameEvent = element;
                        eventsTable.Add(element.name, gameEvent);
                    }
                }
            }

            return gameEvent as T;
        }


        private void CheckDictionary() {
            if(eventsTable == null) {
                eventsTable = new Dictionary<string, GameEvent>();
            }
        }

    }
}
