using UnityEngine;
using System.Collections;

namespace InitialPrefabs.Core {
    /// <summary>
    /// Represents an event in the game
    /// </summary>
    public class GameEvent : ScriptableObject {

        public delegate void GameEventHandler();

        [SerializeField]
        private string comments;
        private event GameEventHandler OnGameInvoke;


        private void OnDisable() {
            // Clean up any rogue delegates
            if(OnGameInvoke != null) {
                foreach(var element in OnGameInvoke.GetInvocationList()) {
                    OnGameInvoke -= (GameEventHandler)element;
                }
            }
        }

        /// <summary>
        /// Registers a GameEventHandler delegate to the event.  Ensures that only one 
        /// delegate of the registered type is assigned.
        /// </summary>
        public void Register(GameEventHandler handler) {
            OnGameInvoke -= handler;
            OnGameInvoke += handler;
        }

        /// <summary>
        /// Deregisters the delegate from the event
        /// </summary>
        /// <param name="handler"></param>
        public void Deregister(GameEventHandler handler) {
            OnGameInvoke -= handler;
        }

        /// <summary>
        /// Invokes the event
        /// </summary>
        public virtual void InvokeEvent() {
            if(OnGameInvoke != null) {
                OnGameInvoke();
            }
        }
    }
}
