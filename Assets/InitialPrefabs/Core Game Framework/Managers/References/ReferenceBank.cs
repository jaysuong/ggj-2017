using UnityEngine;
using System.Collections.Generic;

namespace InitialPrefabs.Core {
    /// <summary>
    /// A collection that contains references and their names
    /// </summary>
    public class ReferenceBank : ScriptableObject {
        private Dictionary<string, object> references;


        private void OnEnable() {
            references = new Dictionary<string, object>();
        }

        private void OnDisable() {
            references.Clear();
        }

        /// <summary>
        /// Gets a reference with the same name and type.  If the same name exists, but the type is different, 
        /// then an invalid cast exception will be thrown.  If no name exists, then null is returned.
        /// </summary>
        /// <typeparam name="T">The type of the reference</typeparam>
        /// <param name="name">The name of the referene</param>
        /// <returns>The reference object, casted as T</returns>
        public T GetReference<T>(string name) {
            object result;

            if(references.TryGetValue(name, out result)) {
                if(result is T) {
                    return (T)result;
                }
                else {
                    throw new System.InvalidCastException(
                        string.Format("You are trying to get a reference of type {0}, but '{1}' is a '{2}'", 
                        typeof(T).Name, name, result.GetType().Name));
                }
            }
            else {
                return default(T);
            }
        }

        /// <summary>
        /// Checks to see if the bank contains the name of the reference
        /// </summary>
        /// <param name="name">The name of the reference</param>
        /// <returns>True if the name exists</returns>
        public bool Contains(string name) {
            return references.ContainsKey(name);
        }

        /// <summary>
        /// Checks to see if the bank contains the reference object
        /// </summary>
        /// <param name="reference">The reference to look for</param>
        /// <returns>True if the reference object exists</returns>
        public bool Contains(object reference) {
            var id = reference.GetHashCode();

            foreach(var element in references.Values) {
                if(element.GetHashCode() == id) {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Attempts to add a reference
        /// </summary>
        /// <param name="name">The name of the reference</param>
        /// <param name="reference">The object to the reference</param>
        public void AddReference(string name, object reference) {
            if(references.ContainsKey(name)) {
                references[name] = reference;
            }
            else {
                references.Add(name, reference);
            }
        }

        /// <summary>
        /// Attempts to remove a reference by name
        /// </summary>
        /// <param name="name">The name of the reference to remove</param>
        public void RemoveReference(string name) {
            if(references.ContainsKey(name)) {
                references.Remove(name);
            }
        }

        /// <summary>
        /// Attempts to remove a reference by object reference.  This method
        /// is slower than removing by name.
        /// </summary>
        /// <param name="reference">The reference to remove</param>
        public void RemoveReference(object reference) {
            var id = reference.GetHashCode();

            foreach(var element in references) {
                if(element.Value.GetHashCode() == id) {
                    references.Remove(element.Key);
                    break;
                }
            }
        }
    }
}
