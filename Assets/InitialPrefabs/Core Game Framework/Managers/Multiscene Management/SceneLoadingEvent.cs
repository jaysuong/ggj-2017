using UnityEngine;
using System.Collections;

namespace InitialPrefabs.Core {
    public enum LoadSceneType { Load, Unload }

    /// <summary>
    /// An event where a scene was loaded or unloaded
    /// </summary>
    public class SceneLoadingEvent : GameEvent {
        /// <summary>
        /// The name of the scene that was loaded / unloaded
        /// </summary>
        public string SceneName { get; private set; }

        /// <summary>
        /// Was the sceen loaded or unloaded?
        /// </summary>
        public LoadSceneType LoadType { get; private set; }

        public delegate void SceneLoadHandler(string sceneName, LoadSceneType loadType);

        private event SceneLoadHandler OnSceneLoad;


        public void InvokeEvent(string sceneName, LoadSceneType loadType) {
            SceneName = sceneName;
            LoadType = loadType;

            // Call the event internally
            InvokeEvent();

            // Manually invoke the event on all SceneLoadHandlers
            if(OnSceneLoad != null) {
                OnSceneLoad(sceneName, loadType);
            }
        }


        public void Register(SceneLoadHandler handler) {
            OnSceneLoad -= handler;
            OnSceneLoad += handler;
        }

        public void Deregister(SceneLoadHandler handler) {
            OnSceneLoad -= handler;
        }

    }
}
