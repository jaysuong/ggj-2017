using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

namespace InitialPrefabs.Core {
    /// <summary>
    /// A manager that handles loading a batch of scenes at once
    /// </summary>
    public class SceneLoader : MonoBehaviour {
        [SerializeField]
        private SceneLoadingEvent sceneEvent;

        [SerializeField, Header("Initial Scenes")]
        private bool loadInitialScenes = false;
        [SerializeField]
        private string[] scenesToLoad;

        private Stack<string[]> loadedBatches;
        private Coroutine loadingRoutine;
        

        /// <summary>
        /// The current scene loader instance
        /// </summary>
        private static SceneLoader Instance {
            get {
                if(instance == null) {
                    instance = FindObjectOfType<SceneLoader>();

                    if(instance == null) {
                        var go = new GameObject("[Generated] Scene Loader");
                        instance = go.AddComponent<SceneLoader>();
                    }
                }

                return instance;
            }
        }

        private static SceneLoader instance;


        private void Awake() {
            if(instance == null) {
                instance = this;
            }
            else if(instance != this) {
                Destroy(this);
            }

            loadedBatches = new Stack<string[]>();
            var currentBatch = new List<string>();

            for(var i = 0; i < SceneManager.sceneCount; ++i) {
                var scene = SceneManager.GetSceneAt(i);

                // If the scene belongs to the same scene the loader is in, ignore it.
                // Othewise, add it to the current batch
                if(scene != gameObject.scene) {
                    currentBatch.Add(scene.name);
                }
            }

            loadedBatches.Push(currentBatch.ToArray());
        }

        private void Start() {
            if(loadInitialScenes) {
                LoadScenes(scenesToLoad);
            }
        }

        /// <summary>
        /// Loads all scenes as a batch
        /// </summary>
        /// <param name="scenes">The scenes to load</param>
        public void LoadScenes(string[] scenes) {
            foreach(var scene in scenes) {
                SceneManager.LoadScene(scene, LoadSceneMode.Additive);

                if(sceneEvent != null) {
                    sceneEvent.InvokeEvent(scene, LoadSceneType.Load);
                }
            }

            loadedBatches.Push(scenes);
        }

        public void LoadScenes(string[] scenes, int popDepth) {
            // Pop existing batches, if possible
            for(var i = 0; i < popDepth && loadedBatches.Count > 0; ++i) {
                var scenesToPop = loadedBatches.Pop();

                foreach(var scene in scenesToPop) {
#if UNITY_5_5_OR_NEWER
                    SceneManager.UnloadSceneAsync(scene);
#else
                    SceneManager.UnloadScene(scene);
#endif

                    if(sceneEvent != null) {
                        sceneEvent.InvokeEvent(scene, LoadSceneType.Unload);
                    }
                }
            }

            // Load new scenes
            foreach(var scene in scenes) {
                SceneManager.LoadScene(scene, LoadSceneMode.Additive);

                if(sceneEvent != null) {
                    sceneEvent.InvokeEvent(scene, LoadSceneType.Load);
                }
            }

            loadedBatches.Push(scenes);
        }

        /// <summary>
        /// Loads all scenes as a batch asynchronously
        /// </summary>
        /// <param name="scenes">The scenes to load</param>
        /// <returns>True if the async operation is succesful, false if the scene loader is busy</returns>
        public bool LoadScenesAsync(string[] scenes) {
            if(loadingRoutine == null) {
                loadingRoutine = StartCoroutine(LoadAsync(scenes));
                return true;
            }

            return false;
        }

        public bool LoadScenesAsync(string[] scenes, int popDepth) {
            if(loadingRoutine == null) {
                // Pop existing batches, if possible
                for(var i = 0; i < popDepth && loadedBatches.Count > 0; ++i) {
                    var scenesToPop = loadedBatches.Pop();

                    foreach(var scene in scenesToPop) {
#if UNITY_5_5_OR_NEWER
                        SceneManager.UnloadSceneAsync(scene);
#else
                        SceneManager.UnloadScene(scene);
#endif

                        if(sceneEvent != null) {
                            sceneEvent.InvokeEvent(scene, LoadSceneType.Unload);
                        }
                    }
                }

                loadingRoutine = StartCoroutine(LoadAsync(scenes));
                return true;
            }

            return false;
        }

        /// <summary>
        /// Loads all scenes asynchronously
        /// </summary>
        /// <param name="scenes">The scenes to load</param>
        private IEnumerator LoadAsync(string[] scenes) {
            foreach(var scene in scenes) {
                var op = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
                while(!op.isDone) {
                    yield return null;
                }

                if(sceneEvent != null) {
                    sceneEvent.InvokeEvent(scene, LoadSceneType.Load);
                }
            }

            loadingRoutine = null;
        }

    }
}
