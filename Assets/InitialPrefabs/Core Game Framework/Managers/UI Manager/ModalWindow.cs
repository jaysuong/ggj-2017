using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

namespace InitialPrefabs.Core {
    /// <summary>
    /// Special kind of window that pops in front of all existing windows.
    /// </summary>
    public class ModalWindow : MonoBehaviour {

        public Text prompt;
        public Button yesButton;
        public Button noButton;
        public Button cancelButton;

        [SerializeField]
        private UIManager manager;


        void Awake() {
            // Check for nulls
            if(prompt == null) {
                Debug.LogWarning("There is no text assigned to 'prompt' in ModalWindow");
            }
            if(yesButton == null) {
                Debug.LogWarning("There is no button assigned to 'yesButton' in ModalWindow");
            }
            if(noButton == null) {
                Debug.LogWarning("There is no button assigned to 'noButton' in ModalWindow");
            }
            if(cancelButton == null) {
                Debug.LogWarning("There is no button assigned to 'cancenButton' in ModalWindow");
            }

            if(manager != null) {
                manager.RegisterModal(this);
            }
        }

        /// <summary>
        /// Opens the modal window with a custom prompt and set of actions
        /// </summary>
        public void Open(string prompt, UnityAction yesAction, UnityAction noAction, UnityAction cancelAction) {
            gameObject.SetActive(true);
            transform.SetAsLastSibling();

            this.prompt.text = prompt;

            // Assign action delegates
            yesButton.onClick.RemoveAllListeners();
            if ( yesAction != null ) {
                yesButton.onClick.AddListener(yesAction);
            }
            yesButton.onClick.AddListener(Close);

            noButton.onClick.RemoveAllListeners();
            if ( noAction != null ) {
                noButton.onClick.AddListener(noAction);
            }
            noButton.onClick.AddListener(Close);

            cancelButton.onClick.RemoveAllListeners();
            if ( cancelAction != null ) {
                cancelButton.onClick.AddListener(cancelAction);
            }
            cancelButton.onClick.AddListener(Close);

            // Activate all subcomponents
            this.prompt.gameObject.SetActive(true);
            yesButton.gameObject.SetActive(true);
            noButton.gameObject.SetActive(true);
            cancelButton.gameObject.SetActive(true);
        }

        /// <summary>
        /// Closes the modal window
        /// </summary>
        public void Close() {
            gameObject.SetActive(false);
        }

    }
}
