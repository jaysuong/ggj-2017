using System.Collections.Generic;
using UnityEngine;

namespace InitialPrefabs.Core {
    /// <summary>
    /// Handles opening, closing, and referencing UI Screens
    /// </summary>
    public class UIManager : ScriptableObject {

        public ModalWindow Modal { get; private set; }

        protected Dictionary<string, UIScreen> screens;


        /// <summary>
        /// Retrieves a screen with a matching name.  Returns null if no such screen exists
        /// </summary>
        public UIScreen GetScreen(string name) {
            CheckDictionary();

            UIScreen screen = null;
            screens.TryGetValue(name, out screen);
            return screen;
        }
        
        /// <summary>
        /// Opens a screen with a matching name
        /// </summary>
        /// <param name="name">The name of the screen to open</param>
        /// <param name="closeOthers">Close every other available screen</param>
        /// <param name="closeModal">Closes the modal window, if any</param>
        public void OpenScreen(string name, bool closeOthers = true, bool closeModal = false) {
            CheckDictionary();
            UIScreen screen = null;

            if(screens.TryGetValue(name, out screen)) {
                screen.Open();
            }

            if(closeOthers) {
                foreach(var otherScreen in screens) {
                    if(otherScreen.Key != name) {
                        otherScreen.Value.Close();
                    }
                }
            }

            if(closeModal) {
                if(Modal != null) {
                    Modal.Close();
                }
            }
        }

        /// <summary>
        /// Closes the screen with a matching name
        /// </summary>
        public void CloseScreen(string name) {
            CheckDictionary();

            UIScreen screen = null;
            if(screens.TryGetValue(name, out screen)) {
                screen.Close();
            }
        }

        /// <summary>
        /// Closes all available screens
        /// </summary>
        public void CloseAllScreens() {
            CheckDictionary();

            foreach(var screen in screens.Values) {
                screen.Close();
            }
        }



        #region Internal Methods

        internal void RegisterScreen(UIScreen screen) {
            CheckDictionary();
            UIScreen candidScreen = null;

            if(!screens.TryGetValue(screen.name, out candidScreen)) {
                candidScreen = screen;
                screens.Add(screen.name, candidScreen);
            }
            else {
                candidScreen = screen;
            }
        }

        internal void RegisterModal(ModalWindow modal) {
            Modal = modal;
        }

        internal void UnRegisterScreen(UIScreen screen) {
            CheckDictionary();

            if(screens.ContainsKey(screen.name)) {
                screens.Remove(screen.name);
            }
        }

        #endregion

        private void CheckDictionary() {
            if(screens == null) {
                screens = new Dictionary<string, UIScreen>();
            }
        }

    }
}
