using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace InitialPrefabs.Core {
    /// <summary>
    /// Represents a screen in the UI.  Contains references to texts, images, and
    /// sliders.
    /// </summary>
    [RequireComponent(typeof(CanvasGroup))]
    public class UIScreen : MonoBehaviour {
        [SerializeField]
        protected UIManager uiManager;

        [SerializeField, Tooltip("Will the screen disable itself when closing?")]
        private bool allowDisableWhenClosing = false;

        [SerializeField, Tooltip("The type of effect that the UI Screen will perform when opening/closing")]
        private TransitionMode transitionMode;

        [SerializeField]
        private float transitionSpeed = 1f;

        [SerializeField]
        private bool useRealtimeTransitions = true;

        [SerializeField]
        private AnimationCurve alphaCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);


        protected Dictionary<string, Text> texts;
        protected Dictionary<string, Image> images;
        protected Dictionary<string, Slider> sliders;
        protected Dictionary<string, Button> buttons;

        public enum TransitionMode { Instant, Fade }
        private enum ActiveState { Opened, Closed }

        private Transform root;
        private CanvasGroup canvasGroup;
        private Coroutine effectRoutine;
        private ActiveState activeState;
        

        protected virtual void Awake() {
            root = transform;
            canvasGroup = GetComponent<CanvasGroup>();
            activeState = ( canvasGroup.alpha == 0f ) ? ActiveState.Closed : ActiveState.Opened;
            canvasGroup.blocksRaycasts = activeState == ActiveState.Opened;
            canvasGroup.interactable = activeState == ActiveState.Opened;    

            texts = new Dictionary<string, Text>();
            images = new Dictionary<string, Image>();
            sliders = new Dictionary<string, Slider>();
            buttons = new Dictionary<string, Button>();

            FetchTexts();
            FetchImages();
            FetchSliders();
            FetchButtons();

            if(uiManager != null) {
                uiManager.RegisterScreen(this);
            }

            OnAwake();
        }

        protected virtual void OnDisable() {
            if(uiManager != null) {
                uiManager.UnRegisterScreen(this);
            }
        }

        #region Message Wrappers

        /// <summary>
        /// Overridable method for Unity's Awake() calls
        /// </summary>
        protected virtual void OnAwake() { }

        /// <summary>
        /// Overridable method for the event where the current screen opens
        /// </summary>
        protected virtual void OnOpen() { }

        /// <summary>
        /// Overridable method for the event where the current screen closes
        /// </summary>
        protected virtual void OnClose() { }
        
        #endregion

        #region Setup

        /// <summary>
        /// Fetches all texts in the screen's children, and assigns them a local relative path
        /// </summary>
        private void FetchTexts() {
            var textObjs = GetComponentsInChildren<Text>(true);
            foreach(var element in textObjs) {
                var path = GetPath(element.transform);
                texts.Add(path, element);
            }
        }

        /// <summary>
        /// Finds all images in the children, and assigns them a local relative path
        /// </summary>
        private void FetchImages() {
            var imageObjs = GetComponentsInChildren<Image>(true);
            foreach(var element in imageObjs) {
                var path = GetPath(element.transform);
                images.Add(path, element);
            }
        }

        /// <summary>
        /// Finds all sliders in the children, and assigns them a local relative path
        /// </summary>
        private void FetchSliders() {
            var sliderObjs = GetComponentsInChildren<Slider>(true);
            foreach(var element in sliderObjs) {
                var path = GetPath(element.transform);
                sliders.Add(path, element);
            }
        }

        /// <summary>
        /// Finds all buttons in the children, and assigns them their local relative paths
        /// </summary>
        private void FetchButtons() {
            var buttonObjs = GetComponentsInChildren<Button>(true);
            foreach(var element in buttonObjs) {
                var path = GetPath(element.transform);
                buttons.Add(path, element);
            }
        }

        /// <summary>
        /// Returns a path relative to the root transform.  Format is: "subpath/.../gameobject name"
        /// </summary>
        /// <param name="current">The current transform to build the path in</param>
        private string GetPath(Transform current) {
            var pathNames = new List<string>();
            var currentTransform = current;

            pathNames.Add(currentTransform.name);
            while(currentTransform.parent != null && currentTransform.parent != root ) {
                pathNames.Add(currentTransform.parent.name);
                currentTransform = currentTransform.parent;
            }

            var pathString = new StringBuilder();
            for(var i = pathNames.Count - 1; i > -1; --i ) {
                pathString.Append(pathNames[i]);
                if(i > 0) {
                    pathString.Append('/');
                }
            }

            return pathString.ToString();
        }

        #endregion

        #region Opening and Closing

        /// <summary>
        /// Opens the screen for display
        /// </summary>
        public void Open() {
            if(activeState == ActiveState.Closed) {
                if(allowDisableWhenClosing) {
                    gameObject.SetActive(true);
                }
                transform.SetAsLastSibling();

                canvasGroup.interactable = true;
                canvasGroup.blocksRaycasts = true;
                
                if(transitionMode == TransitionMode.Fade) {
                    if(effectRoutine != null) {
                        StopCoroutine(effectRoutine);
                    }
                    effectRoutine = StartCoroutine(FadeIn(transitionSpeed));
                }
                else {
                    canvasGroup.alpha = 1f;
                }

                OnOpen();

                activeState = ActiveState.Opened;
            }
        }

        /// <summary>
        /// Hides the screen from display
        /// </summary>
        public void Close() {
            if(activeState == ActiveState.Opened) {
                canvasGroup.interactable = false;
                canvasGroup.blocksRaycasts = false;

                if(transitionMode == TransitionMode.Instant) {
                    canvasGroup.alpha = 0f;

                    if(allowDisableWhenClosing) {
                        gameObject.SetActive(false);
                    }
                }
                else {
                    if(effectRoutine != null) {
                        StopCoroutine(effectRoutine);
                    }
                    effectRoutine = StartCoroutine(FadeOut(transitionSpeed, allowDisableWhenClosing));
                }

                OnClose();

                activeState = ActiveState.Closed;
            }
        }

        #endregion

        #region Get Methods

        /// <summary>
        /// Returns a text at the specific local path.  If no such Text is found, then a warning
        /// will appear in the console and it will return null.
        /// </summary>
        /// <param name="path">The relative location of the Text</param>
        public Text GetText(string path) {
            Text text = null;

            if(!texts.TryGetValue(path, out text)) {
                Debug.LogWarningFormat("Cannot find Text instance at path: {0}", path);
            }

            return text;
        }

        /// <summary>
        /// Returns an image at the specific local path.  If no such image is found, then a warning
        /// will appear in the console and it will return null.
        /// </summary>
        /// <param name="path">The relative location of the image</param>
        public Image GetImage(string path) {
            Image image = null;

            if(!images.TryGetValue(path, out image)) {
                Debug.LogWarningFormat("Cannot find Image instance at path: {0}", path);
            }

            return image;
        }

        /// <summary>
        /// Returns a slider at the specific local path.  If no such slider is found, then a warning
        /// will appear in the console and it will return null.
        /// </summary>
        /// <param name="path">The relative location of the slider</param>
        public Slider GetSlider(string path) {
            Slider slider = null;

            if(!sliders.TryGetValue(path, out slider)) {
                Debug.LogWarningFormat("Cannot find Slider instance at path: {0}", path);
            }

            return slider;
        }

        /// <summary>
        /// Returns a button at the specific local path.  If no such button is found, then a warning
        /// will appear in the console and it will return null.
        /// </summary>
        /// <param name="path">The relative location of the button</param>
        public Button GetButton(string path) {
            Button button = null;

            if(!buttons.TryGetValue(path, out button)) {
                Debug.LogWarningFormat("Cannot find Button instance at path: {0}", path);
            }

            return button;
        }

        #endregion

        #region Coroutines

        /// <summary>
        /// Creates a fade-in effect based on alpha curves
        /// </summary>
        /// <param name="speed">How fast should the fade in effect plays</param>
        private IEnumerator FadeIn(float speed) {
            var time = 0f;

            while(time < 1f) {
                canvasGroup.alpha = alphaCurve.Evaluate(time);
                yield return null;

                if(useRealtimeTransitions) {
                    time += Time.unscaledDeltaTime * speed;
                }
                else {
                    time += Time.deltaTime * speed;
                }
            }

            canvasGroup.alpha = alphaCurve.Evaluate(1f);
        }

        /// <summary>
        /// Creates a fade-out effect based on alpha curves, and optionally disables the screen
        /// </summary>
        /// <param name="speed">How fast the effect will play</param>
        /// <param name="disableAfterEffect">If true, then the screen will be disable after the effect</param>
        private IEnumerator FadeOut(float speed, bool disableAfterEffect) {
            var time = 1f;

            while(time > 0f) {
                canvasGroup.alpha = alphaCurve.Evaluate(time);
                yield return null;

                if(useRealtimeTransitions) {
                    time -= Time.unscaledDeltaTime * speed;
                }
                else {
                    time -= Time.deltaTime * speed;
                }
            }

            canvasGroup.alpha = alphaCurve.Evaluate(0f);

            if(disableAfterEffect) {
                gameObject.SetActive(false);
            }
        }

        #endregion

    }
}
