using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace InitialPrefabs.Core {
    /// <summary>
    /// A generic player.  Derived from the Player Controller class in Unity 4.
    /// Supportes footsteps and camera movement.
    /// </summary>
    [RequireComponent(typeof(CharacterController))]
    public class GenericPlayer : MonoBehaviour {

        #region Public Fields

        /// <summary>
        /// The "head" of the controller, where the camera rests
        /// </summary>
        public Transform head;

        /// <summary>
        /// How fast the character can move in 3d space
        /// </summary>
        public float walkSpeed = 4f;

        /// <summary>
        /// How high the character can jump in meters
        /// </summary>
        public float jumpSpeed = 4f;

        /// <summary>
        /// Does this player obey the laws of gravity?
        /// </summary>
        public bool useGravity = true;

        /// <summary>
        /// The current gravity used to calculate falls
        /// </summary>
        public float gravityMultiplier = 1f;

        /// <summary>
        /// The mouse's sensitivity in the X direction.  Determines how fast the
        /// player can turn
        /// </summary>
        public float sensitivityX = 4f;

        /// <summary>
        /// The mouse's sensitivity in the Y direction.  Determines how fast the
        /// player can look up and down.
        /// </summary>
        public float sensitivityY = 4f;

        /// <summary>
        /// The range that the player can look up and down on.
        /// </summary>
        public Vector2 yCameraBounds = new Vector2(-60, 60);

        /// <summary>
        /// The default audiosource for footsteps
        /// </summary>
        public AudioSource footSource;

        /// <summary>
        /// Footstep sounds
        /// </summary>
        public AudioClip[] footClips;

        #endregion

        #region Protected Fields

        protected CharacterController controller;
        protected Vector3 walkDirection;
        protected float xCamRotation;
        protected float yCamRotation;

        #endregion

        protected virtual void Start() {
            controller = GetComponent<CharacterController>();

            //utilities
            walkDirection = Vector3.zero;
            xCamRotation = transform.localEulerAngles.y;
            yCamRotation = head.localEulerAngles.x;

            //warnings
            if ( head == null )
                Debug.LogWarning("Warning: Head is not assigned.  Unable to look in the y-axis.");
        }
        protected virtual void Update() {
            Move();
            Turn();
        }
        protected virtual void LateUpdate() {
            ApplyCameraTransforms();
        }

        /// <summary>
        /// Moves the character based on Unity's input controls
        /// </summary>
        public virtual void Move() {
            if ( controller.isGrounded ) {
                walkDirection.x = Input.GetAxis("Horizontal");
                walkDirection.y = 0;
                walkDirection.z = Input.GetAxis("Vertical");
                walkDirection = transform.TransformDirection(walkDirection);
                walkDirection *= walkSpeed;
                //jump
                if ( Input.GetKeyUp(KeyCode.Space) )
                    walkDirection.y = jumpSpeed;
                PlayFootstepSound();
            }
            //apply gravity
            if ( useGravity )
                walkDirection.y += Physics.gravity.y * Time.deltaTime * gravityMultiplier;
            controller.Move(walkDirection * Time.deltaTime);
        }

        /// <summary>
        /// Moves the character based on the given direction
        /// </summary>
        public virtual void Move(Vector2 direction) {
            if ( controller.isGrounded ) {
                walkDirection.x = direction.x;
                walkDirection.y = 0;
                walkDirection.z = direction.y;
                walkDirection = transform.TransformDirection(walkDirection);
                walkDirection *= walkSpeed;
                //jump
                if ( Input.GetKeyUp(KeyCode.Space) )
                    walkDirection.y = jumpSpeed;
                PlayFootstepSound();
            }
            //apply gravity
            if ( useGravity )
                walkDirection.y += Physics.gravity.y * Time.deltaTime * gravityMultiplier;
            controller.Move(walkDirection * Time.deltaTime);
        }

        /// <summary>
        /// Moves the character based on the given 3d direction
        /// </summary>
        /// <param name="direction"></param>
        public virtual void Move(Vector3 direction) {
            if ( controller.isGrounded ) {
                walkDirection = direction;
                walkDirection = transform.TransformDirection(walkDirection);
                walkDirection *= walkSpeed;
                walkDirection.y = direction.y * jumpSpeed;
                PlayFootstepSound();
            }
            //apply gravity
            if ( useGravity )
                walkDirection.y += Physics.gravity.y * Time.deltaTime * gravityMultiplier;
            controller.Move(walkDirection * Time.deltaTime);
        }

        /// <summary>
        /// Rotates the camera
        /// </summary>
        public virtual void Turn() {
            //rotate on y axis
            xCamRotation = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;
            //rotate along x axis
            yCamRotation += Input.GetAxis("Mouse Y") * sensitivityY;
        }

        protected virtual void ApplyCameraTransforms() {
            yCamRotation = Mathf.Clamp(yCamRotation, yCameraBounds.x, yCameraBounds.y);
            transform.localEulerAngles = new Vector3(0, xCamRotation, 0);
            if ( head != null )
                head.localEulerAngles = new Vector3(-yCamRotation, 0, 0);
        }


        //sounds
        protected virtual void PlayFootstepSound() {
            if ( footSource != null && footClips.Length > 0 && !footSource.isPlaying ) {
                footSource.clip = footClips[Random.Range(0, footClips.Length)];
                footSource.Play();
            }
        }

        #region Editor Static Calls

#if UNITY_EDITOR

        /// <summary>
        /// Creates a new generic player in the scene (EDITOR ONLY)
        /// </summary>
        [MenuItem("GameObject/Create Other/Initial Prefabs/Generic Player")]
        public static void Create() {
            GameObject clone = new GameObject();
            clone.name = "Player";
            clone.AddComponent<CharacterController>();
            clone.AddComponent<GenericPlayer>();

            GameObject cameraComponent = new GameObject();
            cameraComponent.name = "Camera";
            cameraComponent.transform.parent = clone.transform;
            cameraComponent.transform.localPosition = Vector3.up;
            cameraComponent.AddComponent<Camera>();

            clone.GetComponent<GenericPlayer>().head = cameraComponent.transform;

            Undo.RegisterCreatedObjectUndo(clone, "Generic Player");

            Selection.activeObject = clone;
            EditorGUIUtility.PingObject(clone);
        }

#endif

        #endregion
    }
}
