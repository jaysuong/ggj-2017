#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;
using System;

namespace InitialPrefabs.Core {
    /// <summary>
    /// Tool for generating screenshots by pressing a screenshot.
    /// </summary>
    public class IngameScreenshot : MonoBehaviour {

        /// <summary>
        /// The key used for creating screenshots
        /// </summary>
        public KeyCode keyBinding = KeyCode.F5;

        /// <summary>
        /// The current scale of the generated image
        /// </summary>
        [Range(1, 8)]
        public int imageScale = 1;

        /// <summary>
        /// Setting this to true will force Unity to refresh and display screenshots
        /// immediately
        /// </summary>
        public bool forceImportUpdate = false;

        void Update() {
            CaptureScreen();
        }

        void CaptureScreen() {
            if ( Input.GetKey(keyBinding) ) {
                CheckFolders();
                string filePath = Application.dataPath + "/Screenshots/";
                string fileExtension = "Screenshot_" + DateTime.Now + ".png";
                fileExtension = fileExtension.Replace(" ", "_").Replace("/", "_").Replace(":", "_");
                Application.CaptureScreenshot(filePath + fileExtension, imageScale);
                Debug.Log("File saved at: " + filePath);

                if ( forceImportUpdate == true )
                    AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
            }
        }

        void CheckFolders() {
            if ( !Directory.Exists(Application.dataPath + "/Screenshots/") ) {
                AssetDatabase.CreateFolder("Assets", "Screenshots");
            }
        }


        #region Editor

        [MenuItem("Initial Prefabs/Create/Screenshot Tool")]
        public static void Create() {
            GameObject obj = new GameObject("Screenshot Tool (Generated Instance)");
            obj.AddComponent<IngameScreenshot>();
        }

        #endregion
    }
}
#endif
