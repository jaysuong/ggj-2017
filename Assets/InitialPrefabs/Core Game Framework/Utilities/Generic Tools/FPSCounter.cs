using UnityEngine;
using System.Collections.Generic;

namespace InitialPrefabs.Core {

    /// <summary>
    /// Displays the current FPS ingame.
    /// </summary>
    public class FPSCounter : MonoBehaviour {

        /// <summary>
        /// How many frames to count before calculating the average
        /// </summary>
        public int sampleFrameCount = 30;

        /// <summary>
        /// Threshold for displaying FPS in red
        /// </summary>
        public float lowFpsThreshold = 30f;

        /// <summary>
        /// Threshold for displaying FPS in yellow
        /// </summary>
        public float mediumFpsThreshold = 60f;

        /// <summary>
        /// The preferred color for displaying low FPS
        /// </summary>
        public Color lowFpsColor = Color.red;

        /// <summary>
        /// The preferred color of displaying medium FPS
        /// </summary>
        public Color mediumFpsColor = Color.yellow;

        /// <summary>
        /// The preferred color for displaying high FPS
        /// </summary>
        public Color highFpsColor = Color.green;


        List<float> intervals;

        float fps;

        GUIStyle lowFpsStyle;
        GUIStyle mediumFpsStyle;
        GUIStyle highFpsStyle;

        GUIStyle currentStyle;


        void Awake() {
            intervals = new List<float>(sampleFrameCount);

            lowFpsStyle = new GUIStyle();
            lowFpsStyle.normal.textColor = lowFpsColor;
            mediumFpsStyle = new GUIStyle();
            mediumFpsStyle.normal.textColor = mediumFpsColor;
            highFpsStyle = new GUIStyle();
            highFpsStyle.normal.textColor = highFpsColor;

            currentStyle = mediumFpsStyle;
        }

        void Update() {
            intervals.Add(Time.unscaledDeltaTime);

            if ( intervals.Count >= sampleFrameCount ) {
                CalculateFPS();
                SetFrameColor();
                intervals.Clear();
            }
        }

        void OnGUI() {
            GUILayout.Label(new GUIContent(string.Format("{0:F2} FPS", fps)),
                currentStyle);
        }


        /// <summary>
        /// Calculates the frame rate using current samples
        /// </summary>
        void CalculateFPS() {
            float total = 0;
            for ( int i = 0; i < intervals.Count; ++i ) {
                total += intervals[i];
            }

            fps = 1f / ( total / intervals.Count );
        }

        /// <summary>
        /// Sets the color of the frame based on thresholds
        /// </summary>
        void SetFrameColor() {
            if ( fps <= lowFpsThreshold )
                currentStyle = lowFpsStyle;
            else if ( fps <= mediumFpsThreshold )
                currentStyle = mediumFpsStyle;
            else
                currentStyle = highFpsStyle;
        }

    }
}
