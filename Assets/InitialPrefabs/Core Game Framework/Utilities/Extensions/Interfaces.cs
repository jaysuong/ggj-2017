using UnityEngine;
using System.Linq;

namespace InitialPrefabs.Core {
    /// <summary>
    /// Collection of interface extensions used for searching MonoBehaviours
    /// that contain said interface.
    /// 
    /// The following methods are very similar to Unity's GetComponent<T>()
    /// methods.  However, they are slower than GetComponent.
    /// </summary>
    public static class InterfaceExtensions {

        #region Self Interface Methods

        /// <summary>
        /// Returns the first instance of a MonoBehaviour on the current gameobject
        /// that contains the specific interface.
        /// </summary>
        /// <typeparam name="T">An interfance type</typeparam>
        /// <param name="obj">The current GameObject</param>
        /// <returns>The first instance containing interface T</returns>
        public static T GetInterface<T>(this GameObject obj) {
            if(!typeof(T).IsInterface) {
                throw new System.Exception(string.Format("{0} is not an interface!",
                    typeof(T).Name));
            }
            MonoBehaviour[] behaviors = obj.GetComponents<MonoBehaviour>();
            return ( from a in behaviors
                     where a.GetType().GetInterfaces().Any(k => k == typeof(T))
                     select (T)(object)a ).ToArray().FirstOrDefault();
        }

        /// <summary>
        /// Returns an array of MonoBehaviours that contains said interface.
        /// </summary>
        /// <typeparam name="T">An inteface type</typeparam>
        /// <param name="obj">The current GameObject</param>
        /// <returns>An array of all instances containing interface T</returns>
        public static T[] GetInterfaces<T>(this GameObject obj) {
            if(!typeof(T).IsInterface) {
                throw new System.Exception(string.Format("{0} is not an interface!",
                    typeof(T).Name));
            }
            MonoBehaviour[] behaviors = obj.GetComponents<MonoBehaviour>();
            return ( from a in behaviors
                     where a.GetType().GetInterfaces().Any(k => k == typeof(T))
                     select (T)(object)a ).ToArray();

        }

        #endregion

        #region Children Interface Methods

        /// <summary>
        /// Returns the first instance of a MonoBehaviour in the gameobject and its children
        /// that contains the specific interface.
        /// </summary>
        /// <typeparam name="T">An interface type.  If T is not an interface, a System Exception is thrown.</typeparam>
        /// <param name="obj">The current GameObject</param>
        /// <returns>An instance of type T.  Null otherwise</returns>
        public static T GetInterfaceInChildren<T>(this GameObject obj) {
            if(!typeof(T).IsInterface) {
                throw new System.Exception(string.Format("{0} is not an interface!",
                    typeof(T).Name));
            }
            MonoBehaviour[] behaviours = obj.GetComponentsInChildren<MonoBehaviour>();
            return ( from a in behaviours
                     where a.GetType().GetInterfaces().Any(k => k == typeof(T))
                     select (T)(object)a ).ToArray().FirstOrDefault();
        }

        /// <summary>
        /// Returns all instances of a MonoBehaviour in the gameobject and its children 
        /// that contains the specific interface
        /// </summary>
        /// <typeparam name="T">An interface type.  If T is not an interface, a System Exception is thrown.</typeparam>
        /// <param name="obj">The current GameObject</param>
        /// <returns>An instance of type T.  Null otherwise</returns>
        public static T[] GetInterfacesInChildren<T>(this GameObject obj) {
            if(!typeof(T).IsInterface) {
                throw new System.Exception(string.Format("{0} is not an interface!",
                    typeof(T).Name));
            }
            MonoBehaviour[] behaviours = obj.GetComponentsInChildren<MonoBehaviour>();
            return ( from a in behaviours
                     where a.GetType().GetInterfaces().Any(k => k == typeof(T))
                     select (T)(object)a ).ToArray();
        }

        #endregion

        #region All Game Objects

        /// <summary>
        /// Fetches all Monobehaviours in the scene that contains a specific interface
        /// </summary>
        /// <typeparam name="T">An interface type.  If T is not an interface, a System Exception is thrown.</typeparam>
        /// <returns>An instance of type T.  Null otherwise</returns>
        public static T[] GetInterfacesOfType<T>(this GameObject obj) {
            if(!typeof(T).IsInterface) {
                throw new System.Exception(string.Format("{0} is not an interface!",
                    typeof(T).Name));
            }
            MonoBehaviour[] behaviours = Object.FindObjectsOfType<MonoBehaviour>();
            return ( from a in behaviours
                     where a.GetType().GetInterfaces().Any(k => k == typeof(T))
                     select (T)(object)a ).ToArray();
        }

        #endregion

    }
}
