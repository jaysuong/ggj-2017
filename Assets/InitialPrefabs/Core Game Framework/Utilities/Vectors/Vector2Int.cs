using UnityEngine;
using System.Collections;

namespace InitialPrefabs.Core {
    /// <summary>
    /// An integer representation of Unity's Vector2 Class
    /// </summary>
    [System.Serializable]
    public class Vector2Int {
        /// <summary>
        /// The X component of the vector
        /// </summary>
        public int x;

        /// <summary>
        /// The Y component of the vector
        /// </summary>
        public int y;


        #region Static Properties

        /// <summary>
        /// Shorthand representation of (0, 0)
        /// </summary>
        public static Vector2Int zero { get { return new Vector2Int(); } }

        /// <summary>
        /// Shorthand representation of (1, 1)
        /// </summary>
        public static Vector2Int one { get { return new Vector2Int(1, 1); } }

        #endregion



        #region Constructors

        /// <summary>
        /// Default constructor for the Vector2Int class
        /// </summary>
        public Vector2Int() {
            x = 0;
            y = 0;
        }

        /// <summary>
        /// Creates a new instance of Vector2Int with x and y values
        /// </summary>
        /// <param name="x">The x component of the vector</param>
        /// <param name="y">The y component of the vector</param>
        public Vector2Int(int x, int y) {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// Creates a new copy of the current vector
        /// </summary>
        /// <param name="vector">The vector to copy values from</param>
        public Vector2Int(Vector2Int vector) {
            x = vector.x;
            y = vector.y;
        }

        /// <summary>
        /// Creates a new instance based on Unity's Vector2 instance.
        /// Note that the int representation is a simple cast, not a 
        /// round value.
        /// </summary>
        /// <param name="vector">The vector to copy from</param>
        public Vector2Int(Vector2 vector) {
            x = (int)vector.x;
            y = (int)vector.y;
        }

        #endregion

        /// <summary>
        /// Returns a formatted string represntation of its values
        /// </summary>
        /// <returns>A string that looks like "(x, y)"</returns>
        public string GetStringFormat() {
            return string.Format("({0}, {1})", x, y);
        }

        #region Operators

        /// <summary>
        /// Overriden addition operator.  Creates a new instance with both
        /// values added together.
        /// </summary>
        /// <param name="lhs">The left side of the equation</param>
        /// <param name="rhs">The right side of the equation</param>
        /// <returns>A new instance of lhs + rhs</returns>
        public static Vector2Int operator +(Vector2Int lhs, Vector2Int rhs) {
            return new Vector2Int(lhs.x + rhs.x, lhs.y + rhs.y);
        }

        /// <summary>
        /// Overriden subtraction operator.  Creates a new instance with both
        /// values subtracted.
        /// </summary>
        /// <param name="lhs">The left side of the equation</param>
        /// <param name="rhs">The right side of the equation</param>
        /// <returns>A new instance of lhs - rhs</returns>
        public static Vector2Int operator -(Vector2Int lhs, Vector2Int rhs) {
            return new Vector2Int(lhs.x - rhs.x, lhs.y - rhs.y);
        }

        /// <summary>
        /// Compares if two Vector2Ints' values are equal
        /// </summary>
        /// <param name="lhs">The left side of the equation</param>
        /// <param name="rhs">The right side fo the equation</param>
        /// <returns></returns>
        public static bool operator ==(Vector2Int lhs, Vector2Int rhs) {
            return ( lhs.x == rhs.x ) && ( lhs.y == rhs.y );
        }

        /// <summary>
        /// Compares if two Vector2Ints are not equal
        /// </summary>
        /// <param name="lhs">The left side of the equation</param>
        /// <param name="rhs">The right side fo the equation</param>
        /// <returns></returns>
        public static bool operator !=(Vector2Int lhs, Vector2Int rhs) {
            return !( lhs == rhs );
        }

        #endregion

        #region Equality Overrides

        /// <summary>
        /// Compares if the two Vector2Ints are actually the same instance.
        /// </summary>
        /// <param name="obj">The object to compare with</param>
        /// <returns>True if the other object is the same instance</returns>
        public override bool Equals(object obj) {
            return base.Equals(obj);
        }

        /// <summary>
        /// Returns this instance's hash code
        /// </summary>
        /// <returns>The hash code of the instance</returns>
        public override int GetHashCode() {
            return base.GetHashCode();
        }

        #endregion

    }
}
