using UnityEngine;
using System.Collections;

namespace InitialPrefabs.Core {
    /// <summary>
    /// An integer representation of Unity's Vector3 class
    /// </summary>
    [System.Serializable]
    public class Vector3Int {

        /// <summary>
        /// The X component of the vector
        /// </summary>
        public int x;

        /// <summary>
        /// The y component of the vector
        /// </summary>
        public int y;

        /// <summary>
        /// The z component of the vector
        /// </summary>
        public int z;


        #region Static Properties

        /// <summary>
        /// Shorthand way of representing (0, 0, 0)
        /// </summary>
        public static Vector3Int zero { get { return new Vector3Int(); } }

        /// <summary>
        /// Shorthand way of representing (1, 1, 1)
        /// </summary>
        public static Vector3Int one { get { return new Vector3Int(1, 1, 1); } }

        #endregion


        #region Constructors

        /// <summary>
        /// Default constructor of the instance
        /// </summary>
        public Vector3Int() {
            x = 0;
            y = 0;
            z = 0;
        }

        /// <summary>
        /// Creates a new instance with x, y, and z values
        /// </summary>
        /// <param name="x">The x component of the vector</param>
        /// <param name="y">The y component of the vector</param>
        /// <param name="z">The z component of the vector</param>
        public Vector3Int(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /// <summary>
        /// Creates a new instance with values copied over from another
        /// instance of Vector3Int
        /// </summary>
        /// <param name="vector"></param>
        public Vector3Int(Vector3Int vector) {
            x = vector.x;
            y = vector.y;
            z = vector.z;
        }

        /// <summary>
        /// Creates an int representation of Unity's Vector3 instance
        /// </summary>
        /// <param name="vector"></param>
        public Vector3Int(Vector3 vector) {
            x = (int)vector.x;
            y = (int)vector.y;
            z = (int)vector.z;
        }

        #endregion

        /// <summary>
        /// Returns a formatted string representation.
        /// </summary>
        /// <returns>A string in the format (x, y, z)</returns>
        public string GetStringFormat() {
            return string.Format("({0}, {1}, {2})", x, y, z);
        }


        #region Operators

        /// <summary>
        /// Overriden operator that represents addition
        /// </summary>
        /// <param name="lhs">The left side of the equation</param>
        /// <param name="rhs">The right side fo the equation</param>
        /// <returns>A new instance of lhs + rhs</returns>
        public static Vector3Int operator +(Vector3Int lhs, Vector3Int rhs) {
            return new Vector3Int(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
        }

        /// <summary>
        /// Overriden operator that represents subtraction
        /// </summary>
        /// <param name="lhs">The left side of the equation</param>
        /// <param name="rhs">The right side fo the equation</param>
        /// <returns>A new instance of lhs - rhs</returns>
        public static Vector3Int operator -(Vector3Int lhs, Vector3Int rhs) {
            return new Vector3Int(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
        }

        /// <summary>
        /// Compares if two Vector3Int values are equal
        /// </summary>
        /// <param name="lhs">The left side of the equation</param>
        /// <param name="rhs">The right side fo the equation</param>
        /// <returns>True if both values are equal</returns>
        public static bool operator ==(Vector3Int lhs, Vector3Int rhs) {
            return ( lhs.x == rhs.x ) && ( lhs.y == rhs.y ) && ( lhs.z == rhs.z );
        }

        /// <summary>
        /// Compares if two Vector3Int values are not equal
        /// </summary>
        /// <param name="lhs">The left side of the equation</param>
        /// <param name="rhs">The right side fo the equation</param>
        /// <returns>True of both values are not equal in any way</returns>
        public static bool operator !=(Vector3Int lhs, Vector3Int rhs) {
            return !( lhs == rhs );
        }

        #endregion

        #region Equality Overrides

        /// <summary>
        /// Compares if two instances are really just one instance
        /// </summary>
        /// <param name="obj">The object to compare</param>
        /// <returns>True of both instances are the same</returns>
        public override bool Equals(object obj) {
            return base.Equals(obj);
        }

        /// <summary>
        /// Returns the instance's hash code
        /// </summary>
        /// <returns>The hash code of the instance</returns>
        public override int GetHashCode() {
            return base.GetHashCode();
        }

        #endregion

    }
}
