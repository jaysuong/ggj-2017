using UnityEngine;
using System.Collections;

namespace InitialPrefabs.Core {
    /// <summary>
    /// Determines the state of a scriptable object.  Useful for managers.
    /// </summary>
    internal enum ScriptableObjectState { Uninitialized, Initialized }
}
