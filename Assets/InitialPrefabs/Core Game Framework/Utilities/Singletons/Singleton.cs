using UnityEngine;
using System.Collections;

namespace InitialPrefabs.Core {

    /// <summary>
    /// A game object-based singleton, where there can only be one and one instance of
    /// a particular type.
    /// Note that this is not a true singleton, since there may be multiple instance IF
    /// there are disabled objects of the same type.
    /// </summary>
    public abstract class Singleton<T> : MonoBehaviour where T : Component {

        static T instance;
        static object lockObject = new object();

        [Tooltip("Set to true if you want this script to live when a new scene loads.")]
        [SerializeField] bool persistInAllScenes;

        /// <summary>
        /// Flag to indicate whether or not the application is shutting down.
        /// </summary>
        static bool isShuttingDown = false;

        /// <summary>
        /// The current singleton instance.  If there is none in the current scene, a new
        /// instance will be generated.
        /// </summary>
        public static T Instance {
            get {
                lock(lockObject) {
                    if(instance == null) {
                        // Locate an instance of T
                        instance = FindObjectOfType<T>();

                        if(instance == null && !isShuttingDown) {
                            // Create a new game object with T attached
                            Debug.LogWarningFormat(
                                "No instance of {0} found.  Creating a new instance.", 
                                typeof(T).Name);

                            var gameObj = new GameObject(
                                string.Format("{0} (Generated Instance)", typeof(T).Name));
                            instance = gameObj.AddComponent<T>();
                        }
                    }
                    return instance;
                }
            }
        }


        protected virtual void Awake() {
            if(Instance != null) {
                if(instance == this) {
                    if(persistInAllScenes)
                        DontDestroyOnLoad(gameObject);
                }
                else {
                    Destroy(gameObject);
                }
            }
        }

        protected virtual void OnApplicationQuit() {
            isShuttingDown = true;
        }

    }
}
