using UnityEngine;
using System.Collections;

namespace InitialPrefabs.Core {

    /// <summary>
    /// Represents a character's health ingame.  Takes basic health math such as
    /// taking damage, healing, and dying.
    /// </summary>
    public class Health : MonoBehaviour {

        /// <summary>
        /// The current health of the game character
        /// </summary>
        public float CurrentHealth {
            get { return currentHealth; }
            set { currentHealth = Mathf.Min(value, maxHealth); }
        }

        /// <summary>
        /// The current percentage of the max health.  Range is [0, 100]
        /// </summary>
        public float CurrentPercentage { get { return CurrentHealth / maxHealth * 100f; } }

        /// <summary>
        /// The current fraction of the character's health.  Range is [0, 1]
        /// </summary>
        public float CurrentFraction { get { return CurrentHealth / maxHealth; } }

        /// <summary>
        /// The maximum possible health that a game character can have.
        /// </summary>
        public float MaxHealth {
            get { return maxHealth; }
            set { maxHealth = value; }
        }

        /// <summary>
        /// Event that triggers when the character takes damage
        /// </summary>
        public event HandleHealthDamageEvent DamageHealthEvent {
            add {
                OnHealthDamage -= value;
                OnHealthDamage += value;
            }
            remove { OnHealthDamage -= value; }
        }

        /// <summary>
        /// Event that triggers when a character heals
        /// </summary>
        public event HandleHealthHealEvent HealHealthEvent {
            add {
                OnHealthHeal -= value;
                OnHealthHeal += value;
            }
            remove { OnHealthHeal -= value; }
        }

        /// <summary>
        /// Event that triggers when a character dies
        /// </summary>
        public event HandleDeathEvent DeathEvent {
            add {
                OnDeath -= value;
                OnDeath += value;
            }
            remove {
                OnDeath -= value;
            }
        }

        #region Fields

        protected float currentHealth = 100f;

        /// <summary>
        /// The maximum possible health that a character can have
        /// </summary>
        [SerializeField]
        protected float maxHealth = 100f;

        #endregion

        #region Events

        event HandleHealthDamageEvent OnHealthDamage;
        event HandleHealthHealEvent OnHealthHeal;
        event HandleDeathEvent OnDeath;

        #endregion

        #region Delegate Definitions

        public delegate void HandleHealthDamageEvent( float damage, Health health );
        public delegate void HandleHealthHealEvent( float amount, Health health );
        public delegate void HandleDeathEvent( Health health );

        #endregion


        protected virtual void Start() {
            currentHealth = maxHealth;
        }


        #region Public Methods

        /// <summary>
        /// Damages the character's health.  If the damage is negative, then no damage
        /// will be taken.
        /// </summary>
        /// <param name="damage">The amount of damage a character will take</param>
        public virtual void Damage(float damage) {
            if(currentHealth <= 0f || damage <= 0f) {
                return;
            }

            currentHealth -= damage;

            if(currentHealth <= 0f) {
                Die();
            }
            else {
                if(OnHealthDamage != null) {
                    OnHealthDamage(damage, this);
                }
            }
        }

        /// <summary>
        /// Heals the character.  The current health will not go over the maximum 
        /// possible health.
        /// </summary>
        /// <param name="amount">The amount to heal by</param>
        public virtual void Heal(float amount) {
            currentHealth = Mathf.Min(currentHealth + amount, maxHealth);

            if(OnHealthHeal != null) {
                OnHealthHeal(amount, this);
            }
        }

        /// <summary>
        /// Kills the character by reducing its health to zero
        /// </summary>
        public virtual void Die() {
            currentHealth = 0f;

            if(OnDeath != null) {
                OnDeath(this);
            }
        }

        #endregion

        #region Protected Triggers

        /// <summary>
        /// Manual trigger to invoke OnHealthDamage events
        /// </summary>
        /// <param name="damage">The amount of damage the character took</param>
        protected void TriggerDamageHealthEvent(float damage) {
            if(OnHealthDamage != null) {
                OnHealthDamage(damage, this);
            }
        }

        /// <summary>
        /// Manually triggers an OnHealthHeal event
        /// </summary>
        /// <param name="amount">The amount that the character healed by</param>
        protected void TriggerHealEvent(float amount) {
            if(OnHealthHeal != null) {
                OnHealthHeal(amount, this);
            }
        }

        /// <summary>
        /// Manually triggers an OnDeath event
        /// </summary>
        protected void TriggerDeathEvent() {
            if(OnDeath != null) {
                OnDeath(this);
            }
        }

        #endregion

    }
}
