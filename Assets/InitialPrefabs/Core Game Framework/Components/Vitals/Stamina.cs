using UnityEngine;
using System.Collections;

namespace InitialPrefabs.Core {

    /// <summary>
    /// Represents a player's stamina.  This value can be used for actions
    /// such as running, jumping, etc.
    /// </summary>
    public class Stamina : MonoBehaviour {

        /// <summary>
        /// The current amount of stamina available
        /// </summary>
        public float CurrentStamina {
            get { return currentStamina; }
            set { currentStamina = Mathf.Min(value, maxStamina); }
        }

        /// <summary>
        /// The maximum available stamina allowed
        /// </summary>
        public float MaxStamina {
            get { return maxStamina; }
            set { maxStamina = value; }
        }

        /// <summary>
        /// The current available stamina, as a percent (0, 100)
        /// </summary>
        public float CurrentPercentage { get { return currentStamina / maxStamina * 100f; } }

        /// <summary>
        /// The current available stamina, as a fraction (0, 1)
        /// </summary>
        public float CurrentFraction { get { return currentStamina / maxStamina; } }

        /// <summary>
        /// Is the character able to automatically recover stamina?
        /// </summary>
        public bool CanAutoRecover {
            get { return canAutoRecover; }
            set { canAutoRecover = value; }
        }

        /// <summary>
        /// Event that triggers when the character is exhausting stamina
        /// </summary>
        public event HandleStaminaDepletion StaminaDrainEvent {
            add {
                OnStaminaDrain -= value;
                OnStaminaDrain += value;
            }
            remove {
                OnStaminaDrain -= value;
            }
        }

        /// <summary>
        /// How long will it take after drain to begin recovering stamina?
        /// </summary>
        public float RecoverDelay {
            get { return recoverDelay; }
            set { recoverDelay = Mathf.Abs(value); }
        }

        /// <summary>
        /// How much stamina should the character automatically recover per tick
        /// </summary>
        public float AutoRecoverAmount {
            get { return autoRecoverAmount; }
            set { autoRecoverAmount = Mathf.Abs(value); }
        }

        /// <summary>
        /// How long each tick is
        /// </summary>
        public float RecoverTickLength {
            get { return recoverTickLength; }
            set { recoverTickLength = Mathf.Abs(value); }
        }

        /// <summary>
        /// Event that triggers when the character is recovering stamina
        /// </summary>
        public event HandleStaminaHeal StaminaHealEvent {
            add {
                OnStaminaRecover -= value;
                OnStaminaRecover += value;
            }
            remove {
                OnStaminaRecover -= value;
            }
        }

        #region Fields

        protected float currentStamina = 100f;

        [SerializeField]
        protected float maxStamina = 100f;

        [SerializeField]
        protected bool canAutoRecover = true;

        [SerializeField]
        protected float recoverDelay = 3f;

        [SerializeField]
        protected float autoRecoverAmount = 1f;

        [SerializeField]
        protected float recoverTickLength = 1f;

        event HandleStaminaDepletion OnStaminaDrain;
        event HandleStaminaHeal OnStaminaRecover;

        #endregion

        #region Event Definitions

        public delegate void HandleStaminaDepletion( float amount, Stamina stamina );
        public delegate void HandleStaminaHeal( float amount, Stamina stamina );

        #endregion


        void Start() {
            currentStamina = maxStamina;

            if(canAutoRecover) {
                TriggerAutoRecover();
            }
        }


        #region Public Methods

        /// <summary>
        /// Drains the character's current stamina
        /// </summary>
        /// <param name="amount">The amount to drain by</param>
        public virtual void Drain(float amount) {
            if(currentStamina <= 0f || amount <= 0f) {
                return;
            }

            CancelInvoke();

            currentStamina = Mathf.Max(currentStamina - amount, 0f);
            TriggerDrainEvent(amount);

            if(canAutoRecover) {
                Invoke("Recover", recoverDelay);
            }
        }

        /// <summary>
        /// Recovers the character's current stamina
        /// </summary>
        /// <param name="amount">The amount to recover by</param>
        public virtual void Recover(float amount) {
            if(amount <= 0f || currentStamina >= maxStamina) {
                return;
            }

            currentStamina = Mathf.Min(currentStamina + amount, maxStamina);
            TriggerRecoverEvent(amount);
        }

        /// <summary>
        /// Triggers auto recovery
        /// </summary>
        /// <returns></returns>
        public void TriggerAutoRecover() {
            CancelInvoke();
            Recover(autoRecoverAmount);
            Invoke("TriggerAutoRecover", recoverTickLength);
        }

        #endregion

        #region Protected Triggers

        /// <summary>
        /// Manually triggers OnStaminaDrain event
        /// </summary>
        /// <param name="amount">The amount of stamina to drain</param>
        protected void TriggerDrainEvent(float amount) {
            if(OnStaminaDrain != null) {
                OnStaminaDrain(amount, this);
            }
        }

        /// <summary>
        /// Manually triggers OnStaminaRecover event
        /// </summary>
        /// <param name="amount">The amount to recover by</param>
        protected void TriggerRecoverEvent(float amount) {
            if(OnStaminaRecover != null) {
                OnStaminaRecover(amount, this);
            }
        }

        #endregion

    }
}
