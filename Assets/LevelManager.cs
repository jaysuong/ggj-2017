﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InitialPrefabs.Core;

public class LevelManager : MonoBehaviour
{
	public AudioClip bgm;
	public Health startBlimp;
	public WaveManager waveManager;
	public int GameMode = 0;
	// Use this for initialization
	void Start()
	{
	}
	private void Update()
	{
		if(startBlimp.CurrentHealth <= 0 && GameMode == 0)
		{
			GameMode = 1;
			waveManager.StartSpawning();
			SoundManager.instance.PlayMusic(bgm, 57.5f);
		}
	}

}

